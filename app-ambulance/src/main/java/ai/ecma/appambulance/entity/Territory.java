package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.District;
import ai.ecma.appambulance.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Territory extends AbsEntity {
    private String name;
    private String street;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private District district;
}
