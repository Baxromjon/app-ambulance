package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.MedicineName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MedicineNameRepository extends JpaRepository<MedicineName, UUID> {
    Boolean existsByName(String name);
}
