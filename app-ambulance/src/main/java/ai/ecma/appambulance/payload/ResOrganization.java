package ai.ecma.appambulance.payload;

import ai.ecma.appambulance.entity.OrganizationPermission;
import ai.ecma.appambulance.entity.OrganizationRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.User;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResOrganization {
    @NotBlank
    private String name;
    @NotBlank
    private String phoneNumber;
    @NotNull
    private UUID districtId;
    private String street;
    @NotNull
    private UUID managementId;
    private List<OrganizationRole> roles;
    private List<UUID> rolesId;
    private List<OrganizationPermission> permissions;
    private List<UUID> permissionsId;
    private UserDto userDto;
//    private List<User> users;
//    private List<UUID> userId;

    public ResOrganization(String name, String phoneNumber, UUID districtId, String street, UUID managementId, List<UUID> rolesId, List<UUID> permissionsId) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.districtId = districtId;
        this.street = street;
        this.managementId = managementId;
        this.rolesId = rolesId;
        this.permissionsId = permissionsId;
    }

    public ResOrganization(String name, String phoneNumber, UUID districtId, String street, UUID managementId, List<UUID> rolesId, List<UUID> permissionsId, UserDto userDto) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.districtId = districtId;
        this.street = street;
        this.managementId = managementId;
        this.rolesId = rolesId;
        this.permissionsId = permissionsId;
        this.userDto = userDto;
    }
}
