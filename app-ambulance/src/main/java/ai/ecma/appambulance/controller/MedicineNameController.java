package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.MedicineName;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResMedicineName;
import ai.ecma.appambulance.service.MedicineNameService;
import com.google.api.Http;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/medicineName")
public class MedicineNameController {
    @Autowired
    MedicineNameService medicineNameService;

    @GetMapping
    public List<MedicineName> getAll() {
        return medicineNameService.getAll();
    }

    @GetMapping("{id}")
    public MedicineName getById(@PathVariable UUID id) {
        return medicineNameService.getById(id);
    }

    @PostMapping("save")
    public HttpEntity<?> addMedicineName(@Valid @RequestBody ResMedicineName resMedicineName) {
        ApiResponse apiResponse = medicineNameService.addMedicineName(resMedicineName);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editMedicineName(@PathVariable UUID id, @RequestBody ResMedicineName resMedicineName) {
        ApiResponse apiResponse = medicineNameService.editMedicineName(id, resMedicineName);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteMedicineName(@PathVariable UUID id){
        ApiResponse apiResponse = medicineNameService.deleteMedicineName(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
}
