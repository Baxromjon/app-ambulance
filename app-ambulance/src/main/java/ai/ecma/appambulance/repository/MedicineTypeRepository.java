package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.MedicineType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface MedicineTypeRepository extends JpaRepository<MedicineType, Integer> {
    Boolean existsByName(String name);

}
