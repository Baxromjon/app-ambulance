import {BASEURL} from "./constant";

export default {

    loginUrl: '/auth/login',
    registerUrl: '/auth/register',
    checkPasswordAndLoginUrl: '/auth/checkPasswordAndLogin',
    checkSmsCode: '/auth/checkSmsCode',
    verification: '/auth/verification',
    //COUNTRY
    getCountry: '/country/getAll',
    addCountry: '/country/save',
    editCountry: '/country/edit',
    deleteCountry: '/country/delete',

    //REGION
    getRegion: '/region/getAll',
    addRegion: '/region/save',
    editRegion: '/region/edit',
    deleteRegion: '/region/delete',
    getAllRegionOfCountry: '/region/getAllRegionOfCountry',
    getAllDistrictOfRegion:'/region/getAllDistrictOfRegion',
    getRegionList:'/region/export/excel',

    //DISTRICT
    getDistrict: '/district/getAll',
    addDistrict: '/district/save',
    editDistrict: '/district/edit',
    deleteDistrict: '/district/delete',

    //ADDRESS
    getAddress: '/assembly/getAll',
    addAddress: '/assembly/save',
    editAddress: '/assembly/update',
    deleteAddress: '/assembly/delete',

    //MINISTRY
    getMinistry: '/ministry/getAllMinistry',
    editMinistry: '/ministry/edit',
    addMinistry: '/ministry/save',
    deleteMinistry: '/ministry/delete',
    getMinistryOfCountry:'/ministry/getMinistryOfCountry',

    //MANAGEMENTS
    getManagements: '/management/getAllManagements',
    addManagement: '/management/save',
    editManagement: '/management/edit',
    deleteManagement: '/management/delete',

    //ORGANIZATION
    getOrganization:'/organization/getAllOrganization',
    addOrganization:'/organization/save',
    editOrganization:'/organization/edit',
    deleteOrganization:'/organization/delete',
    getAllOrganization:'/organization/getOrganizationOfManagement',

    //ORGANIZATION_ROLE
    getOrganizationRoles:'/organizationRole/getAllOrganizationRole',

    //ORGANIZATION_PERMISSION
    getOrganizationPermission:'/organizationPermission/getAllPermission',

    //USER
    register:'/user/register_employee',
    getDriver:'/user/getAllDrivers',

    //CAR
    getCar:'/car/getAllCar',
    addCar:'/car/save',
    deleteCar:'/car/delete',
    editCar:'/car/edit',
    getCarsByOrganizationId:'/car/getCarByOrganizationId',

    //MODEL
    getModel:'/model/getAllModel',
    addModel:'/model/save',
    editModel:'/model/edit',
    deleteModel:'/model/delete',
    getAllModelOfBrand:'/model/getAllModelOfBrand',

    //BRAND
    getAllBrand:'/brand/getAllBrand',


    //COLORS
    getColor:'/color/getAllColor',
    addColor:'/color/save',
    editColor:'/color/edit',
    deleteColor:'/color/delete',

    //MADE_YEAR
    getMadeYear:'/madeYear/getAllMadeYear',
    addMadeYear:'/madeYear/save',
    deleteMadeYear:'/madeYear/delete',
    editMadeYear:'/madeYear/edit',

    //TIMETABLE
    getAllTimeTable:'/timeTable/getAllTimeTable',
    saveTimeTable:'/timeTable/saveTimeTableForDriver',

    //POSITION
    getAllPosition:'/position/getAllPosition',
    savePosition:'/position/savePosition',
    deletePosition:'/position/deletePosition',
    editPosition:'/position/editPosition',

    //TERRITORY
    getAllTerritory:'/territory/getAll',
    saveTerritory:'/territory/addTerritory',
    editTerritory:'/territory/edit',
    deleteTerritory:'/territory/delete',

    //MEDICINE_FARM

    addMedicineFarm:'/medicineFarm/save',
    getMedicineFarm:'/medicineFarm/getAllMedicineFarm',
    editMedicineFarm:'/medicineFarm/edit',
    deleteMedicineFarm:'/medicineFarm/delete',

    //MEDICINE_TYPE

    getMedicineType:'/medicineType/getAllMedicineType',
    addMedicineType:'/medicineType/save',
    editMedicineType:'/medicineType/edit',
    deleteMedicineType:'/medicineType/delete',

    //MEDICINE

    getAllMedicine:'/medicine/getAllMedicine',
    addMedicine:'/medicine/saveMedicine',
    editMedicine:'/medicine/editMedicine',
    deleteMedicine:'/medicine/deleteMedicine',








}