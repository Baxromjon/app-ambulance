package ai.ecma.appambulance.entity.template;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@EqualsAndHashCode(callSuper = true)
@Data
@MappedSuperclass
public abstract class AbsNameEntity extends AbsEntity {
    @Column(nullable = false)
    private String name;
}
