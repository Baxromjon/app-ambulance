package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.MedicineFarm;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResMedicineFarm;
import ai.ecma.appambulance.service.MedicineFarmService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/medicineFarm")
public class MedicineFarmController {
    @Autowired
    MedicineFarmService medicineFarmService;

    @GetMapping("getAllMedicineFarm")
    public List<MedicineFarm> getAll() {
        return medicineFarmService.getAllFarm();
    }

    @GetMapping("{id}")
    public MedicineFarm getById(@PathVariable UUID id) {
        return medicineFarmService.getById(id);
    }

    @PostMapping("save")
    public HttpEntity<?> addFarm(@Valid @RequestBody ResMedicineFarm resMedicineFarm) {
        ApiResponse apiResponse = medicineFarmService.addFarm(resMedicineFarm);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editFarm(@PathVariable UUID id, @RequestBody ResMedicineFarm resMedicineFarm) {
        ApiResponse apiResponse = medicineFarmService.editFarm(id, resMedicineFarm);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteFarm(@PathVariable UUID id) {
        ApiResponse apiResponse = medicineFarmService.deleteFarm(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}
