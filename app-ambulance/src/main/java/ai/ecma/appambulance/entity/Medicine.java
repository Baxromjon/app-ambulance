package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))

public class Medicine extends AbsNameEntity {
    private String description;
    @Column(nullable = false)
    private Double amount;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private Date expiredDate;
    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp acceptedDate;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "medicine_medicine_type", joinColumns = {@JoinColumn(name = "medicine_id")},
            inverseJoinColumns = {@JoinColumn(name = "medicine_type_id")})
    private List<MedicineType> medicineTypes;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private MedicineFarm medicineFarm;

}
