package ai.ecma.appambulance.projection;

import java.util.UUID;

public interface DriverProjection {
    UUID getDriverId();

    String getLastName();

    String getFirstName();
}
