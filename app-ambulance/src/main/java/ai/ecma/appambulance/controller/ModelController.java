package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Model;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResModel;
import ai.ecma.appambulance.projection.ModelProjection;
import ai.ecma.appambulance.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.lang.Integer;

@RestController
@CrossOrigin
@RequestMapping("/api/model/")
public class ModelController {
    @Autowired
    ModelService modelService;

    @GetMapping("getAllModel")
    public List<Model> getAllModel() {
        return modelService.getAllModel();
    }

    @PostMapping("save")
    public HttpEntity<?> addModel(@Valid @RequestBody ResModel resModel) {
        ApiResponse apiResponse = modelService.addModel(resModel);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editModel(@Valid @PathVariable Integer id, @RequestBody ResModel resModel) {
        ApiResponse apiResponse = modelService.editModel(id, resModel);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("{id}")
    public Model getModelById(@PathVariable Integer id) {
        return modelService.getModelById(id);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteModel(@PathVariable Integer id) {
        ApiResponse apiResponse = modelService.deleteModel(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("getAllModelOfBrand/{id}")
    public List<ModelProjection> getAllModel(@PathVariable Integer id){
       return modelService.getAllModelOfBrand(id);
    }
}
