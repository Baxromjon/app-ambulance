package ai.ecma.appambulance.projection;

import java.util.UUID;

public interface RegionObject {
    UUID getId();
    String getName();
}
