import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'
import request from "../../utils/request";
import api from "../../utils/api";
import AddOrEditTimeTable from "../../Modals/AddOrEditTimeTable";
import {TOKEN} from "../../utils/constant";

class TimeTableDashboard extends Component {
    state = {
        showModal: false,
        timeTables: [],
        currentTimeTable: '',
        currentMonth: new Date().getMonth()+1,
        months: [],
        days: [],
        month: [
            {id: 1, name: 'January', length: 31},
            {id: 2, name: 'February', length: 29},
            {id: 3, name: 'March', length: 31},
            {id: 4, name: 'April', length: 30},
            {id: 5, name: 'May', length: 31},
            {id: 6, name: 'June', length: 30},
            {id: 7, name: 'July', length: 31},
            {id: 8, name: 'August', length: 31},
            {id: 9, name: 'September', length: 30},
            {id: 10, name: 'October', length: 31},
            {id: 11, name: 'November', length: 30},
            {id: 12, name: 'December', length: 31}
        ],
        day: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
    }

    openModal = () => {
        this.setState({showModal: !this.state.showModal})
    }
    // closeModal = () => {
    //     this.setState({showModal: false})
    // }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getTimeTable()
            this.getMonth()
        }
    }



    getTimeTable = () => {
        request({
            url: api.getAllTimeTable,
            method: 'GET'
        }).then(res => {
            this.setState({timeTables: res.data})
        }).catch(err => {
        })
    }
    saveTimeTable = (e, v) => {
        console.log(v)
        request({
            url: api.saveTimeTable,
            method: 'POST',
            data: v
        }).then(res => {
            console.log("keldi")
            this.getTimeTable()
            this.openModal()
        }).catch(err => {
            alert("xatolik")
        })
    }

    getDriver = () => {
        let current = this.state.currentOrganization
        console.log(current)
        request({
            url: api.getDriver + '/' + current,
            method: 'GET'
        }).then(res => {
            this.setState({drivers: res.data})
        }).catch(err => {
        })
    }

    selectMonth = (month) => {
        this.setState({currentMonth: month.target.value})
    }
    getMonth = () => {
        let nums = [];
        this.state.month.map(item => {
                // console.log(item)
                if (item.id == this.state.currentMonth) {
                    // console.log(item)
                    this.setState({months: item})
                    for (let i = 1; i <= item.length; i++) {
                        nums.push(i);
                    }
                    // console.log(nums)
                    this.setState({days: nums})
                }
            }
        );
    }

    render() {
        const {
            currentMonth,
            month,
            months,
            days,
            day,
            showModal,
            timeTables,
            users,
            cars,
            organizations,
            drivers,
            currentOrganization
        } = this.state;
        console.log(users)


        return (
            <div>
                <h1>Time table</h1>
                <button className="btn btn-success"
                        onClick={this.openModal}>+add
                </button>
                <div>
                    <select onChange={(month) => this.selectMonth(month)}
                            onClick={this.getMonth}
                            defaultValue={new Date().getMonth()+1}>
                        {month.map((month) => (
                            <option value={month.id}>{month.name}</option>
                        ))}
                    </select>
                </div>
                <table className="table table-bordered text-center">
                    <thead>
                    <tr>
                        <th rowSpan={2}>№</th>
                        <th rowSpan={2}>
                            Xodimning ismi va familiyasi
                        </th>
                        {/*<th colSpan={31}>January</th>*/}
                        <th colSpan={months.length} className="text-center">{months.name}</th>
                        <th rowSpan={2}>Xodimning barcha smenlari</th>
                        <th rowSpan={2}>Xodimning umumiy ish soati</th>
                        <th rowSpan={2}>soatbay is stavkasi</th>
                    </tr>
                    <tr>
                        {days?.map(item =>
                            <th>{item}</th>
                        )}
                    </tr>
                    </thead>
                    <tbody>
                    {users?.map((user, index) =>
                        <tr>
                            <td>{index + 1}</td>
                            <td>{user.firstName + ' ' + user.lastName}</td>
                            {new Array(months.length+3).fill(0).map((it, i) =>
                                <td style={{cursor: 'pointer'}} onClick={() => {
                                    this.openModal()
                                }}>{i+1}</td>
                            )}
                        </tr>
                    )}
                    </tbody>
                </table>
                {showModal &&
                <AddOrEditTimeTable
                    toggle={(x) => this.openModal(x)}
                    saveTimeTable={this.saveTimeTable}
                />}
            </div>
        );
    }
}

TimeTableDashboard.propTypes = {};

export default TimeTableDashboard;