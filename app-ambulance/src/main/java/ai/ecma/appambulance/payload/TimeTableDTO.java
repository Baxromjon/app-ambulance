package ai.ecma.appambulance.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeTableDTO {
    @NotNull
    private UUID userId;
    @NotNull
    private UUID carId;
    private String fromDate;
    private String fromTime;
    private String toDate;
    private String toTime;
    private Double rateByHour;
}
