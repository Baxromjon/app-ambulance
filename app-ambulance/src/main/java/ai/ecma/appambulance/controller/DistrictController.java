package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.District;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResDistrict;
import ai.ecma.appambulance.service.DistrictService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/district")
public class DistrictController {
    @Autowired
    DistrictService districtService;

    @GetMapping("getAll")
    public List<District> getAllDistrict() {
        return districtService.getAllDistrict();
    }

    @GetMapping("{id}")
    public District getDistrictById(@PathVariable UUID id) {
        return districtService.getDistrictById(id);
    }

    @PostMapping("save")
    public HttpEntity<?> addDistrict(@RequestBody ResDistrict resDistrict) {
        ApiResponse apiResponse = districtService.addDistrict(resDistrict);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editDistrict(@PathVariable UUID id, @RequestBody ResDistrict resDistrict) {
        ApiResponse apiResponse = districtService.editDistrict(id, resDistrict);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteDistrict(@PathVariable UUID id) {
        ApiResponse apiResponse = districtService.deleteDistrict(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

}
