package ai.ecma.appambulance.payload;

import java.util.UUID;

public interface OrganizationRoleProjection {
    UUID getValue();
    String getLabel();
}
