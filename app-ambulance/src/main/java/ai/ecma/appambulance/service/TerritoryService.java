package ai.ecma.appambulance.service;
/**
 * created by Baxromjon
 * 28.05.2021
 **/

import ai.ecma.appambulance.entity.District;
import ai.ecma.appambulance.entity.Territory;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.TerritoryDto;
import ai.ecma.appambulance.repository.DistrictRepository;
import ai.ecma.appambulance.repository.TerritoryRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service


public class TerritoryService {
    @Autowired
    TerritoryRepository territoryRepository;
    @Autowired
    DistrictRepository districtRepository;

    public List<Territory> getAll() {
        return territoryRepository.findAll();
    }

    public Territory getById(UUID id) {
        Optional<Territory> byId = territoryRepository.findById(id);
        return byId.get();
    }

    public ApiResponse addTerritory(TerritoryDto territoryDto) {
        Optional<District> districtOptional = districtRepository.findById(territoryDto.getDistrictId());
        Boolean exists = territoryRepository.existsByDistrictAndStreet(districtOptional.get(), territoryDto.getStreet());
        if (exists) {
            return new ApiResponse(false, "allReady exists");
        }
        Territory territory = new Territory(
                territoryDto.getName(),
                territoryDto.getStreet(),
                districtOptional.get()
        );
        territoryRepository.save(territory);
        return new ApiResponse(true, "successfully saved");

    }

    public ApiResponse editTerritory(UUID id, TerritoryDto territoryDto) {
        Optional<Territory> territoryOptional = territoryRepository.findById(id);
        Optional<District> districtOptional = districtRepository.findById(territoryDto.getDistrictId());
        if (territoryOptional.isPresent()) {
            Territory territory = territoryOptional.get();
            territory.setName(territoryDto.getName());
            territory.setStreet(territoryDto.getStreet());
            territory.setDistrict(districtOptional.get());
            territoryRepository.save(territory);
            return new ApiResponse(true, "successfully edited");
        }
        return new ApiResponse(false, "not founded");
    }

    public ApiResponse delete(UUID id) {
        try {
            territoryRepository.deleteById(id);
            return new ApiResponse(true,"successfully deleted");
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"exception");
        }

    }
}
