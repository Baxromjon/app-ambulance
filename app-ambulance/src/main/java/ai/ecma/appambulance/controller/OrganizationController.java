package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Organization;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResOrganization;
import ai.ecma.appambulance.payload.UserDto;
import ai.ecma.appambulance.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/organization")
public class OrganizationController {
    @Autowired
    OrganizationService organizationService;

    @GetMapping("getAllOrganization")
    public List<Organization> getAll() {
        return organizationService.getAll();
    }

    @GetMapping("{id}")
    public Organization getById(@PathVariable UUID id) {
        return organizationService.getById(id);
    }

    @PostMapping("save")
    public HttpEntity<?> addOrganization(@RequestBody ResOrganization resOrganization) {
        ApiResponse apiResponse = organizationService.addOrganization(resOrganization);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);

    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editOrganization(@Valid @PathVariable UUID id, @RequestBody ResOrganization resOrganization) {
        ApiResponse apiResponse = organizationService.editOrganization(id, resOrganization);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteOrganization(@PathVariable UUID id) {
        ApiResponse delete = organizationService.delete(id);
        return ResponseEntity.status(delete.isSuccess() ? 200 : 409).body(delete);
    }

    @GetMapping("getOrganizationOfManagement/{managementId}")
    public List<Organization> getAllOrganizationOfManagement(@PathVariable UUID managementId) {
        return organizationService.getAllOrganization(managementId);
    }

}
