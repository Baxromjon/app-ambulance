package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Color;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ColorRepository extends JpaRepository<Color, Integer> {
    Boolean existsByNameAndCode(String name, String code);
}
