package ai.ecma.appambulance.entity;

/**
 * created by Baxromjon
 * 10.05.2021
 **/

import ai.ecma.appambulance.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Position extends AbsEntity {

    private String name;

    private String description;

    @ManyToOne(optional = false)
    private Organization organization;
}
