package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.MadeYear;
import ai.ecma.appambulance.service.MadeYearService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/madeYear")
public class MadeYearController {
    @Autowired
    MadeYearService madeYearService;
    @GetMapping("getAllMadeYear")
    public List<MadeYear> getMadeYear() {
        return madeYearService.getMadeYearService();
    }

    @PostMapping("save")
    public String addMadeYear(@RequestBody MadeYear madeYear) {
        return madeYearService.addMadeYearService(madeYear);
    }

    @GetMapping("/{id}")
    public MadeYear getMadeYearBuId(@PathVariable Integer id) {
        return madeYearService.getMadeYearByIdService(id);
    }

    @DeleteMapping("delete/{id}")
    public String deleteMadeYear(@PathVariable Integer id) {
        return madeYearService.deleteMadeYearService(id);
    }

    @PutMapping("edit/{id}")
    public String updateMadeYear(@PathVariable Integer id, @RequestBody MadeYear madeYear) {
        return madeYearService.updateMadeYearService(id, madeYear);
    }
}
