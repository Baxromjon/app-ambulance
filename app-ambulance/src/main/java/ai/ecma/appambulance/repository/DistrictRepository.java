package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.District;
import ai.ecma.appambulance.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DistrictRepository extends JpaRepository<District, UUID> {
    Boolean existsByNameAndRegion(String name, Region region);
}
