// import React, {Component} from 'react';
// import PropTypes from 'prop-types';
// import request from "../../utils/request";
// import api from "../../utils/api";
// import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
// import {AvField, AvForm} from 'availity-reactstrap-validation'
//
// class Car extends Component {
//     state = {
//         cars: [],
//         currentCar: '',
//         colors: [],
//         madeYear: [],
//         models: [],
//         drivers: [],
//         brands: [],
//         organizations: [],
//         currentBrand: '',
//         currentOrganization: '',
//         showModal: false,
//         showModalDelete: false
//     }
//
//     componentDidMount() {
//         this.getCars()
//         this.getColors()
//         this.getMadeYear()
//         this.getOrganization()
//         this.getModel()
//         this.getBrand()
//     }
//
//     getCars = () => {
//         request({
//             url: api.getCar,
//             method: 'GET'
//         }).then(res => {
//             this.setState({cars: res.data})
//         }).catch(err => {
//         })
//     }
//     getModel = () => {
//         request({
//             url: api.getModel,
//             method: 'GET'
//         }).then(res => {
//             this.setState({models: res.data})
//         }).catch(err => {
//         })
//     }
//     getBrand = () => {
//         request({
//             url: api.getAllBrand,
//             method: 'GET'
//         }).then(res => {
//             this.setState({brands: res.data})
//         }).catch(err => {
//         })
//     }
//     getColors = () => {
//         request({
//             url: api.getColor,
//             method: 'GET'
//         }).then(res => {
//             this.setState({colors: res.data})
//         }).catch(err => {
//         })
//     }
//     getMadeYear = () => {
//         request({
//             url: api.getMadeYear,
//             method: 'GET'
//         }).then(res => {
//             this.setState({madeYear: res.data})
//         }).catch(err => {
//         })
//     }
//     getOrganization = () => {
//         request({
//             url: api.getOrganization,
//             method: 'GET'
//         }).then(res => {
//             this.setState({organizations: res.data})
//         }).catch(err => {
//         })
//     }
//     getDriver = () => {
//         let current = this.state.currentOrganization;
//         request({
//             url: api.getDriver + '/' + current,
//             method: 'GET'
//         }).then(res => {
//             this.setState({drivers: res.data})
//         }).catch(err => {
//         })
//     }
//     getOneOrganization = (organization) => {
//         // this.setState({currentOrganization: organization.target.value})
//         request({
//             url: api.getDriver + '/' + organization.target.value,
//             method: 'GET'
//         }).then(res => {
//             this.setState({drivers: res.data})
//         }).catch(err => {
//         })
//     }
//     GetOneBrand = (brand) => {
//         this.setState({currentBrand: brand.target.value})
//     }
//     saveCar = (e, v) => {
//         let current = this.state.currentCar
//         request({
//             url: api.addCar,
//             method: 'POST',
//             data: v
//         }).then(res => {
//             this.getCars()
//             this.openModal()
//         }).catch(err => {
//         })
//     }
//     openModal = () => {
//         this.setState({showModal: true})
//     }
//     closeModal = () => {
//         this.setState({showModal: false})
//     }
//
//     render() {
//         const {cars, currentCar, colors, madeYear, models, organizations, drivers, brands, currentBrand} = this.state
//         return (
//             <div>
//                 <h1 className="text-center">car page</h1>
//                 <button className="btn btn-success"
//                         onClick={this.openModal}>+add Car
//                 </button>
//                 <hr/>
//                 <br/>
//                 <table className="table table-bordered text-center">
//                     <thead>
//                     <th>№</th>
//                     <th>Driver full Name</th>
//                     <th>car licence serial</th>
//                     <th>car licence serial number</th>
//                     <th>driver licence serial</th>
//                     <th>driver licence serial number</th>
//                     <th>reg state number</th>
//                     <th>color of car</th>
//                     <th>made year of car</th>
//                     <th>model of car</th>
//                     <th>organization name of car</th>
//                     </thead>
//                     {/*<tbody>*/}
//                     {/*{cars?.map((cars, index)=>(*/}
//                     {/*    <tr key={index}>*/}
//                     {/*        <td>{index+1}</td>*/}
//                     {/*        <td>{index+1}</td>*/}
//                     {/*    </tr>*/}
//                     {/*))}*/}
//                     {/*</tbody>*/}
//                 </table>
//                 <Modal isOpen={this.state.showModal}>
//                     <ModalHeader>Add Car</ModalHeader>
//                     <ModalBody>
//                         <AvForm onValidSubmit={this.saveCar}>
//                             <AvField
//                                 type="select"
//                                 name="organizationId"
//                                 onChange={this.getOneOrganization}
//                                 // onClick={this.getDriver}
//                             >
//                                 <option value="">Select Organization</option>
//                                 {organizations?.map(organization => (
//                                     <option value={organization.id}>{organization.name}</option>
//                                 ))}
//                             </AvField>
//                             <AvField
//                                 placeholder="regStateNumber here"
//                                 name="regStateNumber"
//                                 defaultValue={currentCar.regStateNumber}
//                             />
//                             <AvField
//                                 name="carLicensceSerial"
//                                 placeholder="car licence serial here"
//                                 defaultValue={currentCar.carLicenseSerial}/>
//                             <AvField
//                                 name="carLicenseSerialNumber"
//                                 placeholder="car licence serial number here"
//                                 defaultValue={currentCar.carLicenseSerialNumber}/>
//                             <AvField
//                                 type="select"
//                                 name="colorId">
//                                 <option value="">select Color</option>
//                                 {colors?.map(color => (
//                                     <option value={color.id}>{color.name}</option>
//                                 ))}
//                             </AvField>
//                             <AvField
//                                 type="select"
//                                 name="madeYearId">
//                                 <option value="">select made year of Car</option>
//                                 {madeYear?.map(madeYear => (
//                                     <option value={madeYear.id}>{madeYear.value}</option>
//                                 ))}
//                             </AvField>
//                             <AvField
//                                 type="select"
//                                 name="brandId"
//                                 onChange={(brand) => this.GetOneBrand(brand)}>
//                                 <option value="">select Brand of Car</option>
//                                 {brands?.map(brand => (
//                                     <option value={brand.id}>{brand.name}</option>
//                                 ))}
//                             </AvField>
//                             <AvField
//                                 type="select"
//                                 name="modelId">
//                                 <option value="">select model of Car</option>
//                                 {models?.map(model => (
//                                     model.brand.id == currentBrand ?
//                                         <option value={model.id}>{model.name}</option> : ""
//                                 ))}
//                             </AvField>
//                             <AvField
//                                 type="select"
//                                 name="driverId">
//                                 <option value="">select Driver</option>
//                                 {drivers?.map(driver => (
//                                     <option value={driver.id}>{driver.firstName + ' ' + driver.lastName}</option>
//                                 ))}
//                             </AvField>
//                             <AvField
//                                 name="driverLicenseSerial"
//                                 placeholder="Driver license SerialNumber here"
//                                 defaultValue={currentCar.driverLicenseSerial}/>
//                             <AvField
//                                 name="driverLicenseSerialNumber"
//                                 placeholder="Driver license Serial here"
//                                 defaultValue={currentCar.driverLicenseSerialNumber}/>
//
//                             <button className="btn btn-success">save</button>
//                             <button className="btn btn-danger"
//                                     onClick={this.closeModal}>cancel
//                             </button>
//                         </AvForm>
//                     </ModalBody>
//                 </Modal>
//
//             </div>
//         );
//     }
// }
//
// Car.propTypes = {};
//
// export default Car;

import React, {Component} from 'react';
import request from "../../utils/request";
import api from "../../utils/api";
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'
import {TOKEN} from "../../utils/constant";

class Car extends Component {
    state = {
        cars: [],
        organizations: [],
        models: [],
        madeYears: [],
        brands: [],
        colors: [],
        drivers: [],
        currentCar: '',
        currentBrand: '',
        currentOrganization: '',
        showModal: false,
        showModalDelete: false
    }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getCar()
            this.getOrganizations()
            this.getModels()
            this.getBrands()
            this.getMadeYear()
            this.getColor()
        }
    }

    openModal = () => {
        this.setState({showModal: true})
    }
    closeModal = () => {
        this.setState({showModal: false})
    }
    getCar = () => {
        request({
            url: api.getCar,
            method: 'GET'
        }).then(res => {
            this.setState({cars: res.data})
        }).catch(err => {
        })
    }
    getOrganizations = () => {
        request({
            url: api.getOrganization,
            method: 'GET'
        }).then(res => {
            this.setState({organizations: res.data})
        }).catch(err => {
        })
    }
    getModels = () => {
        request({
            url: api.getModel,
            method: 'GET'
        }).then(res => {
            this.setState({models: res.data})
        }).catch(err => {
        })
    }
    getBrands = () => {
        request({
            url: api.getAllBrand,
            method: 'GET'
        }).then(res => {
            this.setState({brands: res.data})
        }).catch(err => {
        })
    }
    saveCar = (e, v) => {
        request({
            url: api.addCar,
            method: 'POST',
            data: v
        }).then(res => {
            this.getCar()
            this.closeModal()
        }).catch(err => {
        })
    }
    getOneBrand = (brand) => {
        this.setState({currentBrand: brand.target.value})
    }
    getModelOfBrand = () => {
        let current = this.state.currentBrand
        request({
            url: api.getAllModelOfBrand + '/' + current,
            method: 'GET'
        }).then(res => {
            this.setState({models: res.data})
        }).catch(err => {
        })
    }
    getMadeYear = () => {
        request({
            url: api.getMadeYear,
            method: 'GET'
        }).then(res => {
            this.setState({madeYears: res.data})
        }).catch(err => {
        })
    }
    getColor = () => {
        request({
            url: api.getColor,
            method: 'GET'
        }).then(res => {
            this.setState({colors: res.data})
        }).catch(err => {
        })
    }
    getOneOrganization = (organization) => {
        console.log(organization.target.value)
        this.setState({currentOrganization: organization.target.value})
    }
    getDriver = () => {
        let current = this.state.currentOrganization
        console.log(current)
        request({
            url: api.getDriver + '/' + current,
            method: 'GET'
        }).then(res => {
            console.log("keldi")
            console.log(res.data)
            this.setState({drivers: res.data})
        }).catch(err => {
        })
    }

    render() {
        let cars = this.state.cars
        let organizations = this.state.organizations
        let brand = this.state.brands
        let models = this.state.models
        let years = this.state.madeYears
        let colors = this.state.colors
        let driver = this.state.drivers
        console.log(cars)
        return (
            <div>
                <h1 className="text-center">Car page</h1>
                <button className="btn btn-success"
                        onClick={this.openModal}>+add car
                </button>
                <hr/>
                <table className="table table-bordered">
                    <thead className="text-center">
                    <th>№</th>
                    <th>Organization name</th>
                    <th>Model name</th>
                    <th>Made year</th>
                    <th>Color</th>
                    <th>Reg state number</th>
                    <th>Driver Full Name</th>
                    <th>Driver licence serial</th>
                    <th>Driver licence serial number</th>
                    <th>Car licence serial</th>
                    <th>Car licence serial number</th>
                    <th>Online</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    {cars?.map((cars, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{cars.organization.name}</td>
                            <td>{cars.model.name}</td>
                            <td>{cars.madeYear.value}</td>
                            <td>{cars.color.name}</td>
                            <td>{cars.regStateNumber}</td>
                            <td>{"driverni ismi"}</td>
                            <td>{cars.driverLicenseSerial}</td>
                            <td>{cars.driverLicenseSerialNumber}</td>
                            <td>{cars.carLicenseSerial}</td>
                            <td>{cars.carLicenseSerialNumber}</td>
                            <td>{cars.online}</td>
                            <td>
                                <button className="btn btn-success">edit</button>
                                <button className="btn btn-danger"
                                >delete</button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>SAVE CAR</ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveCar}>
                            <AvField
                                type="select"
                                name="organizationId"
                                onChange={(organization) => this.getOneOrganization(organization)}>
                                <option value="">select Organization</option>
                                {organizations?.map(organization => (
                                    <option value={organization.id}>{organization.name}</option>
                                ))}
                            </AvField>
                            <AvField
                                type="select"
                                name="brandId"
                                onChange={(brand) => this.getOneBrand(brand)}>
                                <option value="">select Brand of Car</option>
                                {brand?.map(brand => (
                                    <option value={brand.id}>{brand.name}</option>
                                ))}
                            </AvField>
                            <AvField
                                type="select"
                                name="modelId"
                                onClick={this.getModelOfBrand}>
                                <option value="">select Model of Car</option>
                                {models?.map(model => (
                                    <option value={model.id}>{model.name}</option>
                                ))}
                            </AvField>
                            <AvField
                                type="select"
                                name="madeYearId">
                                <option value="">select made year of Car</option>
                                {years?.map(year => (
                                    <option value={year.id}>{year.value}</option>
                                ))}
                            </AvField>
                            <AvField
                                type="select"
                                name="colorId">
                                <option value="">select color of Car</option>
                                {colors?.map(color => (
                                    <option value={color.id}>{color.name}</option>
                                ))}
                            </AvField>
                            <AvField
                                name="regStateNumber"
                                placeholder="regStateNumber"
                                // defaultValue={this.state.currentCar.regStateNumber}
                                required/>
                            <AvField
                                type="select"
                                name="driverId"
                                onClick={this.getDriver}>
                                <option value="">select driver</option>
                                {driver?.map(driver => (
                                    <option value={driver.driverId}>{driver.firstName + ' ' + driver.lastName}</option>
                                ))}
                            </AvField>
                            <AvField
                                name="driverLicenseSerial"
                                placeholder="driver licence serial"
                                // defaultValue={this.state.currentCar.driverLicenceSerial}
                                required/>
                            <AvField
                                name="driverLicenseSerialNumber"
                                placeholder="driver licence serial number"
                                // defaultValue={this.state.currentCar.driverLicenceSerialNumber}
                                required/>
                            <AvField
                                name="carLicenseSerial"
                                placeholder="car license serial"
                                // defaultValue={this.state.currentCar.carLicenceSerial}
                                required/>
                            <AvField
                                name="carLicenseSerialNumber"
                                placeholder="car license serial number"
                                // defaultValue={this.state.currentCar.carLicenceSerialNumber}
                                required/>
                            <button className="btn btn-success">save</button>
                            <button className="btn btn-danger"
                                    onClick={this.closeModal}>cancel
                            </button>
                        </AvForm>

                    </ModalBody>

                </Modal>

            </div>
        );
    }
}

Car.propTypes = {};

export default Car;
