package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Color;
import ai.ecma.appambulance.entity.Country;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResCountry;
import ai.ecma.appambulance.repository.CountryRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CountryService {
    @Autowired
    CountryRepository countryRepository;

    public List<Country> getAllCountry() {
        return countryRepository.findAll();
    }

    public ApiResponse addCountry(ResCountry resCountry) {
        Boolean exists = countryRepository.existsByName(resCountry.getName());
        if (exists){
            return new ApiResponse(false,"allready exists" );
        }
        Country country=new Country(resCountry.getName());
        countryRepository.save(country);
        return new ApiResponse(true,"Country added");

    }

    public ApiResponse editCountry(UUID id, ResCountry resCountry) {
        Optional<Country> optional = countryRepository.findById(id);
        if (!optional.isPresent())
            return new ApiResponse(false,"Country not exists" );

        Country country=optional.get();
        country.setName(resCountry.getName());
        countryRepository.save(country);
        return new ApiResponse(true,"Country edited");
    }

    public ApiResponse deleteCountry(UUID id) {
        countryRepository.deleteById(id);
        return new ApiResponse(true,"Country deleted");
    }

    public Country getCountryById(UUID id) {
        Optional<Country> optional = countryRepository.findById(id);
        if (!optional.isPresent())
            return new Country();
        return optional.get();
    }
}
