package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Country;
import ai.ecma.appambulance.entity.Region;
import ai.ecma.appambulance.projection.DistrictObject;
import ai.ecma.appambulance.projection.RegionObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface RegionRepository extends JpaRepository<Region, UUID> {
    Boolean existsByNameAndCountry(String name, Country country);

    @Query(nativeQuery = true, value = "select cast (r.id as varchar ) as id, r.name as name from region r\n" +
            "inner join country c on r.country_id = c.id\n" +
            "where c.id=:id")
    List<RegionObject> getAllRegionOfCountry(UUID id);

    @Query(nativeQuery = true, value = "select cast(d.id as varchar) as id, d.name from district d\n" +
            "inner join region r on d.region_id = r.id\n" +
            "where r.id=:id")
    List<DistrictObject> getAllDistrictOfRegion(UUID id);
}
