import React, {Component} from 'react';
import {ToastContainer} from "react-toastify";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Login from "./pages/Login";
import Country from "./pages/admin/country";
import Region from "./pages/admin/region";
import District from "./pages/admin/district";
import Address from "./pages/admin/address";
import Register from "./pages/admin/Register";
import Cabinet from "./pages/admin/Cabinet";
import Verification from "./pages/admin/Verification";
import Home from "./pages/Home";
import Ministry from "./pages/admin/Ministry";
import Management from "./pages/admin/Management";
import Organization from "./pages/admin/Organization";
import Car from "./pages/admin/Car";
import TimeTableDashboard from "./pages/admin/TimeTableDashboard";
import Position from "./pages/organization/position";
import Territory from "./pages/admin/Territory";
import MedicineFarm from "./pages/organization/MedicineFarm";
import MedicineType from "./pages/organization/MedicineType";
import Medicine from "./pages/organization/Medicine";


class App extends Component {
    render() {
        return (
            <div className="container pt-3">
                <ToastContainer/>
                <Router>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/login" component={Login}/>
                        <Route exact path="/country" component={Country}/>
                        <Route exact path="/region" component={Region}/>
                        <Route exact path="/district" component={District}/>
                        <Route exact path="/address" component={Address}/>
                        <Route exact path="/register" component={Register}/>
                        <Route exact path="/cabinet" component={Cabinet}/>
                        <Route exact path="/verification" component={Verification}/>
                        <Route exact path="/ministry" component={Ministry}/>
                        <Route exact path="/management" component={Management}/>
                        <Route exact path="/organization" component={Organization}/>
                        <Route exact path="/car" component={Car}/>
                        <Route exact path="/position" component={Position}/>
                        <Route exact path="/timeTableDashboard" component={TimeTableDashboard}/>
                        <Route exact path="/territory" component={Territory}/>
                        <Route exact path="/medicineFarm" component={MedicineFarm}/>
                        <Route exact path="/medicineType" component={MedicineType}/>
                        <Route exact path="/medicine" component={Medicine}/>
                    </Switch>
                </Router>

            </div>
        );
    }
}

App.propTypes = {};

export default App;