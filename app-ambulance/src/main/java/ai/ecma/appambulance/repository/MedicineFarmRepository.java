package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.MedicineFarm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MedicineFarmRepository extends JpaRepository<MedicineFarm, UUID> {
    Boolean existsByName(String name);
}
