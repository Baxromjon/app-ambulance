import React, {Component} from 'react';
import {AvField, AvForm} from "availity-reactstrap-validation";
import request from "../../utils/request";
import api from "../../utils/api";
import {TOKEN} from "../../utils/constant";

class Register extends Component {
    state = {
        organizations: [],
        organizationRoles: [],
        permissionList: [],
        roles: [],
        managements: [],
        currentManagement: ''

    }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getRoles()
            this.getManagement()
        }
    }

    getManagement = () => {
        request({
            url: api.getManagements,
            method: 'GET'
        }).then(res => {
            this.setState({managements: res.data})
        }).catch(err => {
        })
    }
    getOrganization = () => {
        let current = this.state.currentManagement
        console.log(current)
        request({
            url: api.getAllOrganization + '/' + current,
            method: 'GET'
        }).then(res => {
            console.log(res.data)
            this.setState({organizations: res.data})
        }).catch(err => {
        })
    }
    getCurrentManagement = (management) => {
        this.setState({currentManagement: management.target.value})
    }

    getOrganizationRoles = (e) => {
        let id = e.target.value;
        let organizationArr = this.state.organizations;
        let length = organizationArr.length;
        for (let i = 0; i < length; i++) {
            if (id === organizationArr[i].id) {
                this.setState({
                    organizationRoles: organizationArr[i].organizationRoles,
                    permissionList: organizationArr[i].permissionList,
                })
            }
        }
    }


    getRoles = () => {
        request({
            url: api.getOrganizationRoles,
            method: 'GET'
        }).then(res => {
            this.setState({roles: res.data})
        }).catch(err => {
            alert(err.data.message)
        })
    }
    register = (e, v) => {
        request({
            url: api.register,
            method: 'POST',
            data: v
        }).then(res => {
            alert(res.data.message);
            this.props.history.push('/')
        }).catch(err => {
            alert(err.response.data.message)
        })
    }


    render() {
        const {managements, organizations, permissionList} = this.state;
        return (
            <div>
                <h1 className="text-center">Register</h1>
                <AvForm onValidSubmit={this.register}>
                    <AvField
                        placeholder="Name"
                        validate={{
                            required: {value: true, errorMessage: 'Please, enter your name'}
                        }}
                        name="firstName"/>
                    <AvField
                        placeholder="Last name"
                        validate={{
                            required: {value: true, errorMessage: 'Enter your last name'}
                        }}
                        name="lastName"/>
                    <AvField
                        placeholder="passport serial"
                        name="passportSerial"/>
                    <AvField
                        placeholder="passport serial number"
                        name="passportSerialNumber"/>
                    <AvField
                        placeholder="Phone number"
                        validate={{
                            required: {value: true, errorMessage: 'Please enter your phone number'}
                        }}
                        name="phoneNumber"/>
                    <AvField
                        type="select"
                        name="managementId"
                        onChange={(management) => this.getCurrentManagement(management)}
                        onClick={this.getOrganization}
                    >
                        <option value="">Select Management</option>
                        {managements?.map(management => (
                            <option value={management.id}>{management.name}</option>
                        ))}
                    </AvField>
                    <AvField
                        type="select"
                        name="organizationId"
                        onChange={this.getOrganizationRoles}
                    >
                        <option value="">Select Organization</option>
                        {organizations?.map(organization => (
                            // organization.management.id == this.state.managements.id ?
                            <option value={organization.id}>{organization.name}</option>
                        ))}
                    </AvField>
                    <AvField
                        type="select"
                        name="organizationRoleId"
                    >
                        <option value="">Select Role</option>
                        {this.state.organizationRoles?.map(role => (
                            <option value={role.id}>{role.name}</option>
                        ))}
                    </AvField>
                    <AvField
                        type="select"
                        name="permissionsId" multiple>
                        <option value="">Select Permission</option>
                        {permissionList?.map(permissions => (
                            <option value={permissions.id}>{permissions.organizationPermissionEnum}</option>
                        ))}
                    </AvField>


                    <button className={"btn btn-success"}>Register
                    </button>
                    {/*<button className={"btn btn-primary"}>Login</button>*/}
                </AvForm>
            </div>
        );
    }
}

export default Register;