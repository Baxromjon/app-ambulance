package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Organization;
import ai.ecma.appambulance.entity.Position;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PositionRepository extends JpaRepository<Position, UUID> {
    Boolean existsByNameAndOrganization(String name, Organization organization);
}
