import React, {Component} from 'react';
import PropTypes from 'prop-types';
import request from "../../utils/request";
import api from "../../utils/api";
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'
import {TOKEN} from "../../utils/constant";

class Position extends Component {
    state = {
        position: [],
        currentPosition: '',
        organization: [],
        currentOrganization: '',
        showModal: false,
        showModalDelete: false
    }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getAllPosition()
            this.getOrganization()
        }
    }

    getAllPosition = () => {
        request({
            url: api.getAllPosition,
            method: 'GET'
        }).then(res => {
            console.log(res.data)
            this.setState({position: res.data})
        }).catch(err => {
        })
    }

    getOrganization = () => {
        request({
            url: api.getOrganization,
            method: 'GET'
        }).then(res => {
            console.log(res.data)
            this.setState({organization: res.data})
        }).catch(err => {
        })
    }

    modal = () => {
        this.setState({showModal: !this.state.showModal})
    }

    render() {
        const {position, organization, currentPosition, currentOrganization} = this.state;
        return (
            <div>
                <h1>Position List</h1>
                <hr/>
                <br/>
                <button className="btn btn-success"
                        onClick={this.modal}>+add position
                </button>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {position?.map((position, index) =>
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{position.name}</td>
                            <td>
                                <button className="btn btn-info">edit</button>
                                <button className="btn btn-danger">delete</button>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </table>
                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>add Position</ModalHeader>
                    <ModalBody>
                        <AvForm>
                            <AvField
                                name="name"
                                placeholder="position name here"
                                defaultValue={this.state.currentPosition.name}
                            />
                            <AvField
                                type="select"
                                name="organizationId">
                                <option value="">select Organization</option>
                                {organization?.map(organization => (
                                    <option value={organization.id}>{organization.name}</option>
                                ))}
                            </AvField>
                            <button className="btn btn-success">save</button>
                            <button className="btn btn-danger"
                                    onClick={this.modal}>cancel
                            </button>
                        </AvForm>
                    </ModalBody>

                </Modal>
            </div>
        );
    }
}

Position.propTypes = {};

export default Position;