package ai.ecma.appambulance.controller;
/**
 * created by Baxromjon
 * 28.05.2021
 **/

import ai.ecma.appambulance.entity.Territory;
import ai.ecma.appambulance.entity.template.AbsNameEntity;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.TerritoryDto;
import ai.ecma.appambulance.service.TerritoryService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Entity;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("api/territory")


public class TerritoryController extends AbsNameEntity {
    @Autowired
    TerritoryService territoryService;

    @GetMapping("getAll")
    public List<Territory> getAll() {
        return territoryService.getAll();
    }

    @GetMapping("getById/{id}")
    public Territory getById(@PathVariable UUID id) {
        return territoryService.getById(id);
    }

    @PostMapping("addTerritory")
    public HttpEntity<?> addTerritory(@RequestBody TerritoryDto territoryDto) {
        ApiResponse apiResponse = territoryService.addTerritory(territoryDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editTerritory(@PathVariable UUID id, @RequestBody TerritoryDto territoryDto) {
        ApiResponse apiResponse = territoryService.editTerritory(id, territoryDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteTerritory(@PathVariable UUID id) {
        ApiResponse delete = territoryService.delete(id);
        return ResponseEntity.status(delete.isSuccess() ? 200 : 409).body(delete);
    }

}
