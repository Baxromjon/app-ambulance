package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.District;
import ai.ecma.appambulance.entity.MedicineFarm;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResMedicineFarm;
import ai.ecma.appambulance.repository.DistrictRepository;
import ai.ecma.appambulance.repository.MedicineFarmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MedicineFarmService {
    @Autowired
    MedicineFarmRepository medicineFarmRepository;
    @Autowired
    DistrictRepository districtRepository;

    public List<MedicineFarm> getAllFarm() {
        return medicineFarmRepository.findAll();
    }

    public MedicineFarm getById(UUID id) {
        Optional<MedicineFarm> byId = medicineFarmRepository.findById(id);
        if (!byId.isPresent()) {
            return new MedicineFarm();
        }
        return byId.get();
    }

    public ApiResponse addFarm(ResMedicineFarm resMedicineFarm) {
        Optional<District> addressOptional = districtRepository.findById(resMedicineFarm.getDistrictId());
        Boolean exists = medicineFarmRepository.existsByName(resMedicineFarm.getName());
        if (exists) {
            return new ApiResponse(false, "Medicine farm allReady exists");
        }
        MedicineFarm medicineFarm = new MedicineFarm(resMedicineFarm.getName(),resMedicineFarm.getDescription(), addressOptional.get(),resMedicineFarm.getStreet(), resMedicineFarm.getPhoneNumber());
        medicineFarmRepository.save(medicineFarm);
        return new ApiResponse(true, "Medicine Farm added");
    }

    public ApiResponse editFarm(UUID id, ResMedicineFarm resMedicineFarm) {
        Optional<District> addressOptional = districtRepository.findById(resMedicineFarm.getDistrictId());
        Optional<MedicineFarm> optional = medicineFarmRepository.findById(id);
        if (!optional.isPresent()) {
            return new ApiResponse(false, "Medicine Farm not Found");
        }
        MedicineFarm medicineFarm = optional.get();
        medicineFarm.setName(resMedicineFarm.getName());
        medicineFarm.setDescription(resMedicineFarm.getDescription());
        medicineFarm.setDistrict(addressOptional.get());
        medicineFarm.setPhoneNumber(resMedicineFarm.getPhoneNumber());
        medicineFarmRepository.save(medicineFarm);
        return new ApiResponse(true, "Medicine Farm edited");
    }

    public ApiResponse deleteFarm(UUID id) {
        medicineFarmRepository.deleteById(id);
        return new ApiResponse(true,"Farm deleted");
    }
}
