package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.TimeTable;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.TimeTableDTO;
import ai.ecma.appambulance.service.TimeTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/timeTable")
public class TimeTableController {
    @Autowired
    TimeTableService timeTableService;

    @GetMapping("getAllTimeTable")
    public List<TimeTable> getAll() {
        return timeTableService.getAll();
    }

    @PostMapping("saveTimeTableForDriver")
    public HttpEntity<?> addTimeTable(@Valid @RequestBody TimeTableDTO timeTableDTO) throws ParseException {
        ApiResponse apiResponse = timeTableService.addTimeTable(timeTableDTO);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }
}
