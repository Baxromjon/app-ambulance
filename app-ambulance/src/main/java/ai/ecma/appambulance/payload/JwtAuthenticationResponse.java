package ai.ecma.appambulance.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtAuthenticationResponse {
    private String accessToken;
    private UUID deviceKey;
    private String tokenType="Bearer";
    private String refreshToken;

    public JwtAuthenticationResponse(String accessToken, UUID deviceKey, String refreshToken) {
        this.accessToken = accessToken;
        this.deviceKey = deviceKey;
        this.refreshToken = refreshToken;
    }
}
