package ai.ecma.appambulance.service;

import ai.ecma.appambulance.payload.ApiResponse;
import okhttp3.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class SmsService {
    private static final String BASE_URL = "https://portal.bulkgate.com/api/1.0/simple/";
    private static final String APPLICATION_ID = "11676";
    private static final String APPLICATION_TOKEN = "La5ILIhVQQuEReJFgn8Fiz9zClqXFUsyqXS8tafEOvipaQEJiL";
    private static final SmsService instance = null;

    private SmsService() {
    }

    public static SmsService getInstance() {
        if (instance == null) return new SmsService();
        return instance;
    }

    @Transactional
    public ApiResponse sendMessage(List<String> phones, String message) throws IOException {
        List<String> uzbPhones = new ArrayList<>();
        List<String> otherPhones = new ArrayList<>();
        for (String phone : phones) {
            if (phone.startsWith("+998") || phone.startsWith("998")) {
                uzbPhones.add(phone);
            } else {
                otherPhones.add(phone);
            }
        }
        boolean uzb = false;
        if (uzbPhones.size() > 0) {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED);
            List<String> p = new ArrayList<>();
            for (String uzbPhone : uzbPhones) {
                if (uzbPhone.startsWith("+")) uzbPhone = uzbPhone.substring(1);
                p.add("{\"phone\":\"" + uzbPhone + "\",\"text\":\"" + message + "\"}");
            }
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("login", "inflex");
            map.add("password", "dTSt2QFCEfoU");
            map.add("data", "[" + String.join(",", p) + "]");
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            ResponseEntity<String> response = restTemplate.postForEntity("http://83.69.139.182:8083",
                    request, String.class);
//            System.out.println(response);
            if (response.getBody() != null && !response.getBody().contains("error") && !response.getStatusCode().isError())
                uzb = true;
        }
        boolean other = false;
        if (otherPhones.size() > 0) {
            Response response = httpClient("promotional", "{\"application_id\":\"" + APPLICATION_ID + "\",\"application_token\":\"" + APPLICATION_TOKEN + "\",\"number\":\"" + String.join(";", otherPhones) + "\",\"text\":\"" + message + "\"}", "POST");
            if (response.code() == 200) other = true;
        }
        return new ApiResponse(uzb || other);
    }

    public ApiResponse checkCreditBalance() throws Exception {
        try {
            Response response = httpClient("info", "{\"application_id\":\"" + APPLICATION_ID + "\",\"application_token\":\"" + APPLICATION_TOKEN + "\"}", "POST");
            return new ApiResponse(response.code() == 200, response.body().string());
        } catch (IOException e) {
            throw new Exception("Tizim xatoligi", e);
//            e.printStackTrace();
//            return new ApiResponse(false, "Tizim xatoligi");
        }
    }

    private Response httpClient(String url, String data, String method) throws IOException {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, data);

        Request request = new Request.Builder()
                .url(BASE_URL + url)
                .method(method, body)
                .addHeader("Content-Type", "application/json")
                .build();
        return client.newCall(request).execute();
    }

}
