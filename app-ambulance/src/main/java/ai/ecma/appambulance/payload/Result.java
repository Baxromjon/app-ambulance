package ai.ecma.appambulance.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private String message;
    private boolean success;
    private UUID userId;

    public Result(String message, boolean success) {
        this.message = message;
        this.success = success;
    }

    public Result(String message, UUID userId) {
        this.message = message;
        this.userId = userId;
    }
}
