package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.PhoneAuth;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import javax.transaction.Transactional;
import java.util.UUID;

public interface PhoneAuthRepository extends JpaRepository<PhoneAuth, UUID> {
    Boolean existsByPhoneNumber(String phoneNumber);

    @Modifying
    @Transactional
    void deleteAllByPhoneNumber(String phoneNumber);
}
