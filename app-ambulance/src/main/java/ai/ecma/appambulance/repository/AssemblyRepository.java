package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Assembly;
import ai.ecma.appambulance.entity.District;
import ai.ecma.appambulance.entity.Territory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface AssemblyRepository extends JpaRepository<Assembly, UUID> {
    Boolean existsAssembliesByNameAndTerritory(String name, Territory territory);
}
//