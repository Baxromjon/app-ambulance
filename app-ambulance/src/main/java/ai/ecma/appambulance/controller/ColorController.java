package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Color;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResColor;
import ai.ecma.appambulance.service.ColorService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/color")
public class ColorController {
    @Autowired
    ColorService colorService;


    @GetMapping("getAllColor")
    public List<Color> getAllColor() {
        return colorService.getAllColor();
    }

    @PostMapping("save")
    public HttpEntity<?> addColor(@RequestBody ResColor resColor) {
        ApiResponse apiResponse = colorService.addColor(resColor);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editColor(@PathVariable Integer id, @RequestBody ResColor resColor) {
        ApiResponse apiResponse = colorService.editColor(id, resColor);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteColor(@PathVariable Integer id) {
        ApiResponse apiResponse = colorService.deleteColor(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("{id}")
    public Color getColorById(@PathVariable Integer id){
        return colorService.getColorById(id);
    }
}
