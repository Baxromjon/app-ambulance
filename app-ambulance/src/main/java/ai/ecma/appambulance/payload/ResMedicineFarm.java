package ai.ecma.appambulance.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResMedicineFarm {
    @NotBlank
    private String name;
    private String description;
    @NotNull
    private UUID districtId;
    private String street;
    @NotBlank
    private String phoneNumber;
}
