package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Organization;
import ai.ecma.appambulance.entity.OrganizationRole;
import ai.ecma.appambulance.entity.User;
import ai.ecma.appambulance.projection.DriverProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByEmail(String email);
    Boolean existsByPhoneNumber(String phoneNumber);
    Boolean existsByFirstNameAndLastNameAndPhoneNumber(String firstName, String lastName, String phoneNumber);
//    Boolean existsByFirstNameAndLastNameAndOrganizationRole(String firstName, String lastName, OrganizationRole organizationRole);
//    Boolean existsByOrganizationRoleAndOrganization(OrganizationRole organizationRole, List<Organization> organization);

    Optional<User> findByPhoneNumberAndRolesIn(String phoneNumber, Set<OrganizationRole> roles);

    @Query(value = "select *\n" +
            "                        from users\n" +
            "                        where phone_number = :phoneNumber\n" +
            "                          and id in (\n" +
            "                            select user_id\n" +
            "                            from users_roles\n" +
            "                            where user_id = id\n" +
            "                              and system_role_id\n" +
            "                                in (select id from role where  role_name <> 'ROLE_USER'))", nativeQuery = true)
    Optional<User> findFirstByPhoneNumberAndRolesNotIn(@Param("phoneNumber") String phoneNumber);

    @Query(value = "SELECT cast(u.id as varchar) as driverId, u.first_name as firstName, u.last_name as lastName from users u\n" +
            "            inner join organization_role o on o.id = u.organization_role_id\n" +
            "            left join organization_organization_role oor on o.id = oor.organization_role_id\n" +
            "            where o.name='ROLE_DRIVER'", nativeQuery = true)
    List<DriverProjection> getAllDriver();


    Optional<User> findByPhoneNumber(String phoneNumber);
    Optional<User> findByPhoneNumberAndCodeAndEnabledFalse(String phoneNumber, String code);

    @Query(nativeQuery = true, value = "select cast(u.id as varchar) as userId, u.first_name as firstName, u.last_name as lastName\n" +
            "from users u\n" +
            "         inner join users_organizations uo on u.id = uo.user_id\n" +
            "where uo.organization_id = :organizationId\n" +
            "  and u.organization_role_id=:roleId")
    List<User> getAllUser(UUID organizationId, UUID roleId);
}
