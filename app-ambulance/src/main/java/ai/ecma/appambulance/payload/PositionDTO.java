package ai.ecma.appambulance.payload;

/**
 * created by Baxromjon
 * 10.05.2021
 **/

import ai.ecma.appambulance.entity.Organization;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PositionDTO {
    private String name;
    private String description;
    private UUID organizationId;
}
