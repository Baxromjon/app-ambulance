package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.MadeYear;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MadeYearRepository extends JpaRepository<MadeYear, Integer> {
}
