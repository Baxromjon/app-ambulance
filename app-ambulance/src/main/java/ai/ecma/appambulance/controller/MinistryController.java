package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Ministry;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResMinistry;
import ai.ecma.appambulance.service.MinistryService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/ministry")
public class MinistryController {
    @Autowired
    MinistryService ministryService;

    @GetMapping("getAllMinistry")
    public List<Ministry> getAll() {
        return ministryService.getAll();
    }

    @GetMapping("{id}")
    public Ministry getById(@PathVariable UUID id) {
        return ministryService.getById(id);
    }

    @PostMapping("save")
    public HttpEntity<?> addMinistry(@Valid @RequestBody ResMinistry resMinistry) {
        ApiResponse apiResponse = ministryService.addMinistry(resMinistry);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteMinistry(@PathVariable UUID id) {
        ApiResponse delete = ministryService.delete(id);
        return ResponseEntity.status(delete.isSuccess() ? 200 : 409).body(delete);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editMinistry(@Valid @PathVariable UUID id, @RequestBody ResMinistry resMinistry) {
        ApiResponse edit = ministryService.edit(id, resMinistry);
        return ResponseEntity.status(edit.isSuccess() ? 200 : 409).body(edit);
    }

    @GetMapping("getMinistryOfCountry/{id}")
    public List<?> getMinistryOfCountry(@PathVariable UUID id) {
        return ministryService.getMinistry(id);
    }

}
