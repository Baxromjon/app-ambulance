package ai.ecma.appambulance.controller;

/**
 * created by Baxromjon
 * 10.05.2021
 **/

import ai.ecma.appambulance.entity.Position;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.PositionDTO;
import ai.ecma.appambulance.service.PositionService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/position")
public class PositionController {
    @Autowired
    PositionService positionService;

    @GetMapping("getAllPosition")
    public List<Position> getAllPosition() {
        return positionService.getAllPosition();
    }

    @GetMapping("getById/{id}")
    public Position getById(@PathVariable UUID id) {
        return positionService.getById(id);
    }

    @PostMapping("savePosition")
    public HttpEntity<?> savePosition(@RequestBody PositionDTO positionDTO) {
        ApiResponse apiResponse = positionService.savePosition(positionDTO);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("editPosition/{id}")
    public HttpEntity<?> editPosition(@PathVariable UUID id, @RequestBody PositionDTO positionDTO) {
        ApiResponse apiResponse = positionService.editPosition(id, positionDTO);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("deletePosition/{id}")
    public HttpEntity<?> deletePosition(@PathVariable UUID id) {
        ApiResponse apiResponse = positionService.deletePosition(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}
