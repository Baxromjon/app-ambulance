package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Country;
import ai.ecma.appambulance.entity.Ministry;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResMinistry;
import ai.ecma.appambulance.projection.MinistryProjection;
import ai.ecma.appambulance.repository.CountryRepository;
import ai.ecma.appambulance.repository.MinistryRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MinistryService {
    @Autowired
    MinistryRepository ministryRepository;
    @Autowired
    CountryRepository countryRepository;

    public List<Ministry> getAll() {
        return ministryRepository.findAll();
    }

    public Ministry getById(UUID id) {
        Optional<Ministry> byId = ministryRepository.findById(id);
        return byId.get();
    }

    public ApiResponse addMinistry(ResMinistry resMinistry) {
        Country country = countryRepository.findById(resMinistry.getCountryId()).orElseGet(Country::new);
        Boolean exists = ministryRepository.existsByNameAndCountry(resMinistry.getName(),country);
        if (exists){
            return new ApiResponse(false, "Ministry allReady exists");
        }
        Ministry ministry=new Ministry(resMinistry.getName(),country);
        ministryRepository.save(ministry);
        return new ApiResponse(true,"Ministry successfully added");
    }

    public ApiResponse delete(UUID id) {
        try {
            ministryRepository.deleteById(id);
            return new ApiResponse(true,"Ministry deleted");
        }catch (Exception e){
            return new ApiResponse(false,"not found");
        }
    }

    public ApiResponse edit(UUID id, ResMinistry resMinistry) {
        Country country = countryRepository.findById(resMinistry.getCountryId()).orElseGet(Country::new);
        Optional<Ministry> optional = ministryRepository.findById(id);
        if (!optional.isPresent()){
            return new ApiResponse(false, "Ministry not found");
        }
        Ministry ministry=optional.get();
        ministry.setName(resMinistry.getName());
        ministry.setCountry(country);
        ministryRepository.save(ministry);
        return new ApiResponse(true, "successfully edited");
    }

    public List<MinistryProjection> getMinistry(UUID id) {
        List<MinistryProjection> allMinistryOfCountry = ministryRepository.getAllMinistryOfCountry(id);
        return allMinistryOfCountry;
    }
}
