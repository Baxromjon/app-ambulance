package ai.ecma.appambulance.payload;

import ai.ecma.appambulance.entity.Organization;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResOrganizationRole {
    private String name;
    private String description;
    private UUID organizationId;
}
