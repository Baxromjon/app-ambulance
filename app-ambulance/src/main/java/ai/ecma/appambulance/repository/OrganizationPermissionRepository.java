package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.OrganizationPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface OrganizationPermissionRepository extends JpaRepository<OrganizationPermission, UUID> {

    @Query(nativeQuery = true, value = "select * from organization_permission\n" +
            "where id in (:permissionId)")
    List<OrganizationPermission> getAllById(List<UUID> permissionId);

}
