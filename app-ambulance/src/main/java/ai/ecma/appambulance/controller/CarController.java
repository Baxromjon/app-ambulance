package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Car;
import ai.ecma.appambulance.entity.User;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResCar;
import ai.ecma.appambulance.security.CurrentUser;
import ai.ecma.appambulance.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/car")
public class CarController {
    @Autowired
    CarService carService;

    @GetMapping("getAllCar")
    public List<Car> getAll() {
        return carService.getAll();
    }

    @GetMapping("{id}")
    public Car getById(@PathVariable UUID id) {
        return carService.getById(id);
    }

//    @PreAuthorize("hasAnyRole('ROLE_PERSONNEL_DEPARTMENT')")
    @PostMapping("save")
    public HttpEntity<?> addCar(@RequestBody ResCar resCar) {
        ApiResponse apiResponse = carService.addCar(resCar);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

//    @PreAuthorize("hasAnyRole('ROLE_PERSONNEL_DEPARTMENT')")
    @DeleteMapping("delete/{id}")
    public HttpEntity<?> delete(@PathVariable UUID id) {
        ApiResponse delete = carService.delete(id);
        return ResponseEntity.status(delete.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(delete);
    }

//    @PreAuthorize("hasAnyRole('ROLE_PERSONNEL_DEPARTMENT')")
    @PutMapping("edit/{id}")
    public HttpEntity<?> editCar(@PathVariable UUID id, @RequestBody ResCar resCar) {
        ApiResponse edit = carService.edit(id, resCar);
        return ResponseEntity.status(edit.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(edit);
    }

//    @PreAuthorize("hasAnyRole('ROLE_DRIVER')")
    @PutMapping("changeOnline")
    public HttpEntity<?> changeOnline(@CurrentUser User user) throws Exception {
        ApiResponse apiResponse = carService.changeOnline(user);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

//    @PreAuthorize("hasAnyRole('ROLE_DRIVER')")
    @PutMapping("updateLocation/{lat}/{lon}")
    public HttpEntity<?> updateLocation(@CurrentUser User user, @PathVariable Float lat, @PathVariable Float lon) {
        ApiResponse apiResponse = carService.updateLocation(user, lat, lon);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }
    @GetMapping("getCarByOrganizationId/{organizationId}")
    public List<Car> getByOrganizationId(@PathVariable UUID organizationId){
       return carService.getByOrganizationId(organizationId);
    }
}
