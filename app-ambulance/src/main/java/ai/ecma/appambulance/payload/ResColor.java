package ai.ecma.appambulance.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResColor {
    private String name;
    private String code;
    private String description;
}
