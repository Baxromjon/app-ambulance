import React, {Component} from 'react';
import request from "../../utils/request";
import api from "../../utils/api";
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import {TOKEN} from "../../utils/constant";

class Management extends Component {
    state = {
        managements: [],
        ministries: [],
        countries: [],
        currentCountry: '',
        currentManagement: '',
        currentMinistry: '',
        showModal: false,
        showModalDelete: false,
    }

    componentDidMount() {
        // if (!localStorage.getItem(TOKEN)) {
        //     this.props.history.push('/login')
        // } else {
            this.getManagements()
            this.getMinistries()
            this.getCountries()
        // }
    }
    getCountries = () => {
        request({
            url: api.getCountry,
            method: 'GET'
        }).then(ans => {
            this.setState({countries: ans.data})
        }).catch(err => {
        })
    }
    getCountriess = (item) => {
        console.log(item.target.value)
        this.setState({currentCountry: item.target.value})
    }
    getMinistries = () => {
        request({
            url: api.getMinistry,
            method: 'GET'
        }).then(ans => {
            console.log(ans)
            this.setState({ministries: ans.data})
        }).catch(res => {
            alert("xatolik")
        })
    }
    hideDeleteModal = () => {
        this.setState({
            showModalDelete: false,
            currentManagement: ''
        })
    }
    getManagements = () => {
        console.log(this.state.currentCountry)
        request({
            url: api.getManagements,
            method: 'GET'
        }).then(ans => {
            console.log(ans)
            this.setState({managements: ans.data})
        }).catch(res => {
            alert("xatolik")
        })
    }
    getMinistryOfCountry = () => {
        let current = this.state.currentCountry
        console.log(current)
        console.log(this.state.currentCountry)
        request({
            url: api.getMinistryOfCountry + '/' + current.id,
            method: 'GET'
        }).then(ans => {
            console.log(ans)
            this.setState({
                ministries: ans.data.object
            })
        }).catch(res => {
            alert('Xatolik')
        })
    }
    saveManagement = (e, v) => {
        console.log(v)
        let current = this.state.currentManagement
        request({
            url: current ? (api.editManagement + '/' + current.id) : api.addManagement,
            method: current ? 'PUT' : 'POST',
            data: v
        }).then(ans => {
            this.getManagements()
            this.closeModal()
        }).catch(res => {
            alert("xatolik")
        })
    }
    deleteManagement = () => {
        let current = this.state.currentManagement
        request({
            url: api.deleteManagement + '/' + current.id,
            method: 'DELETE'
        }).then(ans => {
            this.hideDeleteModal()
            this.getManagements()
        }).catch(res => {
            alert("error")
        })
    }
    openModal = () => {
        this.setState({
            showModal: true
        })
    }
    closeModal = () => {
        this.setState({
            showModal: false
        })
    }
    editModal = (management) => {
        this.openModal()
        this.setState({
            currentManagement: management
        })
    }
    deleteModal = (management) => {
        this.setState({
            currentManagement: management,
            showModalDelete: true
        })
    }
    render() {
        console.log(this.state.ministries)
        return (
            <div>
                <h1>Management List</h1>
                <hr/>
                <br/>
                <button className="btn btn-success"
                        onClick={this.openModal}>add Management
                </button>
                <br/>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Management name</th>
                        <th>Ministry name</th>
                        <th>Option</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.managements?.map((management, index) =>
                        <tr key={index}>
                            <th>{index + 1}</th>
                            <td>{management.name}</td>
                            <td>{management.ministry.name}</td>
                            <td>
                                <button className="btn btn-success" onClick={() => this.editModal(management)}>edit
                                </button>
                                <button className="btn btn-danger" onClick={() => this.deleteModal(management)}>delete
                                </button>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </table>

                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>
                        {this.state.currentManagement ? 'Edit Management' : 'Add Management'}
                    </ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveManagement}>
                            <AvField
                                defaultValue={this.state.currentManagement.name}
                                name="name" required/>
                            <AvField
                                type="select"
                                name="countryId"
                                onChange={(country) => this.getCountriess(country)}
                                // onClick={this.getMinistryOfCountry}
                            >
                                <option value="" disabled>select Country</option>
                                {this.state.countries?.map(country =>
                                    <option value={country.id}>{country.name}</option>
                                )}
                            </AvField>
                            <AvField
                                type="select"
                                name="ministryId"
                            >
                                <option value="">select ministry</option>
                                {this.state.ministries?.map((ministry, index) =>
                                    // ministry.country.id == this.currentCountry ?
                                        <option value={ministry.id}>{ministry.name}</option>
                                    // : ""
                                )}</AvField>
                            <button className="btn btn-success mt-3">save</button>
                            <button className="btn btn-danger mt-3"
                                    onClick={this.closeModal}>cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>{"Do you want delete this management " + this.state.currentManagement.name}</ModalHeader>
                    <ModalFooter>
                        <button className="btn btn-danger"
                                onClick={this.deleteManagement}>delete
                        </button>
                        <button className="btn btn-success"
                                onClick={this.hideDeleteModal}>cancel
                        </button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

Management.propTypes = {};

export default Management;