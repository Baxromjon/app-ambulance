package ai.ecma.appambulance.payload;

import ai.ecma.appambulance.entity.MedicineFarm;
import ai.ecma.appambulance.entity.MedicineType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResMedicine {
    @NotBlank
    private String name;
    private String description;
    @NotBlank
    private Double amount;
    @NotBlank
    private Double price;
    @NotNull
    private Date expiredDate;
    @NotNull
    private Timestamp acceptedDate;
    @NotNull
    private Integer medicineTypeId;
    @NotNull
    private UUID medicineFarmId;
}
