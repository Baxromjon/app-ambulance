package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.SickMan;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResSickMan;
import ai.ecma.appambulance.repository.SickManRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class SickManService {
    @Autowired
    SickManRepository sickManRepository;

    public List<SickMan> getAll() {
        return sickManRepository.findAll();
    }

    public SickMan getById(UUID id) {
        Optional<SickMan> byId = sickManRepository.findById(id);
        if (!byId.isPresent()) {
            return new SickMan();
        }
        return byId.get();
    }

    public ApiResponse add(ResSickMan resSickMan) {
        SickMan sickMan = new SickMan(
                resSickMan.getFirstName(),
                resSickMan.getLastName(),
                resSickMan.getBirthDate(),
                resSickMan.getPhoneNumber()
        );
        sickManRepository.save(sickMan);
        return new ApiResponse(true,"successfully edited");
    }

    public ApiResponse edit(UUID id, ResSickMan resSickMan) {
        boolean exists = sickManRepository.existsById(id);
        SickMan sickMan = sickManRepository.findById(id).orElseGet(SickMan::new);
        if (exists){
            return new ApiResponse(false,"sick man not found");
        }
        sickMan.setDiagnosis(resSickMan.getDiagnosis());
        sickManRepository.save(sickMan);
        return new ApiResponse(true, "successfully edited");
    }
}
