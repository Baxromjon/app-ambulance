package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.OrganizationRole;
import ai.ecma.appambulance.payload.OrganizationRoleProjection;
import ai.ecma.appambulance.payload.ResOrganizationRole;
import ai.ecma.appambulance.service.OrganizationRoleService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/organizationRole")
public class OrganizationRoleController {

    @Autowired
    OrganizationRoleService organizationRoleService;

    @GetMapping("getAllOrganizationRole")
    public List<OrganizationRoleProjection> getAll(){
        return organizationRoleService.getAll();
    }

    @GetMapping("{id}")
    public OrganizationRole getById(@PathVariable UUID id){
        return organizationRoleService.getById(id);
    }

//    @PostMapping("save")
//    public HttpEntity<?> addOrganizationRole(@RequestBody ResOrganizationRole resOrganizationRole){
//
//    }

}
