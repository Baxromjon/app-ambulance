package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.PhoneAuth;
import ai.ecma.appambulance.entity.PhoneCode;
import ai.ecma.appambulance.entity.User;
import ai.ecma.appambulance.payload.*;
import ai.ecma.appambulance.repository.PhoneAuthRepository;
import ai.ecma.appambulance.repository.PhoneCodeRepository;
import ai.ecma.appambulance.repository.UserRepository;
import ai.ecma.appambulance.security.JwtTokenProvider;
import ai.ecma.appambulance.service.AuthService;
import ai.ecma.appambulance.service.SmsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.Random;

@CrossOrigin
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthService authService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtTokenProvider jwtTokenProvider;
    @Autowired
    PhoneCodeRepository phoneCodeRepository;
    @Autowired
    PhoneAuthRepository phoneAuthRepository;

//    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @PostMapping("/register")
    HttpEntity<?> register(@Valid @RequestBody RegisterDto registerDto) {
        Result apiResponse = authService.register(registerDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

//    @PostMapping("/loginForDriver")
//    public HttpEntity<?> loginDriver(@RequestBody UserDto userDto) {
//        Result result = authService.loginDriver(userDto);
//        return ResponseEntity.status(result.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(result);
//    }

//    @PostMapping("/checkSmsCode")
//    @Transactional
//    public HttpEntity<?> checkSmsCode(@Valid @RequestBody CheckCodeRequest checkCodeRequest) throws Exception {
//        try {
//            Optional<PhoneCode> phoneCodeOptional = phoneCodeRepository.findById(checkCodeRequest.getConfirmationResult());
//            if (phoneCodeOptional.isPresent()) {
//                PhoneCode phoneCode = phoneCodeOptional.get();
//                phoneCode.setChecked(true);
//                phoneCodeRepository.save(phoneCode);
//                phoneAuthRepository.save(new PhoneAuth(phoneCode.getPhoneNumber()));
//                return ResponseEntity.ok(new ApiResponse(true, "Saqlandi"));
//            } else {
//                return ResponseEntity.ok(new ApiResponse(false, "Tasdiqlash kodi xato"));
//            }
//        } catch (Exception e) {
//            logger.error("error send sms", e);
//            throw new Exception("Tizim xatoligi", e);
//        }
//    }

    @PostMapping("/login")
    public HttpEntity<?> checkLogin(@Valid @RequestBody LoginDto userDto) {
        try {
            Authentication authentication =
                    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                            userDto.getPhoneNumber(),
                            userDto.getPassword()
                    ));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            User user = (User) authentication.getPrincipal();
            String token = jwtTokenProvider.generateToken(user.getId());
            return ResponseEntity.status(200).body(new ApiResponse(true, "successfully", token));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(409).body(new ApiResponse(false, "Bad credentials"));
    }

//    @PostMapping("/checkPasswordAndLogin")
//    public HttpEntity<?> checkPasswordAndLogin(@Valid @RequestBody LoginDto loginDto) {
//        ApiResponse apiResponse = authService.checkPasswordAndLogin(loginDto);
//        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
//    }
//
//    @GetMapping("/verification")
//    public HttpEntity<?> verificationPhone(@RequestParam String phoneNumber, @RequestParam String code) {
//        ApiResponse apiResponse = authService.verificationPhone(phoneNumber, code);
//        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
//
//    }

//    @PostMapping("send_sms_code")
//    public ResponseEntity<ApiResponse> sendSms(@Valid @RequestBody SendSmsRequest sendSmsRequest) throws Exception {
//        try {
//            String code=(new Random().nextInt(900000)+100000)+"";
//            System.out.println(code);
//            ApiResponse apiResponse= SmsService.getInstance().sendMessage(Arrays.asList(sendSmsRequest.getPhoneNumber()),"app-ambulance uchun tasdiqlash kodi:"+code);
//            PhoneCode phoneCode=null;
//            if (apiResponse.isSuccess()){
//                phoneCode=phoneCodeRepository.save(new PhoneCode(code, sendSmsRequest.getPhoneNumber(),false));
//            }
//            return ResponseEntity.ok(new ApiResponse(apiResponse.isSuccess(),apiResponse.isSuccess()?"Kod jo`natildi":"Xatolik", phoneCode!=null?phoneCode.getId():null));
//        } catch (IOException e) {
//            logger.error("error send sms", e);
//            throw new Exception("tizim xatoligi",e);
//        }
//    }


}

