package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.SickMan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SickManRepository extends JpaRepository<SickMan, UUID> {
    boolean existsById(UUID id);
}
