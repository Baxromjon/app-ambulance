package ai.ecma.appambulance.projection;

import java.util.UUID;

public interface MinistryProjection {
    UUID getId();
    String getName();
}
