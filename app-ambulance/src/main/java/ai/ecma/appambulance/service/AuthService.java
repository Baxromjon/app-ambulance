package ai.ecma.appambulance.service;

import ai.ecma.appambulance.component.MessageByLang;
import ai.ecma.appambulance.entity.*;
import ai.ecma.appambulance.entity.enums.RoleName;
import ai.ecma.appambulance.entity.enums.SystemRoleEnum;
import ai.ecma.appambulance.payload.*;
import ai.ecma.appambulance.repository.*;
import ai.ecma.appambulance.security.JwtTokenProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PhoneAuthRepository phoneAuthRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    OrganizationRoleRepository roleRepository;

//    @Autowired
//    UserDeviceRepository userDeviceRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    MessageByLang messageByLang;

    @Autowired
    ManagementRepository managementRepository;

    @Autowired
    OrganizationRepository organizationRepository;
    @Autowired
    OrganizationPermissionRepository organizationPermissionRepository;
    @Autowired
    SystemRoleRepository systemRoleRepository;
    @Autowired
    JwtTokenProvider tokenProvider;
    @Autowired
    OrganizationRoleRepository organizationRoleRepository;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(s).orElseThrow(() -> new UsernameNotFoundException(s));
    }

    public UserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
        return userRepository.findById(UUID.fromString(userId)).orElseThrow(() -> new UsernameNotFoundException("userId"));
    }

    private String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }

    public Result loginDriver(UserDto userDto) {
        Set<OrganizationRole> role = roleRepository.findByName(RoleName.ROLE_DRIVER);
//        organizationRoleRepository.findByName(userDto.get)
        Optional<User> optionalUser = userRepository.findByPhoneNumberAndRolesIn(userDto.getPhoneNumberUser(), role);
        if (optionalUser.isPresent()) {
            String randomNumberString = getRandomNumberString();
            User user = optionalUser.get();
            user.setCode(randomNumberString);
            userRepository.save(user);
            return new Result(randomNumberString, true, user.getId());
        }
        return new Result("Bo`linmaga murojaat qiling", false);


    }

//    public Result checkSmsCode(Result result) {
//        Optional<User> optionalUser = userRepository.findById(result.getUserId());
//        if (!optionalUser.isPresent()) {
//            return new Result("O'chirilgan", false);
//        }
//        User user = optionalUser.get();
//        if (user.getCode().equals(result.getMessage())) {
//            user.setEnabled(true);
//            userRepository.save(user);
//            return new Result("Ok", true);
//        }
//        return new Result("Kod xato", false);
//    }

    public Result register(RegisterDto registerDto) {
        Boolean exists = userRepository.existsByPhoneNumber(registerDto.getPhoneNumber());
        if (exists) {
            return new Result(messageByLang.getMessageByKey("allready exist"), false);
        }
        return new Result("", true);
    }
//
//    public ApiResponse checkPasswordAndLogin(LoginDto loginDto) {
//        try {
//            authenticationManager.authenticate(
//                    new UsernamePasswordAuthenticationToken(
//                            loginDto.getPhoneNumber(),
//                            loginDto.getPassword()
//                    ));
//            return new ApiResponse(true, "Tekshirildi");
//        } catch (Exception e) {
//            return new ApiResponse(false, "error!!!");
//        }
//    }
//
//    public ApiResponse verificationPhone(String phoneNumber, String code) {
//        Optional<User> optional = userRepository.findByPhoneNumberAndCodeAndEnabledFalse(phoneNumber, code);
//        if (optional.isPresent()) {
//            User user = optional.get();
//            user.setEnabled(true);
//            user.setCode(null);
//            userRepository.save(user);
//            return new ApiResponse(true, "Tasdiqlandi", tokenProvider.generateToken(user.getId()));
//        }
//        return new ApiResponse(false, "Error");
//    }
}
