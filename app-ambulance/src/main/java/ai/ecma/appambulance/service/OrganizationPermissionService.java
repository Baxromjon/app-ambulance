package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.OrganizationPermission;
import ai.ecma.appambulance.repository.OrganizationPermissionRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganizationPermissionService {
    @Autowired
    OrganizationPermissionRepository organizationPermissionRepository;

    public List<OrganizationPermission> getAllPermission() {
        return organizationPermissionRepository.findAll();
    }
}
