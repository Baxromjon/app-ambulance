import React, {Component} from 'react';
import request from "../../utils/request";
import api from "../../utils/api";
import {Collapse} from "reactstrap";
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'
import {TOKEN} from "../../utils/constant";

class Organization extends Component {
    state = {
        organizations: [],
        managements: [],
        ministries: [],
        districts: [],
        countries: [],
        regions: [],
        roles: [],
        permissions: [],
        currentRegion: '',
        currentCountry: '',
        currentOrganization: '',
        currentManagement: '',
        currentMinistry: '',
        showModal: false,
        showModalDelete: false,
        addOrganization: false,
        saveUser: false,
    }

    toggleAddOrganization = () => {
        this.setState({
            addOrganization: !this.state.addOrganization
            // saveUser: !this.state.saveUser
        })
    }
    toggleSaveUser = () => {
        this.setState({
            saveUser: !this.state.saveUser,
            // addOrganization: !this.state.addOrganization
        })
    }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getOrganization()
            this.getManagement()
            this.getMinistries()
            this.getDistrict()
            this.getCountry()
            this.getRegion()
            this.getRoles()
            this.getPermission()
        }
    }

    saveOrganization = (e, v) => {
        let userDto={
            firstName:"",
            lastName:"",
            passportSerial:"",
            passportSerialNumber:"",
            phoneNumberUser:"",
            password:""
        }
        userDto.firstName=v.firstName
        userDto.lastName=v.lastName
        userDto.passportSerial=v.passportSerial
        userDto.passportSerialNumber=v.passportSerialNumber
        userDto.phoneNumberUser=v.phoneNumberUser
        userDto.password=v.password
        v.userDto=userDto;

        let current = this.state.currentOrganization
        request({
            url: current ? (api.editOrganization + '/' + current.id) : api.addOrganization,
            method: current ? 'PUT' : 'POST',
            data: v
        }).then(ans => {
            this.getOrganization()
            this.closeModal()
        }).catch(res => {
        })
    }
    getAllRegionOfCountry = () => {
        let current = this.state.currentCountry
        request({
            url: api.getAllRegionOfCountry + '/' + current,
            method: 'GET'
        }).then(res => {
            this.setState({regions: res.data.data})
        }).catch(err => {
        })
    }
    getRoles = () => {
        request({
            url: api.getOrganizationRoles,
            method: 'GET'
        }).then(res => {
            this.setState({roles: res.data})
        }).catch(err => {
            alert(err.data.message)
        })
    }
    getPermission = () => {
        request({
            url: api.getOrganizationPermission,
            method: 'GET'
        }).then(res => {
            this.setState({permissions: res.data})
        }).catch(err => {
        })
    }
    getAllDistrictOfRegion = () => {
        let current = this.state.currentRegion
        request({
            url: api.getAllDistrictOfRegion + '/' + current,
            method: 'GET'
        }).then(ans => {
            this.setState({districts: ans.data.data})
        }).catch(res => {
        })
    }
    getRegions = (region) => {
        this.setState({currentRegion: region.target.value})
    }
    getManagements = (item) => {
        this.setState({currentManagement: item.target.value})
    }
    getMinistry = (item) => {
        this.setState({currentMinistry: item.target.value})
    }
    getCountries = (country) => {
        this.setState({currentCountry: country.target.value})
    }
    getRegion = () => {
        request({
            url: api.getRegion,
            method: 'GET'
        }).then(ans => {
            this.setState({regions: ans.data})
        })
    }
    getCountry = () => {
        request({
            url: api.getCountry,
            method: 'GET'
        }).then(ans => {
            this.setState({countries: ans.data})
        })
    }
    getMinistries = () => {
        request({
            url: api.getMinistry,
            method: 'GET'
        }).then(ans => {
            this.setState({ministries: ans.data})
        }).catch(res => {
            alert("xatolik")
        })
    }
    getManagement = () => {
        request({
            url: api.getManagements,
            method: 'GET'
        }).then(ans => {
            this.setState({managements: ans.data})
        }).catch(res => {
            alert("xatolik")
        })
    }
    getOrganization = () => {
        request({
            url: api.getOrganization,
            method: 'GET'
        }).then(res => {
            this.setState({organizations: res.data})
        }).catch(err => {
            alert("xatolik")
        })
    }
    getDistrict = () => {
        request({
            url: api.getDistrict,
            method: 'GET'
        }).then(ans => {
            this.setState({districts: ans.data})
        }).catch(res => {
        })
    }
    openModal = () => {
        this.setState({showModal: true})
    }
    closeModal = () => {
        this.setState({showModal: false})
    }
    editModal = (organization) => {
        this.openModal()
        this.setState({currentOrganization: organization})
    }
    deleteModal = (organization) => {
        this.setState({
            currentOrganization: organization,
            showModalDelete: true
        })
    }
    deleteOrganization = () => {
        let current = this.state.currentOrganization
        request({
            url: api.deleteOrganization + '/' + current.id,
            method: 'DELETE'
        }).then(ans => {
            this.hideDeleteModal()
            this.getOrganization()
        }).catch(res => {
        })
    }
    hideDeleteModal = () => {
        this.setState({
            showModalDelete: false,
            currentOrganization: ''
        })
    }
    // saveUser=(e,v)=>{
    //     request({
    //         url:api.
    //     })
    // }

    render() {
        let roles = this.state.roles
        let permissions = this.state.permissions
        return (
            <div>
                <h1>Organization List</h1>
                <hr/>
                <br/>
                <button className="btn btn-success"
                        onClick={this.openModal}>add Organization
                </button>
                <br/>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Organization name</th>
                        <th>Management name</th>
                        <th>Address</th>
                        <th>Phone number</th>
                        <th>Option</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.organizations?.map((organization, index) =>
                        <tr key={index}>
                            <th>{index + 1}</th>
                            <td>{organization.name}</td>
                            <td>{organization.management.name}</td>
                            <td>{organization.street + ', ' + organization.district.name + ', ' +
                            organization.district.region.name
                            }</td>
                            <td>{organization.phoneNumber}</td>
                            <td>
                                <button className="btn btn-success"
                                        onClick={() => this.editModal(organization)}>edit
                                </button>
                                <button className="btn btn-danger"
                                        onClick={() => this.deleteModal(organization)}>delete
                                </button>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </table>
                <Modal isOpen={this.state.showModal}>

                    <ModalBody>
                        <div className="flex-row justify-content-between">
                                        <span className="form-section-title align-self-center margin-right-15">
                                            <h3>Save organization</h3>
                                        </span>
                        </div>
                        <AvForm onValidSubmit={this.saveOrganization}>
                            <EditIcon onClick={this.toggleAddOrganization}/>
                            {/*<Collapse isOpen={this.state.addOrganization}>*/}
                                <AvField
                                    placeholder="enter name here"
                                    defaultValue={this.state.currentOrganization.name}
                                    name="name" required
                                />
                                <AvField
                                    placeholder="enter phone number here"
                                    defaulValue={this.state.currentOrganization.phoneNumber}
                                    name="phoneNumber"
                                />
                                <div className="row m-0">
                                    <div className="col-md-6">
                                        <AvField
                                            type="select"
                                            name="countryId"
                                            onChange={(country) => this.getCountries(country)}
                                            // onClick={this.getAllRegionOfCountry}
                                        >
                                            <option value="">select Country</option>
                                            {this.state.countries?.map((country, index) =>
                                                // country.region.id==this.state.currentRegion?
                                                <option value={country.id}>{country.name}</option>
                                            )}
                                        </AvField>
                                    </div>
                                    <div className="col-md-6">
                                        <AvField
                                            type="select"
                                            name="ministryId"
                                            onChange={(item) => this.getMinistry(item)}>
                                            <option value="">select Ministry</option>
                                            {this.state.ministries?.map((ministry, index) =>
                                                <option value={ministry.id}>{ministry.name}</option>
                                            )}
                                        </AvField>
                                    </div>
                                </div>

                                <div className="row m-0">
                                    <div className="col-md-6">
                                        <AvField
                                            type="select"
                                            name="regionId"
                                            onChange={(region) => this.getRegions(region)}
                                            // onClick={this.getAllDistrictOfRegion()}
                                        >
                                            <option value="">select Region</option>
                                            {this.state.regions?.map((region, index) =>
                                                region.country.id == this.state.currentCountry ?
                                                    <option value={region.id}>{region.name}</option> : ""
                                            )}
                                        </AvField>
                                    </div>
                                    <div className="col-md-6">
                                        <AvField
                                            type="select"
                                            name="managementId"
                                            // onChange={(item) => this.getManagements(item)}
                                        >
                                            <option value="">select management</option>
                                            {this.state.managements?.map((management, index) =>
                                                // management.ministry.id=this.state.currentMinistry?
                                                <option value={management.id}>{management.name}</option>
                                            )}
                                        </AvField>
                                    </div>
                                </div>
                                <div className="row m-0">
                                    <div className="col-md-6">
                                        <AvField
                                            type="select"
                                            name="districtId">
                                            <option value="">select district</option>
                                            {this.state.districts?.map((district, index) =>
                                                district.region.id == this.state.currentRegion ?
                                                    <option value={district.id}>{district.name}</option> : ""
                                            )}
                                        </AvField>
                                    </div>
                                    <div className="col-md-6">
                                        <AvField
                                            placeholder="enter street here"
                                            defaultValue={this.state.currentOrganization.street}
                                            name="street"/>
                                    </div>
                                </div>
                                <div className="row m-0">
                                    <div className="col-md-6">
                                        <AvField
                                            name="rolesId"
                                            type="select"
                                            label="select Roles for Organization"
                                            multiple>
                                            {roles?.map(roles => (
                                                <option value={roles.value}>{roles.label}</option>
                                            ))}
                                        </AvField>
                                    </div>
                                    <div className="col-md-6">
                                        <AvField
                                            name="permissionsId"
                                            type="select"
                                            label="select Permission Organization" multiple>
                                            {permissions?.map(permission => (
                                                <option
                                                    value={permission.id}>{permission.organizationPermissionEnum}</option>
                                            ))}
                                        </AvField>
                                    </div>
                                </div>

                            {/*</Collapse>*/}
                            <div className="flex-row justify-content-between">
                                        <span className="form-section-title align-self-center margin-right-15">
                                            <h3>Save user</h3>
                                        </span>
                            </div>
                            <EditIcon onClick={this.toggleSaveUser}/>
                            {/*<Collapse isOpen={this.state.saveUser}>*/}

                                <div className="row m-0">
                                    <div className="col-md-6">
                                        <AvField
                                            placeholder="First Name here"
                                            name="firstName" required
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <AvField
                                            placeholder="Last Name here"
                                            name="lastName" required
                                        />
                                    </div>
                                </div>
                                <div className="row m-0">
                                    <div className="col-md-6">
                                        <AvField
                                            placeholder="Passport Serial here"
                                            name="passportSerial" required
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <AvField
                                            placeholder="Passport Serial Number here"
                                            name="passportSerialNumber" required
                                        />
                                    </div>
                                </div>
                                <div className="row m-0">
                                    <div className="col-md-6">
                                        <AvField
                                            placeholder="PhoneNumber here"
                                            name="phoneNumberUser"
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <AvField
                                            type="password"
                                            placeholder="Password here"
                                            name="password"/>
                                    </div>
                                </div>
                            {/*</Collapse>*/}
                            <button className="btn btn-success">save</button>
                            <button className="btn btn-danger"
                                    onClick={this.closeModal}>cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                    {/*<div className="flex-row justify-content-between">*/}
                    {/*                    <span className="form-section-title align-self-center margin-right-15">*/}
                    {/*                        <h3>Save Leader</h3>*/}
                    {/*                    </span>*/}
                    {/*    <EditIcon onClick={this.toggleSaveUser}/>*/}
                    {/*</div>*/}
                    {/*<ModalBody>*/}
                    {/*    <AvForm onValidSubmit={this.saveOrganization}>*/}
                    {/*        */}
                    {/*        <button className="btn btn-success">save</button>*/}
                    {/*        <button className="btn btn-danger"*/}
                    {/*                onClick={this.closeModal}>cancel*/}
                    {/*        </button>*/}
                    {/*    </AvForm>*/}
                    {/*</ModalBody>*/}

                </Modal>

                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>{"Do you want delete this organization " + this.state.currentOrganization.name}</ModalHeader>
                    <ModalFooter>
                        <button className="btn btn-danger"
                                onClick={this.deleteOrganization}>delete
                        </button>
                        <button className="btn btn-success"
                                onClick={this.hideDeleteModal}>cancel
                        </button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

Organization.propTypes = {};

export default Organization;

function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className} id={props.id}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="#313E47"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"/>
            </svg>
        </div>
    );
}