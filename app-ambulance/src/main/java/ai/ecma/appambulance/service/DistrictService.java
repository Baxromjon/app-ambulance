package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.District;
import ai.ecma.appambulance.entity.Region;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResDistrict;
import ai.ecma.appambulance.repository.DistrictRepository;
import ai.ecma.appambulance.repository.RegionRepository;
import com.google.protobuf.Api;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DistrictService {
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    RegionRepository regionRepository;

    public List<District> getAllDistrict() {
        return districtRepository.findAll();
    }

    public District getDistrictById(UUID id) {
        Optional<District> optional = districtRepository.findById(id);
        if (!optional.isPresent()){
            return new District();
        }
        return optional.get();
    }

    public ApiResponse addDistrict(ResDistrict resDistrict) {
        Optional<Region> optional = regionRepository.findById(resDistrict.getRegionId());
        Boolean exists = districtRepository.existsByNameAndRegion(resDistrict.getName(), optional.get());
        if (exists){
            return new ApiResponse(false, "allReady exists");
        }
        District district=new District(resDistrict.getName(), optional.get());
        districtRepository.save(district);
        return new ApiResponse(true, "District added");
    }

    public ApiResponse editDistrict(UUID id, ResDistrict resDistrict) {
        Optional<Region> regionOptional = regionRepository.findById(resDistrict.getRegionId());
        Optional<District> optional = districtRepository.findById(id);
        if (!optional.isPresent()){
            return new ApiResponse(false,"District not found");
        }
        District district=optional.get();
        district.setName(resDistrict.getName());
        district.setRegion(regionOptional.get());
        districtRepository.save(district);
        return new ApiResponse(true,"District edited");
    }

    public ApiResponse deleteDistrict(UUID id) {
        districtRepository.deleteById(id);
        return new ApiResponse(true, "District deleted");
    }
}
