package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.OrganizationPermission;
import ai.ecma.appambulance.service.OrganizationPermissionService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/organizationPermission")
public class OrganizationPermissionController {
    @Autowired
    OrganizationPermissionService organizationPermissionService;

    @GetMapping("getAllPermission")
    public List<OrganizationPermission> getAllPermission(){
       return organizationPermissionService.getAllPermission();
    }
}
