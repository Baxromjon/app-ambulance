package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.template.AbsEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"passportSerial", "passportSerialNumber"})
})
public class User extends AbsEntity implements UserDetails {

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String passportSerial;

    @Column(nullable = false)
    private String passportSerialNumber;

    @Column(nullable = false)
    private String phoneNumber;

    @Column(unique = true)
    private String email;

    @JsonIgnore
    @Column(nullable = false)
    private String password;

    @ManyToOne(fetch = FetchType.LAZY)
    private Attachment photo;

    @ManyToMany
    @JoinTable(name = "users_positions",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "position_id")})
    private List<Position> position;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_organizations",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "organization_id")})
    private List<Organization> organization;

    @JsonIgnore
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    private OrganizationRole organizationRole;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_system_roles", joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "system_role_id")})
    private Set<SystemRole> roles;

    @ManyToMany
    @JoinTable(name = "users_organization_permissions",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "organization_permission_id")})
    private List<OrganizationPermission> permissionList;

//    @ManyToOne(fetch = FetchType.LAZY)
//    private Car car;

    private boolean enabled = true;
    private boolean credentialsNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean accountNonExpired = true;

    private String code;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public String getUsername() {
        return phoneNumber;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public User(String firstName, String lastName, String phoneNumber, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    public User(String firstName, String lastName, String phoneNumber, String password, Set<SystemRole> roles, boolean enabled) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.roles = roles;
        this.enabled = enabled;
    }

    public User(String firstName, String lastName, String phoneNumber, String password, List<Organization> organization, OrganizationRole organizationRole, Set<SystemRole> roles, List<OrganizationPermission> permissions) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.organization = organization;
        this.organizationRole = organizationRole;
        this.roles = roles;
        this.permissionList = permissions;
    }

    public User(String firstName, String lastName, String passportSerial, String passportSerialNumber, String phoneNumber, String password, List<Organization> organization, OrganizationRole organizationRole, Set<SystemRole> roles, List<OrganizationPermission> permissionList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.passportSerial = passportSerial;
        this.passportSerialNumber = passportSerialNumber;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.organization = organization;
        this.organizationRole = organizationRole;
        this.roles = roles;
        this.permissionList = permissionList;
    }

    public User(String firstName, String lastName, String passportSerial, String passportSerialNumber, String phoneNumber, String password, Set<SystemRole> roles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.passportSerial = passportSerial;
        this.passportSerialNumber = passportSerialNumber;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.roles = roles;
    }

    public User(String firstName, String lastName, String passportSerial, String passportSerialNumber, String phoneNumber, String password, List<Organization> organization, OrganizationRole organizationRole, List<OrganizationPermission> permissionList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.passportSerial = passportSerial;
        this.passportSerialNumber = passportSerialNumber;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.organization = organization;
        this.organizationRole = organizationRole;
        this.permissionList = permissionList;
    }
}
