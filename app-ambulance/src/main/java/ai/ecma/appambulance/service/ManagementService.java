package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Management;
import ai.ecma.appambulance.entity.Ministry;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResManagement;
import ai.ecma.appambulance.repository.ManagementRepository;
import ai.ecma.appambulance.repository.MinistryRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ManagementService {
    @Autowired
    ManagementRepository managementRepository;
    @Autowired
    MinistryRepository ministryRepository;

    public List<Management> getAll() {
        return managementRepository.findAll();
    }

    public Management getById(UUID id) {
        Optional<Management> optional = managementRepository.findById(id);
        if (!optional.isPresent()){
            return new Management();
        }
        return optional.get();
    }

    public ApiResponse addManagement(ResManagement resManagement) {
        Optional<Ministry> ministryOptional = ministryRepository.findById(resManagement.getMinistryId());
        Boolean exists = managementRepository.existsByName(resManagement.getName());
        if (exists){
            return new ApiResponse(false, "Management allReady exists");
        }
        Management management=new Management(resManagement.getName(),ministryOptional.get());
        managementRepository.save(management);
        return new ApiResponse(true, "Management successfully added");

    }

    public ApiResponse edit(UUID id, ResManagement resManagement) {
        Optional<Ministry> ministryOptional = ministryRepository.findById(resManagement.getMinistryId());
        Optional<Management> optional = managementRepository.findById(id);
        if (!optional.isPresent()){
            return new ApiResponse(false, "not found");
        }
        Management management=optional.get();
        management.setName(resManagement.getName());
        management.setMinistry(ministryOptional.get());
        managementRepository.save(management);
        return new ApiResponse(true, "Successfully edited");
    }

    public ApiResponse delete(UUID id) {
        try {
            managementRepository.deleteById(id);
            return new ApiResponse(true, "deleted");
        }catch (Exception e){
            return new ApiResponse(false, "not found");
        }
    }


}
