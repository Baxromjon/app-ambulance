-- insert into organization_role(name)
-- values ('ROLE_CLIENT'),
--        ('ROLE_DRIVER'),
--        ('ROLE_MODERATOR'),
--        ('ROLE_ADMIN'),
--        ('ROLE_SUPER_ADMIN');

insert into brand(id, name)
values (1, 'CHEVROLET'),
       (2, 'RAVON');

insert into model(name, brand_id)
values ('DAMAS', 1),
        ('DAMAS',2);

insert into color(name, code)
values ('White', '#fff'),
       ('Black', '#000'),
       ('Green', '#008000');

insert into made_year(value)values
(2012),
(2013),
(2014),
(2015),
(2016),
(2017),
(2018),
(2019),
(2020);

