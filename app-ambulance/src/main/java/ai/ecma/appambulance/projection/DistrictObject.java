package ai.ecma.appambulance.projection;

import java.util.UUID;

public interface DistrictObject {
    UUID getId();
    String getName();
}
