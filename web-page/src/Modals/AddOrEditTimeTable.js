import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import request from "../utils/request";
import api from "../utils/api";

AddOrEditTimeTable.propTypes = {};

function AddOrEditTimeTable(props) {
    const {
        toggle,
        saveTimeTable,
        isEdit
    } = props

    useEffect(() => {
        getOrganizations()
        getUser()
        getCar()
    },[])

    const [isOpen, setIsOpen] = useState(true);
    const [organizations, setOrganizations] = useState([]);
    const [drivers, setDrivers] = useState([]);
    const [cars, setCars] = useState([]);
    const [currentOrganization, setCurrentOrganization] = useState('');

    const getOrganizations = () => {
        request({
            url: api.getOrganization,
            method: 'GET'
        }).then(res => {
            setOrganizations(res.data)
        }).catch(err => {
        })
    }
    const getUser = () => {
        request({
            url: api.getDriver,
            method: 'GET'
        }).then(res => {
            setDrivers(res.data)
        }).catch(err => {
        })
    }
    const getCar = () => {
        request({
            url: api.getCarsByOrganizationId + '/' + currentOrganization,
            method: 'GET'
        }).then(res => {
            setCars(res.data)
        }).catch(err => {
        })
    }
    const getOneOrganization = (organization) => {
        console.log(organization.target.value)
        setCurrentOrganization(organization.target.value)
    }

    return (
        <div>
            <Modal isOpen={isOpen}>
                <ModalHeader>add Time Table</ModalHeader>
                <ModalBody>
                    <AvForm onValidSubmit={saveTimeTable}>
                        <AvField
                            type="select"
                            name="organizationId"
                            onChange={(organization) => getOneOrganization(organization)}>
                            <option value="">select Organization</option>
                            {organizations?.map(organization => (
                                <option value={organization.id}>{organization.name}</option>
                            ))}
                        </AvField>
                        <AvField
                            type="select"
                            name="userId"
                            onClick={()=>getUser()}>
                            <option value="">select User</option>
                            {drivers?.map(driver => (
                                <option value={driver.driverId}>{driver.firstName + ' ' + driver.lastName}</option>
                            ))}
                        </AvField>
                        <AvField
                            type="select"
                            name="carId"
                            onClick={() => getCar()}>
                            <option value="">select car</option>
                            {cars?.map(car => (
                                <option value={car.id}>{car.model.brand.name + ' ' + car.model.name}</option>
                            ))}
                        </AvField>
                        <div className="row m-0">
                            <div className="col-md-6">
                                <AvField
                                    name="fromDate"
                                    label="select from Date"
                                    type={"date"}
                                />
                            </div>
                            <div className="col-md-6">
                                <AvField
                                    name="fromTime"
                                    label="select fromTime"
                                    type={"time"}/>
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className="col-md-6">
                                <AvField
                                    name="toDate"
                                    label="select to Date"
                                    type={"date"}
                                />
                            </div>
                            <div className="col-md-6">
                                <AvField
                                    name="toTime"
                                    label="select to Time"
                                    type={"time"}/>
                            </div>
                        </div>

                        <AvField
                            name="rateByHour"
                            placeholder="rate by hour here"
                        />
                        <button className="btn btn-success">save</button>
                        <button className="btn btn-danger"
                                onClick={toggle}>cancel
                        </button>
                    </AvForm>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default AddOrEditTimeTable;