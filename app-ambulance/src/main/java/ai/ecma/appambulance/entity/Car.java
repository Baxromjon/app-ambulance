package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.template.AbsEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"carLicenseSerial", "carLicenseSerialNumber"}),
        @UniqueConstraint(columnNames = {"driverLicenseSerial", "driverLicenseSerialNumber"})})

public class Car extends AbsEntity {
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Model model;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Color color;
    @Column(nullable = false)
    private String carLicenseSerial;
    @Column(nullable = false)
    private String carLicenseSerialNumber;
    @Column(nullable = false)
    private String driverLicenseSerial;
    @Column(nullable = false)
    private String driverLicenseSerialNumber;
    @Column(nullable = false, unique = true)
    private String regStateNumber;
    private Float lat;
    private Float lon;
    private boolean online;
    @JsonIgnore

    @OneToMany
    private List<User> drivers;

    @ManyToOne(optional = false)
    private MadeYear madeYear;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Organization organization;

    public Car(Model model, Color color, String carLicenseSerial, String carLicenseSerialNumber, String driverLicenseSerial, String driverLicenseSerialNumber, String regStateNumber, List<User> drivers, MadeYear madeYear, Organization organization) {
        this.model = model;
        this.color = color;
        this.carLicenseSerial = carLicenseSerial;
        this.carLicenseSerialNumber = carLicenseSerialNumber;
        this.driverLicenseSerial = driverLicenseSerial;
        this.driverLicenseSerialNumber = driverLicenseSerialNumber;
        this.regStateNumber = regStateNumber;
        this.drivers = drivers;
        this.madeYear = madeYear;
        this.organization = organization;
    }
}
