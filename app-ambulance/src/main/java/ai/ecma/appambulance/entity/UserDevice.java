//package ai.ecma.appambulance.entity;
//
//import ai.ecma.appambulance.entity.template.AbsNameEntity;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.ManyToOne;
//import java.util.UUID;
//
//@EqualsAndHashCode(callSuper = true)
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Entity
//public class UserDevice extends AbsNameEntity {
//    private String userAgent;
//    @Column(unique = true, nullable = false)
//    private UUID key;
//    @ManyToOne(fetch = FetchType.LAZY, optional = false)
//    private User user;
//
//
//}
