package ai.ecma.appambulance.payload;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class CaptchaResponse {
    private Boolean success;
    private Timestamp challengeTs;
    private String hostname;
    private List<String> errorCodes;
}
