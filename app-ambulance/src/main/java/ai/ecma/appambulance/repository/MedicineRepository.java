package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Medicine;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MedicineRepository extends JpaRepository<Medicine, UUID> {
    Boolean existsByName(String name);
}
