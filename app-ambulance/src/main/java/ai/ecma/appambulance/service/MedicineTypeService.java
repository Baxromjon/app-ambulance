package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Medicine;
import ai.ecma.appambulance.entity.MedicineType;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResMedicineType;
import ai.ecma.appambulance.repository.MedicineTypeRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MedicineTypeService {
    @Autowired
    MedicineTypeRepository medicineTypeRepository;

    public List<MedicineType> getAllType() {
        return medicineTypeRepository.findAll();
    }

    public MedicineType getTypeById(Integer id) {
        Optional<MedicineType> byId = medicineTypeRepository.findById(id);
        if (!byId.isPresent()){
            return new MedicineType();
        }
        return byId.get();
    }

    public ApiResponse addMedicineType(ResMedicineType resMedicineType) {
        Boolean exists = medicineTypeRepository.existsByName(resMedicineType.getName());
        if (exists){
            return new ApiResponse(false, "Medicine Type allReady exists");
        }
        MedicineType medicineType=new MedicineType(resMedicineType.getName(),resMedicineType.getDescription());
        medicineTypeRepository.save(medicineType);
        return new ApiResponse(true, "Medicine Type added");
    }

    public ApiResponse editMedicineType(Integer id, ResMedicineType resMedicineType) {
        Optional<MedicineType> optional = medicineTypeRepository.findById(id);
        if (!optional.isPresent()){
            return new ApiResponse(false, "Medicine Type not fount");
        }
        MedicineType medicineType=optional.get();
        medicineType.setName(resMedicineType.getName());
        medicineType.setDescription(resMedicineType.getDescription());
        medicineTypeRepository.save(medicineType);
        return new ApiResponse(true, "Medicine type edited");
    }

    public ApiResponse delete(Integer id) {
        medicineTypeRepository.deleteById(id);
        return new ApiResponse(true, "Medicine type deleted");
    }
}
