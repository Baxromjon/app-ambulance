package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.OrganizationRole;
import ai.ecma.appambulance.payload.OrganizationRoleProjection;
import ai.ecma.appambulance.repository.OrganizationRoleRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrganizationRoleService {
    @Autowired
    OrganizationRoleRepository organizationRoleRepository;

    public List<OrganizationRoleProjection> getAll() {
        return organizationRoleRepository.getAllOrganizationRole();
    }

    public OrganizationRole getById(UUID id) {
        Optional<OrganizationRole> optional = organizationRoleRepository.findById(id);
        if (!optional.isPresent()){
            return new OrganizationRole();
        }
        return optional.get();
    }
}
