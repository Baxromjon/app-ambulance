package ai.ecma.appambulance.payload;

import ai.ecma.appambulance.entity.enums.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResOrder {
private UUID carId;
private UUID clientId;
private Timestamp createdAt;
private Timestamp acceptedAt;
private Timestamp arrivedAt;
private Timestamp startedAt;
private Timestamp closedAt;
@Enumerated(EnumType.STRING)
private OrderStatus status;
private UUID sickManId;

}
