package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Brand;
import ai.ecma.appambulance.entity.Model;
import ai.ecma.appambulance.projection.ModelProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ModelRepository extends JpaRepository<Model, Integer> {
    Boolean existsByNameAndBrand(String name, Brand brand);

    @Query(nativeQuery = true, value = "select cast(m.id as varchar) as id, m.name as name\n" +
            "from model m\n" +
            "where brand_id = :id")
    List<ModelProjection> getAllModelOfBrand(Integer id);
}
