package ai.ecma.appambulance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppAmbulanceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppAmbulanceApplication.class, args);
    }

}
