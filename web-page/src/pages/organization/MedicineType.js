import React, {Component} from 'react';
import {TOKEN} from "../../utils/constant";
import request from "../../utils/request";
import api from "../../utils/api";
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'

class MedicineType extends Component {
    state = {
        medicineTypes: [],
        currentMedicineType: '',
        showModal: false,
        showDeleteModal: false
    }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getMedicineType()
        }
    }

    openModal = () => {
        this.setState({showModal: !this.state.showModal})
    }

    getMedicineType = () => {
        request({
            url: api.getMedicineType,
            method: 'GET'
        }).then(res => {
            this.setState({medicineTypes: res.data})
        }).catch(err => {
        })
    }
    saveMedicineType = (e, v) => {
        let current = this.state.currentMedicineType
        request({
            url: !current ? api.addMedicineType : (api.editMedicineType + '/' + current.id),
            method: !current ? 'POST' : 'PUT',
            data: v
        }).then(res => {
            this.getMedicineType()
            this.openModal()
        }).catch(err => {
        })
    }
    editModal = (item) => {
        this.openModal()
        this.setState({currentMedicineType: item})
    }
    openDeleteModal = () => {
        this.setState({showDeleteModal: !this.state.showDeleteModal})
    }
    deleteModal = (item) => {
        this.openDeleteModal()
        this.setState({currentMedicineType: item})
    }
    deleteType = () => {
        let current = this.state.currentMedicineType
        request({
            url: api.deleteMedicineType+'/'+current.id,
            method: 'DELETE'
        }).then(res => {
            this.getMedicineType()
            this.openDeleteModal()
        }).catch(err => {
        })
    }

    render() {
        const {medicineTypes, currentMedicineType} = this.state
        return (
            <div>
                <h1 className="text-center">Medicine Type page</h1>
                <button className="btn btn-success"
                        onClick={this.openModal}>add medicine type
                </button>
                <hr/>
                <br/>

                <table className="table table-bordered">
                    <thead>
                    <th>№</th>
                    <th>Medicine Type name</th>
                    <th>description</th>
                    <th>Action</th>
                    </thead>

                    <tbody>
                    {medicineTypes?.map((item, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{item.name}</td>
                            <td>{item.description}</td>
                            <td>
                                <button className="btn btn-success"
                                        onClick={() => this.editModal(item)}>edit
                                </button>
                                <button className="btn btn-danger"
                                        onClick={() => this.deleteModal(item)}>delete
                                </button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>{currentMedicineType?"Edit Medicine type":"Save medicine Type"}</ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveMedicineType}>
                            <AvField
                                validate={{required: {value: true, errorMessage: "Please enter name"}}}
                                defaultValue={currentMedicineType.name}
                                placeholder="enter name here"
                                name="name"/>
                            <AvField
                                // validate={{required: {value: true, errorMessage: "Please enter name"}}}
                                defaultValue={currentMedicineType.description}
                                placeholder="enter description here"
                                name="description"/>
                            <button className="btn btn-success">save</button>
                            <button className="btn btn-danger">cancel</button>
                        </AvForm>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.showDeleteModal}>
                    <ModalHeader>{"Do you want delete this type: " + currentMedicineType.name}</ModalHeader>
                    <ModalFooter>
                        <button className="btn btn-danger"
                                onClick={this.deleteType}>Delete
                        </button>
                        <button className="btn btn-success"
                                onClick={this.openDeleteModal}>Cancel
                        </button>
                    </ModalFooter>
                </Modal>


            </div>
        );
    }
}

MedicineType.propTypes = {};

export default MedicineType;