package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.MadeYear;
import ai.ecma.appambulance.repository.MadeYearRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MadeYearService {
    @Autowired
    MadeYearRepository madeYearRepository;

    public List<MadeYear> getMadeYearService() {
        return madeYearRepository.findAll();
    }

    public String addMadeYearService(MadeYear madeYear) {
        try {
            madeYearRepository.save(madeYear);
            return "Save";
        } catch (Exception e) {
            return "Not Save";
        }
    }

    public MadeYear getMadeYearByIdService(Integer id) {
        try {
            return madeYearRepository.findById(id).get();
        } catch (Exception t) {
            return new MadeYear();
        }
    }

    public String deleteMadeYearService(Integer id) {
        try {
            madeYearRepository.deleteById(id);
            return "Deleted";
        } catch (Exception y) {
            return "Not Delete";
        }
    }

    public String updateMadeYearService(Integer id, MadeYear madeYear) {
        Optional<MadeYear> optionalMadeYear = madeYearRepository.findById(id);
        if (!optionalMadeYear.isPresent())
            return "not updated";

        MadeYear madeYear1 = optionalMadeYear.get();
        madeYear1.setValue(madeYear.getValue());
        madeYearRepository.save(madeYear1);
        return "updated";
    }

}
