package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Management;
import ai.ecma.appambulance.entity.Ministry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ManagementRepository extends JpaRepository<Management, UUID> {
    Boolean existsByName(String name);

}
