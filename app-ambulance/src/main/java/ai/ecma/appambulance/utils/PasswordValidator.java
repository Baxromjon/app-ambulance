package ai.ecma.appambulance.utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<ValidPassword, Object> {


    private String password;
    private String prePassword;

    @Override
    public void initialize(ValidPassword constraintAnnotation) {
        password = constraintAnnotation.password();
        prePassword=constraintAnnotation.prePassword();
    }

    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        return password!=null&&password.equals(prePassword);
    }
}
