import React, {Component} from 'react';
import PropTypes from 'prop-types';
import request from "../../utils/request";
import api from "../../utils/api";
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'
import {TOKEN} from "../../utils/constant";

class Address extends Component {
    state = {
        address: [],
        currentAddress: '',
        district: [],
        region: [],
        currentDistrict: '',
        showModal: false,
        showModalDelete: false
    }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getAddress()
            this.getDistrict()
        }
    }

    getAddress = () => {
        request({
            url: api.getAddress,
            method: 'GET'
        }).then(res => {
            this.setState({address: res.data})
        }).catch(err => {
        })
    }
    getDistrict = () => {
        request({
            url: api.getDistrict,
            method: 'GET'
        }).then(res => {
            this.setState({district: res.data})
        }).catch(err => {
        })
    }
    saveAddress = (e, v) => {
        let current = this.state.currentAddress
        request({
            url: !current ? api.addAddress : (api.editAddress + '/' + current.id),
            method: !current ? 'POST' : 'PUT',
            data: v
        }).then(res => {
            this.getAddress()
            this.closeModal()
        }).catch(err => {
        })
    }
    editAddress = (address) => {
        this.openModal()
        this.setState({currentAddress: address})
    }
    openModal = () => {
        this.setState({showModal: true})
    }
    closeModal = () => {
        this.setState({showModal: false})
    }
    deleteModal = (address) => {
        console.log(address.id)
        this.setState({
            currentAddress: address,
            showModalDelete: true
        })
    }
    hideDeleteModal = () => {
        this.setState({
            showModalDelete: false,
            currentAddress: ''
        })
    }
    deleteAddress = () => {
        let current = this.state.currentAddress
        console.log(current.id)
        request({
            url: api.deleteAddress + '/' + current.id,
            method: 'DELETE'
        }).then(res => {
            this.getAddress()
            this.hideDeleteModal()
        }).catch(err => {
        })
    }

    render() {
        const {address, district, currentAddress} = this.state
        return (
            <div>
                <h1 className="text-center">address page</h1>
                <button className="btn btn-success"
                        onClick={this.openModal}>+add address
                </button>
                <hr/>
                <table className="table table-bordered">
                    <thead>
                    <th>№</th>
                    <th>Address name</th>
                    <th>Street</th>
                    <th>District, Region</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    {address?.map((assembly, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{assembly.name}</td>
                            <td>{assembly.street}</td>
                            <td>{assembly.district.name + ', ' + assembly.district.region.name}</td>
                            <td>
                                <button className="btn btn-success"
                                        onClick={() => this.editAddress(assembly)}>edit
                                </button>
                                <button className="btn btn-danger"
                                        onClick={() => this.deleteModal(assembly)}>delete
                                </button>
                            </td>
                        </tr>
                    )) }
                    </tbody>
                </table>

                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>{currentAddress ? "Edit Address" : "Add Address"}</ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveAddress}>
                            <AvField
                                defaultValue={this.state.currentAddress.name}
                                placeholder="address name here"
                                name="name"/>
                            <AvField
                                defaultValue={this.state.currentAddress.street}
                                placeholder="address street here"
                                name="street"/>
                            <AvField
                                type="select"
                                name="districtId">
                                <option value="">Select District</option>
                                {district?.map(district => (
                                    <option value={district.id}>{district.name}</option>
                                ))}
                            </AvField>
                            <button className="btn btn-success">save</button>
                            <button className="btn btn-danger"
                                    onClick={this.closeModal}>cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>{"Do you want delete this address " + currentAddress.name}</ModalHeader>
                    <ModalFooter>
                        <button className="btn btn-success"
                                onClick={this.hideDeleteModal}>Cancel
                        </button>
                        <button className="btn btn-danger"
                                onClick={this.deleteAddress}>Delete
                        </button>
                    </ModalFooter>
                </Modal>

            </div>
        );
    }
}

Address.propTypes = {};

export default Address;