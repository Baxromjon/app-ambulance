import React, {Component} from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import request from "../../utils/request";
import api from "../../utils/api";
import {TOKEN} from "../../utils/constant";

class District extends Component {
    state = {
        districts: [],
        regions: [],
        currentCountry: '',
        currentDistrict: '',
        countries: [],
        currentRegion: '',
        showModal: false,
        showModalDelete: false
    }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getRegions();
            this.getDistrict();
            this.getCountries();
        }
    }

    openModal = () => {
        this.setState({showModal: true})
    }
    closeModal = () => {
        this.setState({showModal: false})
    }
    getRegions = () => {
        request({
            url: api.getRegion,
            method: 'GET'
        }).then(ans => {
            this.setState({regions: ans.data})
        }).catch(err => {
            console.log(err)
        })
    }
    getCountries = () => {
        request({
            url: api.getCountry,
            method: 'GET'
        }).then(res => {
            this.setState({countries: res.data})
        }).catch(err => {
        })
    }
    getAllRegionOfCountry = () => {
        let current = this.state.currentCountry
        request({
            url: api.getAllDistrictOfRegion + '/' + current,
            method: 'GET'
        }).then(res => {
            this.setState({regions: res.data.data})
        }).catch(err => {
        })
    }
    getCountry = (country) => {
        this.setState({currentCountry: country.target.value})
    }
    getDistrict = () => {
        request({
            url: api.getDistrict,
            method: 'GET'
        }).then(ans => {
            console.log(ans.data)
            this.setState({districts: ans.data})
        }).catch(err => {
        })
    }
    saveDistrict = (e, v) => {
        let current = this.state.currentDistrict;
        console.log(current)
        request({
            url: !current ? api.addDistrict : (api.editDistrict + '/' + current.id),
            method: !current ? 'POST' : 'PUT',
            data: v
        }).then(res => {
            console.log(res)
            this.getDistrict();
            this.closeModal();
        }).catch(err => {
        })
        console.log(v)
    }
    editDistrict = (district) => {
        this.openModal();
        this.setState({
            currentDistrict: district
        })
    }
    deleteModal = (district) => {
        this.setState({
            showModalDelete: true,
            currentDistrict: district
        })
    }
    hideDeleteModal = () => {
        this.setState({
            showModalDelete: false,
            currentDistrict: '',
        })
    }
    deleteDistrict = () => {
        let current = this.state.currentDistrict;
        request({
            url: api.deleteDistrict + '/' + current.id,
            method: 'DELETE',
        }).then(ans => {
            this.hideDeleteModal();
            this.getDistrict();
        }).catch(err => {
        })
    }

    render() {
        const {regions, countries} = this.state;
        return (
            <div>
                <h1 className="text-center">District List</h1>
                <button className="btn btn-info btn btn-outline-warning"
                        onClick={this.openModal}>add District
                </button>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>T/R</th>
                        <th>District Name</th>
                        <th>Region Name</th>
                        <th colSpan={2}>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.districts?.map((district, i) =>
                        (<tr key={i}>
                            <th>{i + 1}</th>
                            <td>{district.name}</td>
                            <td>{district.region.name}</td>
                            <td>
                                <button className="btn btn-info"
                                        onClick={() => this.editDistrict(district)}>Edit
                                </button>
                            </td>
                            <td>
                                <button className="btn btn-danger"
                                        onClick={() => this.deleteModal(district)}>Delete
                                </button>
                            </td>
                        </tr>)
                    )}
                    </tbody>
                </table>
                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>
                        {!this.state.currentDistrict ? 'Add District' : 'Edit District'}
                    </ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveDistrict}>
                            <AvField
                                defaultValue={this.state.currentDistrict.name}
                                name="name" required/>
                            <AvField
                                type="select"
                                name="countryId"
                                onChange={(country) => this.getCountry(country)}
                                onClick={this.getAllRegionOfCountry}>
                                <option value="" disabled>select country</option>
                                {countries?.map(country =>
                                    <option value={country.id}>{country.name}</option>
                                )}
                            </AvField>
                            <AvField
                                type="select"
                                name="regionId">
                                <option value="" disabled>select region</option>
                                {regions?.map(regions =>
                                    <option value={regions.id}>{regions.name}</option>
                                )}
                            </AvField>
                            <button
                                className="btn btn-success mt-3">{!this.state.currentDistrict ? 'Save' : 'Edit'}</button>
                            <button className="btn btn-danger mt-3"
                                // type="button"
                                    onClick={this.closeModal}>Cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>
                        {'Do you want delete this district: ' + this.state.currentDistrict.name}
                    </ModalHeader>
                    <ModalFooter>
                        <button onClick={this.deleteDistrict}
                                className="btn btn-danger">Delete
                        </button>
                        <button className="btn btn-success"
                                onClick={this.hideDeleteModal}>Cancel
                        </button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default District;