package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TimeTable extends AbsEntity {

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Car car;

    @Column(nullable = false)
    private Timestamp fromTime;

    @Column(nullable = false)
    private Timestamp toTime;

    private Double rateByHour;

    public TimeTable(User user, Timestamp fromTime, Timestamp toTime, Double rateByHour) {
        this.user = user;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.rateByHour=rateByHour;
    }
}
