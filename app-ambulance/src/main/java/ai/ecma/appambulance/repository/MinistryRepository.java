package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Country;
import ai.ecma.appambulance.entity.Ministry;
import ai.ecma.appambulance.projection.MinistryProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface MinistryRepository extends JpaRepository<Ministry, UUID> {
    Boolean existsByNameAndCountry(String name, Country country);

    @Query(nativeQuery = true, value = "select cast(m.id as varchar) as id, m.name from ministry m\n" +
            "inner join country c on m.country_id = c.id\n" +
            "where c.id=:id")
    List<MinistryProjection> getAllMinistryOfCountry(UUID id);
}
