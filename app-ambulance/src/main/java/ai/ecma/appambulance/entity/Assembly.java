package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Assembly extends AbsEntity {

    @Column(nullable = false)
    private String name;
    private String street;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Territory territory;
}
