package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.template.AbsNameEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.lang.invoke.LambdaConversionException;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Organization extends AbsNameEntity {
    @Column(nullable = false)
    private String name;
    @Column(nullable = false, unique = true)
    private String phoneNumber;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private District district;
    private String street;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Management management;

    @ManyToMany
    @JoinTable(name = "organization_organization_role",
            joinColumns = {@JoinColumn(name = "organization_id")},
            inverseJoinColumns = {@JoinColumn(name = "organization_role_id")})
    private List<OrganizationRole> organizationRoles;

    @ManyToMany
    @JoinTable(name = "organizations_organization_permissions",
            joinColumns = {@JoinColumn(name = "organization_id")},
            inverseJoinColumns = {@JoinColumn(name = "organization_permission_id")})
    private List<OrganizationPermission> permissionList;

}
