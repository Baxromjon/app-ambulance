import React, {Component} from 'react';
import request from "../../utils/request";
import api from "../../utils/api";
import {TOKEN} from "../../utils/constant";
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'

class MedicineFarm extends Component {
    state = {
        medicineFarms: [],
        currentMedicineFarm: '',
        district: [],
        currentDistrict: '',
        showModal: false,
        showModalDelete: false,
    }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getMedicineFarm()
            this.getDistrict()
        }
    }

    getDistrict = () => {
        request({
            url: api.getDistrict,
            method: 'GET'
        }).then(res => {
            this.setState({district: res.data})
        }).catch(err => {
        })
    }
    getMedicineFarm = () => {
        request({
            url: api.getMedicineFarm,
            method: 'GET'
        }).then(res => {
            this.setState({medicineFarms: res.data})
        }).catch(err => {
        })
    }
    saveMedicineFarm = (e, v) => {
        let current = this.state.currentMedicineFarm
        request({
            url: !current ? api.addMedicineFarm : (api.editMedicineFarm + '/' + current.id),
            method: !current ? 'POST' : 'PUT',
            data: v
        }).then(res => {
            this.getMedicineFarm()
            this.openModal()
        }).catch(err => {
        })
    }
    deleteFarm = () => {
        let current = this.state.currentMedicineFarm
        request({
            url: api.deleteMedicineFarm + '/' + current.id,
            method: 'DELETE'
        }).then(res => {
            this.getMedicineFarm()
            this.openModalDelete()
        }).catch(err => {
        })
    }
    openModal = () => {
        this.setState({showModal: !this.state.showModal})
    }
    openModalDelete = () => {
        this.setState({showModalDelete: !this.state.showModalDelete})
    }
    editModal = (item) => {
        console.log(item)
        this.openModal()
        this.setState({currentMedicineFarm: item})
    }
    deleteModal = (item) => {
        console.log(item)
        this.openModalDelete()
        this.setState({currentMedicineFarm: item})
    }


    render() {
        const {medicineFarms, currentMedicineFarm, district, currentDistrict} = this.state
        console.log(medicineFarms)
        return (
            <div>
                <h1 className="text-center">Medicine Farm page</h1>
                <button className="btn btn-success"
                        onClick={this.openModal}>add Medicine Farm
                </button>
                <hr/>
                <br/>
                <table className="table table-bordered text-center">
                    <thead>
                    <th>№</th>
                    <th>Medicine Farm name</th>
                    <th>description</th>
                    <th>Phone number</th>
                    <th>Street</th>
                    <th>District</th>
                    <th>Action</th>
                    </thead>

                    <tbody>
                    {medicineFarms?.map((item, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{item.name}</td>
                            <td>{item.description}</td>
                            <td>{item.phoneNumber}</td>
                            <td>{item.street}</td>
                            <td>{item.district.name}</td>
                            <td>
                                <button className="btn btn-success"
                                        onClick={() => this.editModal(item)}>edit
                                </button>
                                <button className="btn btn-danger"
                                        onClick={() => this.deleteModal(item)}>delete
                                </button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>

                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>{currentMedicineFarm ? "Edit Medicine Farm" : "Save Medicine Farm"}</ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveMedicineFarm}>
                            <AvField
                                validate={{required: {value: true, errorMessage: "Please enter name"}}}
                                defaultValue={currentMedicineFarm.name}
                                placeholder="enter name here"
                                name="name"/>
                            <AvField
                                // validate={{required: {value: true, errorMessage: "Please enter description"}}}
                                defaultValue={currentMedicineFarm.description}
                                placeholder="enter description here"
                                name="description"/>
                            <AvField
                                validate={{required: {value: true, errorMessage: "Please enter phone Number"}}}
                                defaultValue={currentMedicineFarm.phoneNumber}
                                placeholder="enter phone number here"
                                name="phoneNumber"/>
                            <AvField
                                validate={{required: {value: true, errorMessage: "Please enter street"}}}
                                defaultValue={currentMedicineFarm.street}
                                placeholder="enter street here"
                                name="street"/>
                            <AvField
                                validate={{required: {value: true, errorMessage: "Please select district"}}}
                                defaultValue={currentMedicineFarm.district}
                                name="districtId"
                                type="select">
                                <option value="">select district</option>
                                {district?.map(item =>
                                    <option value={item.id}>{item.name}</option>)}
                            </AvField>
                            <button className="btn btn-success">save</button>
                            <button className="btn btn-danger"
                                    onClick={this.openModal}>cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>{"Do you want delete this Farm: " + currentMedicineFarm.name}</ModalHeader>
                    <ModalFooter>
                        <button className="btn btn-danger"
                                onClick={this.deleteFarm}>delete
                        </button>
                        <button className="btn btn-success"
                                onClick={this.openModalDelete}>cancel
                        </button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

MedicineFarm.propTypes = {};

export default MedicineFarm;