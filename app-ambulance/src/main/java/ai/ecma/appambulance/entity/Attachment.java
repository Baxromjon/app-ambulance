package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.template.AbsEntity;
import ai.ecma.appambulance.entity.template.AbsNameEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
public class Attachment extends AbsNameEntity {
    private long size;
    private String contentType;
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "attachment",cascade = CascadeType.ALL)
    private AttachmentContent attachmentContent;

    public Attachment(long size, String contentType, AttachmentContent attachmentContent) {
        this.size = size;
        this.contentType = contentType;
        this.attachmentContent = attachmentContent;
    }
}
