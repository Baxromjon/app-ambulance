package ai.ecma.appambulance.entity.enums;

public enum OrderStatus {
    NEW,
    WAITING,
    IN_PROGRESS,
    CANCELED,
    CLOSED
}
