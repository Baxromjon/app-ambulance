package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.PhoneCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface PhoneCodeRepository extends JpaRepository<PhoneCode, UUID> {
//    @Query(value = "select t.* from PhoneCode t where t.id=:id and t.code=:code and t.checked=false ", nativeQuery = true)
//    Optional<PhoneCode> findByIdAndCode(UUID confirmationResult, String code);
}
