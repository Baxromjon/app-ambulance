package ai.ecma.appambulance.component;

import ai.ecma.appambulance.entity.*;
import ai.ecma.appambulance.entity.enums.OrganizationPermissionEnum;
import ai.ecma.appambulance.entity.enums.RoleName;
import ai.ecma.appambulance.entity.enums.SystemRoleEnum;
import ai.ecma.appambulance.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {


    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    SystemRoleRepository systemRoleRepository;
    @Autowired
    OrganizationPermissionRepository organizationPermissionRepository;
    @Autowired
    OrganizationRepository organizationRepository;
    @Autowired
    CountryRepository countryRepository;
    @Autowired
    MinistryRepository ministryRepository;
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    OrganizationRoleRepository organizationRoleRepository;

    @Value(value = "${spring.datasource.initialization-mode}")
    private String initialMode;


    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            User superAdmin = new User(
                    "SuperAdmin",
                    "SuperAdmin",
                    "AB",
                    "1234567",
                    "+998978333001",
                    passwordEncoder.encode("root123"),
                    Collections.singleton(systemRoleRepository.findBySystemRoleEnum(SystemRoleEnum.ROLE_SUPER_ADMIN))
            );
            User admin = new User(
                    "Admin",
                    "Admin",
                    "AA",
                    "1234567",
                    "+998990068005",
                    passwordEncoder.encode("root123"),
                    Collections.singleton(systemRoleRepository.findBySystemRoleEnum(SystemRoleEnum.ROLE_ADMIN))
            );
            Country country = new Country("O'zbekiston");
            countryRepository.save(country);
            Region andijon = new Region("Andijon", country);
            Region buxoro = new Region("Buxoro", country);
            Region namangan = new Region("Namangan", country);
            Region fargona = new Region("Farg`ona", country);
            Region toshkent = new Region("Toshkent", country);
            Region sirdaryo = new Region("Sirdaryo", country);
            Region jizzax = new Region("Jizzax", country);
            Region samarqand = new Region("Samarqand", country);
            Region qashqadaryo = new Region("Qashqadaryo", country);
            Region surxondaryo = new Region("Surxondaryo", country);
            Region navoiy = new Region("Navoiy", country);
            Region xorazm = new Region("Xorazm", country);
            Region toshkents = new Region("Toshkent shahri", country);
            Region nukus = new Region("Qoraqalpog`iston Respublikasi", country);
            List<Region> regions = Arrays.asList(andijon, fargona, namangan, toshkent, toshkents, samarqand, sirdaryo, jizzax, qashqadaryo, surxondaryo, navoiy, xorazm, buxoro, nukus);
            regionRepository.saveAll(regions);
            Ministry ministry = new Ministry("SSV", country);
            ministryRepository.save(ministry);
            List<User> users = Arrays.asList(admin, superAdmin);
            userRepository.saveAll(users);

            RoleName[] valuesRole=RoleName.values();
            List<OrganizationRole> organizationRoles=new ArrayList<>();
            for (RoleName valueRole:valuesRole){
                organizationRoles.add(new OrganizationRole(valueRole));
                organizationRoleRepository.saveAll(organizationRoles);
            }

            OrganizationPermissionEnum[] values = OrganizationPermissionEnum.values();
            List<OrganizationPermission> organizationPermissions = new ArrayList<>();
            for (OrganizationPermissionEnum value : values) {
                organizationPermissions.add(new OrganizationPermission(value));
                organizationPermissionRepository.saveAll(organizationPermissions);
            }
//            RoleName[] valuesRoles = RoleName.values();
//            List<OrganizationRole> roleList = new ArrayList<>();
//            for (RoleName name : valuesRoles) {
//                roleList.add(new OrganizationRole(name));
//                organizationRoleRepository.saveAll(roleList);
//            }
        }
    }
}
