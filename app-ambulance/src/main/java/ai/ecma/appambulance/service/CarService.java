package ai.ecma.appambulance.service;

import ai.ecma.appambulance.Exception.ResourceNotFoundException;
import ai.ecma.appambulance.entity.*;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResCar;
import ai.ecma.appambulance.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CarService {
    @Autowired
    CarRepository carRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ModelRepository modelRepository;
    @Autowired
    ColorRepository colorRepository;
    @Autowired
    MadeYearRepository madeYearRepository;
    @Autowired
    OrganizationRepository organizationRepository;

    public List<Car> getAll() {
        return carRepository.findAll();
    }

    public Car getById(UUID id) {
        Optional<Car> byId = carRepository.findById(id);
        return byId.get();
    }

    public ApiResponse addCar(ResCar resCar) {
        User user = userRepository.findById(resCar.getDriverId()).orElseGet(User::new);
        Boolean exists = carRepository.existsByCarLicenseSerialAndCarLicenseSerialNumberAndDrivers(resCar.getCarLicenseSerial(), resCar.getCarLicenseSerialNumber(), user);
        if (exists) {
            return new ApiResponse(false, "allReady exists");
        }
        Car car = new Car(
                modelRepository.findById(resCar.getModelId()).orElseGet(Model::new),
                colorRepository.findById(resCar.getColorId()).orElseGet(Color::new),
                resCar.getCarLicenseSerial(),
                resCar.getCarLicenseSerialNumber(),
                resCar.getDriverLicenseSerial(),
                resCar.getDriverLicenseSerialNumber(),
                resCar.getRegStateNumber(),
                Collections.singletonList(user),
                madeYearRepository.findById(resCar.getMadeYearId()).orElseGet(MadeYear::new),
                organizationRepository.findById(resCar.getOrganizationId()).orElseGet(Organization::new)
        );
        carRepository.save(car);
        return new ApiResponse(true, "successfully added");
    }

    public ApiResponse delete(UUID id) {
        try {
            carRepository.deleteById(id);
            return new ApiResponse(true, "successfully deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "not found");
        }
    }

    public ApiResponse edit(UUID id, ResCar resCar) {
        Optional<Car> byId = carRepository.findById(id);
        if (!byId.isPresent()) {
            return new ApiResponse(false, "not found");
        }
        Car car = byId.get();
        car.setColor(colorRepository.findById(resCar.getColorId()).orElseGet(Color::new));
//        car.setDriver(userRepository.findById(resCar.getDriverId()).orElseGet(User::new));
        car.setRegStateNumber(resCar.getRegStateNumber());
        car.setDriverLicenseSerial(resCar.getDriverLicenseSerial());
        car.setDriverLicenseSerialNumber(resCar.getDriverLicenseSerialNumber());
        carRepository.save(car);
        return new ApiResponse(true, "successfully edited");
    }

    public ApiResponse changeOnline(User user) throws Exception {
        Car car = carRepository.findByDrivers(user.getId()).orElseThrow(Exception::new);
        boolean online=car.isOnline();
        car.setOnline(!online);
        carRepository.save(car);
        return new ApiResponse(true,"successfully changed");
    }

    public ApiResponse updateLocation(User user, Float lat, Float lon) {
        Car car = carRepository.findByDrivers(user.getId()).orElseThrow(() -> new ResourceNotFoundException("car", "userId", user.getId()));
        car.setLat(lat);
        car.setLon(lon);
        carRepository.save(car);
        return new ApiResponse(true,"successfully updated");
    }

    public List<Car> getByOrganizationId(UUID organizationId) {
        return carRepository.getByOrganizationId(organizationId);
    }
}
