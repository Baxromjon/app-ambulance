package ai.ecma.appambulance.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResCar {
    private Integer modelId;
    private Integer colorId;
    private String carLicenseSerial;
    private String carLicenseSerialNumber;
    private String driverLicenseSerial;
    private String driverLicenseSerialNumber;
    private String regStateNumber;
    private UUID driverId;
    private Integer madeYearId;
    private UUID organizationId;
}
