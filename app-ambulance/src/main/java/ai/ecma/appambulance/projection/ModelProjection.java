package ai.ecma.appambulance.projection;

public interface ModelProjection {
    Integer getId();
    String getName();
}
