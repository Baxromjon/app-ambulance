package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.SickMan;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResSickMan;
import ai.ecma.appambulance.service.SickManService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/sick_man")
public class SickManController {
    @Autowired
    SickManService sickManService;

    @GetMapping
    public List<SickMan> getAll() {
        return sickManService.getAll();
    }

    @GetMapping("{id}")
    public SickMan getById(@PathVariable UUID id) {
        return sickManService.getById(id);
    }

    @PostMapping("save")
    public HttpEntity<?> addSickMan(@Valid @RequestBody ResSickMan resSickMan) {
        ApiResponse add = sickManService.add(resSickMan);
        return ResponseEntity.status(add.isSuccess() ? 200 : 409).body(add);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> edit(@Valid @PathVariable UUID id, @RequestBody ResSickMan resSickMan) {
        ApiResponse edit = sickManService.edit(id, resSickMan);
        return ResponseEntity.status(edit.isSuccess() ? 200 : 409).body(edit);
    }


}
