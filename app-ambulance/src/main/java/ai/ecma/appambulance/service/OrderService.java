package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Order;
import ai.ecma.appambulance.entity.Route;
import ai.ecma.appambulance.entity.enums.OrderStatus;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResOrder;
import ai.ecma.appambulance.repository.CarRepository;
import ai.ecma.appambulance.repository.OrderRepository;
import ai.ecma.appambulance.repository.RouteRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    RouteRepository routeRepository;
    @Autowired
    CarRepository carRepository;

    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    public Order getById(UUID id) {
        Optional<Order> optional = orderRepository.findById(id);
        return optional.get();
    }

    public ApiResponse addOrder(List<Route> routes) {
        Order order = new Order();
        order.setStatus(OrderStatus.NEW);
        orderRepository.save(order);
        int a = 0;
        for (Route route:routes){
            route.setOrder(order);
            route.setRouteIndex(++a);
        }
        routeRepository.saveAll(routes);
        return new ApiResponse(true, "searching");
    }


}
