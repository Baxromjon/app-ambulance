package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MedicineFarm extends AbsNameEntity {
    private String name;
    private String description;
    @ManyToOne(fetch = FetchType.LAZY)
    private District district;
    private String street;
    private String phoneNumber;
}
