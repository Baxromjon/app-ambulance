package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.template.AbsEntity;
import ai.ecma.appambulance.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Region extends AbsEntity {

    private String name;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Country country;
}
