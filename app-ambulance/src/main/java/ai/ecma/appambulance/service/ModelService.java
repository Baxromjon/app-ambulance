package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Brand;
import ai.ecma.appambulance.entity.Model;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResModel;
import ai.ecma.appambulance.projection.ModelProjection;
import ai.ecma.appambulance.repository.BrandRepository;
import ai.ecma.appambulance.repository.ModelRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.lang.Integer;

@Service
public class ModelService {
    @Autowired
    ModelRepository modelRepository;
    @Autowired
    BrandRepository brandRepository;

    public List<Model> getAllModel() {
        return modelRepository.findAll();
    }

    public ApiResponse addModel(ResModel resModel) {
        Optional<Brand> brandOptional = brandRepository.findById(resModel.getBrandId());
        Boolean exists = modelRepository.existsByNameAndBrand(resModel.getName(), brandOptional.get());
        if (exists) {
            return new ApiResponse(false, "this Model allReady exists");
        }
        Model model = new Model();
        model.setName(resModel.getName());
        model.setBrand(brandOptional.get());
        model.setDescription(resModel.getDescription());
        modelRepository.save(model);
        return new ApiResponse(true, "Model added");
    }

    public ApiResponse editModel(Integer id, ResModel resModel) {
        Optional<Brand> brandOptional = brandRepository.findById(resModel.getBrandId());
        Optional<Model> optional = modelRepository.findById(id);
        Boolean exists = modelRepository.existsByNameAndBrand(resModel.getName(), brandOptional.get());
        if (!optional.isPresent()) {
            return new ApiResponse(false, "Model not found");
        }
        if (exists) {
            return new ApiResponse(false, "Model allReady exists");
        }
        Model model = optional.get();
        model.setName(resModel.getName());
        model.setDescription(resModel.getDescription());
        model.setBrand(brandOptional.get());
        modelRepository.save(model);
        return new ApiResponse(true, "Model edited");
    }

    public Model getModelById(Integer id) {
        Optional<Model> optional = modelRepository.findById(id);
        if (!optional.isPresent()){
            return new Model();
        }
        return optional.get();
    }

    public ApiResponse deleteModel(Integer id) {
        modelRepository.deleteById(id);
        return new ApiResponse(true, "model deleted");
    }

    public List<ModelProjection> getAllModelOfBrand(Integer id) {
       return modelRepository.getAllModelOfBrand(id);
    }
}
