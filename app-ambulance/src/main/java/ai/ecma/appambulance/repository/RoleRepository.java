//package ai.ecma.appambulance.repository;
//
//import ai.ecma.appambulance.entity.Role;
//import ai.ecma.appambulance.entity.enums.RoleName;
//import ai.ecma.appambulance.entity.enums.SystemRoleEnum;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.util.List;
//import java.util.Set;
//
//public interface RoleRepository extends JpaRepository<Role, Integer> {
////    List<Role> findAllByName(RoleName roleName);
////
////    Role findByName(RoleName roleName);
////
////    Set<Role> findAllByRoleNameIn(Set<RoleName> names);
//
//    Role findByRoleName(RoleName roleName);
//}
