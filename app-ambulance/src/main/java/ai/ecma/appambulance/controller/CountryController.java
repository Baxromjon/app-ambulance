package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Country;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResCountry;
import ai.ecma.appambulance.service.CountryService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/country")
public class CountryController {
    @Autowired
    CountryService countryService;

    @GetMapping("getAll")
    public List<Country> getAllCountry() {
        return countryService.getAllCountry();
    }

    @PostMapping("save")
    public HttpEntity<?> addCountry(@RequestBody ResCountry resCountry) {
        ApiResponse apiResponse = countryService.addCountry(resCountry);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editCountry(@PathVariable UUID id, @RequestBody ResCountry resCountry) {
        ApiResponse apiResponse = countryService.editCountry(id, resCountry);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteCountry(@PathVariable UUID id) {
        ApiResponse apiResponse = countryService.deleteCountry(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("{id}")
    public Country getCountryById(@PathVariable UUID id){
       return countryService.getCountryById(id);
    }
}
