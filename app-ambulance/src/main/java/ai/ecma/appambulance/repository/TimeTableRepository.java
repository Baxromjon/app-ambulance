package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Car;
import ai.ecma.appambulance.entity.TimeTable;
import ai.ecma.appambulance.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Timestamp;
import java.util.UUID;

public interface TimeTableRepository extends JpaRepository<TimeTable, UUID> {

    Boolean existsByUserAndCarAndFromTime(User user, Car car, Timestamp fromTime);
}
