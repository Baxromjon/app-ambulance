//package ai.ecma.appambulance.repository;
//
//import ai.ecma.appambulance.entity.User;
//import ai.ecma.appambulance.entity.UserDevice;
//import ai.ecma.appambulance.projection.CustomUserDevice;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.util.UUID;
//
//
//public interface UserDeviceRepository extends JpaRepository<UserDevice, UUID> {
//    boolean existsByUser_PhoneNumberAndKey(String user_phoneNumber, UUID key);
//
//    Page<CustomUserDevice> findAllByUser(User user, Pageable pageable);
//}
