package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.MedicineName;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResMedicineName;
import ai.ecma.appambulance.repository.MedicineNameRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MedicineNameService {
    @Autowired
    MedicineNameRepository medicineNameRepository;

    public List<MedicineName> getAll() {
        List<MedicineName> all = medicineNameRepository.findAll();
        return all;
    }

    public MedicineName getById(UUID id) {
        Optional<MedicineName> byId = medicineNameRepository.findById(id);
        if (!byId.isPresent()) {
            return new MedicineName();
        }
        return byId.get();
    }

    public ApiResponse addMedicineName(ResMedicineName resMedicineName) {
        Boolean exists = medicineNameRepository.existsByName(resMedicineName.getName());
        if (exists) {
            return new ApiResponse(false, "Medicine Name allready exists");
        }
        MedicineName medicineName = new MedicineName();
        medicineName.setName(resMedicineName.getName());
        medicineName.setDescription(resMedicineName.getDescription());
        medicineNameRepository.save(medicineName);
        return new ApiResponse(true, "Medicine name added");
    }

    public ApiResponse editMedicineName(UUID id, ResMedicineName resMedicineName) {
        Optional<MedicineName> optional = medicineNameRepository.findById(id);
        if (!optional.isPresent()){
            return new ApiResponse(false, "Medicine Name not found");
        }
        MedicineName medicineName=optional.get();
        medicineName.setName(resMedicineName.getName());
        medicineName.setDescription(resMedicineName.getDescription());
        medicineNameRepository.save(medicineName);
        return new ApiResponse(true,"Medicine name edited");
    }

    public ApiResponse deleteMedicineName(UUID id) {
        medicineNameRepository.deleteById(id);
        return new ApiResponse(false, "Medicine name deleted");
    }
}
