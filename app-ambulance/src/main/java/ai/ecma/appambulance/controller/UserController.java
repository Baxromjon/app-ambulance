package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.User;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.UserDto;
import ai.ecma.appambulance.projection.DriverProjection;
import ai.ecma.appambulance.security.CurrentUser;
import ai.ecma.appambulance.service.UserService;
import com.google.protobuf.Api;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("api/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("me")
    public HttpEntity<?> getUserMe(@CurrentUser User user) {
        return ResponseEntity.ok(new ApiResponse(true, "success", user));
    }

//    @PostMapping("addLeader")
//    public HttpEntity<?> addLeader()

    @PutMapping("/edit")
    public HttpEntity<?> editProfile(@RequestBody UserDto userDto, @CurrentUser User user) {
        ApiResponse response = userService.editProfile(userDto, user);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    @PreAuthorize("hasAnyRole('ROLE_PERSONNEL_DEPARTMENT','ROLE_LEADER')")
    @PostMapping("register_employee")
    public HttpEntity<?> register(@RequestBody UserDto userDto) {
        ApiResponse register = userService.register(userDto);
        return ResponseEntity.status(register.isSuccess() ? 200 : 409).body(register);
    }

//    @PreAuthorize("hasAnyRole('ROLE_LEADER')")
//    @PostMapping("register_personnel_department")
//    public HttpEntity<?> registerPersonnelDepartment(@RequestBody UserDto userDto) {
//        ApiResponse apiResponse = userService.registerPersonnelDepartment(userDto);
//        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
//    }

//    @PreAuthorize("hasAnyRole('ROLE_ADMIN, ROLE_MODERATOR')")
//    @PostMapping("register_leader")
//    public HttpEntity<?> registerLeader(@RequestBody UserDto userDto) {
//        ApiResponse apiResponse = userService.registerLeader(userDto);
//        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
//    }

    @PutMapping("edit_employee/{id}")
    public HttpEntity<?> edit(@PathVariable UUID id, @RequestBody UserDto userDto) {
        ApiResponse apiResponse = userService.editEmployee(id, userDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> delete(@PathVariable UUID id) {
        ApiResponse delete = userService.delete(id);
        return ResponseEntity.status(delete.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(delete);
    }

    @GetMapping("getAllEmployee")
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("getAllDrivers")
    public List<DriverProjection> getAllDriver(){
        return userService.getAllDriver();
    }

    @GetMapping("getAllUserByOrgAndRole/{organizationId}/{roleId}")
        public List<User> getAllById(@PathVariable UUID organizationId,@PathVariable UUID roleId){
        return userService.getAllById(organizationId, roleId);
        }
}
