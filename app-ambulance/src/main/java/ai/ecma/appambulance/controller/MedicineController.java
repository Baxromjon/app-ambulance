package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Medicine;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResMedicine;
import ai.ecma.appambulance.service.MedicineService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/medicine")
public class MedicineController {
    @Autowired
    MedicineService medicineService;

    @GetMapping("getAllMedicine")
    public List<Medicine> getAllMedicine() {
        return medicineService.getAllMedicine();
    }

    @GetMapping("{id}")
    public Medicine getMedicineById(@PathVariable UUID id) {
        return medicineService.getMedicineById(id);
    }

    @PostMapping("saveMedicine")
    public HttpEntity<?> addMedicine(@Valid @RequestBody ResMedicine resMedicine) {
        ApiResponse apiResponse = medicineService.addMedicine(resMedicine);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("deleteMedicine/{id}")
    public HttpEntity<?> deleteMedicine(@PathVariable UUID id) {
        ApiResponse response = medicineService.delete(id);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    @PutMapping("editMedicine/{id}")
    public HttpEntity<?> editMedicine(@PathVariable UUID id, @RequestBody ResMedicine resMedicine) {
        ApiResponse apiResponse = medicineService.editMedicine(id, resMedicine);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}
