package ai.ecma.appambulance.projection;

import java.util.UUID;

public interface CustomUserDevice {
    UUID getId();
    String getUserAgent();
}
