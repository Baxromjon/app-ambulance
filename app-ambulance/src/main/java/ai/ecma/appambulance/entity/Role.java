//package ai.ecma.appambulance.entity;
//
//import ai.ecma.appambulance.entity.enums.RoleName;
//import ai.ecma.appambulance.entity.template.AbsEntity;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//import lombok.NoArgsConstructor;
//import org.springframework.security.core.GrantedAuthority;
//
//import javax.persistence.*;
//
//
//@EqualsAndHashCode(callSuper = true)
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Entity
//public class Role extends AbsEntity implements GrantedAuthority {
//    @Override
//    public String getAuthority() {
//        return roleName.toString();
//    }
//
//    @Enumerated(EnumType.STRING)
//    private RoleName roleName;
//
//}
