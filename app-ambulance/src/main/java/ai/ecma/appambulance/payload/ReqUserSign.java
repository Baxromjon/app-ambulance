package ai.ecma.appambulance.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqUserSign {
    @NotBlank
    private String phoneNumber;
    @NotBlank
    private String password;
    private String deviceKey;
    private Boolean deviceTrust=false;
}
