import React, {Component} from 'react';
import request from "../../utils/request";
import api from "../../utils/api";
import {TOKEN} from "../../utils/constant";
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'


class Country extends Component {
    state = {
        countries: [],
        currentCountry: '',
        showModal: false,
        showModalDelete: false,
    }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getCountries();
        }
    }

    getCountries = () => {
        request({
            url: api.getCountry,
            method: 'GET'
        }).then(res => {
            this.setState({countries: res.data})
        }).catch(err => {
            alert("xato")
        })
    }
    openModal = () => {
        this.setState({showModal: true})
    }
    closeModal = () => {
        this.setState({showModal: false})
    }
    saveCountry = (e, v) => {
        let current = this.state.currentCountry;
        request({
            url: current ? (api.editCountry + '/' + current.id) : api.addCountry + '',
            method: current ? 'PUT' : 'POST',
            data: v
        }).then(res => {
            this.getCountries();
            this.closeModal();
        }).catch(err => {
        })
    }
    deleteCountry = () => {
        let current = this.state.currentCountry;
        request({
            url: api.deleteCountry + '/' + current.id,
            method: 'DELETE',
        }).then(ans => {
            this.hideDeleteModal();
            this.getCountries();
        }).catch(err => {
        })
    }
    editCountry = (country) => {
        this.openModal();
        this.setState({currentCountry: country})
    }
    deleteModal = (country) => {
        this.setState({
            showModalDelete: true,
            currentCountry: country
        })
    }
    hideDeleteModal = () => {
        this.setState({
            showModalDelete: false,
            currentCountry: '',
        })
    }
    render() {
        return (
            <div>
                <h1 className="text-center">Country List</h1>
                <button className="btn btn-primary"
                onClick={this.openModal}>Add Country</button>
                <br/>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Country name</th>
                        <th >Option</th>
                    </tr>
                    </thead>

                    <tbody>
                    {this.state.countries?.map((country, index) =>
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{country.name}</td>
                            <td>
                                <button className="btn btn-primary"
                                        onClick={()=>this.editCountry(country)}>edit</button>
                            {/*</td>*/}
                            {/*<td>*/}
                                <button className="btn btn-danger"
                                        onClick={()=>this.deleteModal(country)}>delete</button>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </table>
                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>{this.state.currentCountry? 'Edit country':'Add Country'}</ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveCountry}>
                            <AvField
                                defaultValue={this.state.currentCountry.name}
                                name="name"/>
                            <button className="btn btn-success">Save</button>
                            <button type={"button"}
                                    onClick={this.closeModal}
                                    className="btn btn-danger">Cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>
                        {"Do you want really delete this Country " + this.state.currentCountry.name}
                    </ModalHeader>
                    <ModalFooter>
                        <button className="btn btn-info"
                                onClick={this.hideDeleteModal}>Cancel
                        </button>
                        <button className="btn btn-danger"
                                onClick={this.deleteCountry}>Delete
                        </button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

Country.propTypes = {};

export default Country;