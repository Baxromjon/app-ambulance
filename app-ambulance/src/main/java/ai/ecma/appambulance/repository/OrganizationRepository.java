package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Management;
import ai.ecma.appambulance.entity.Organization;
import ai.ecma.appambulance.projection.OrganizationProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.awt.*;
import java.util.List;
import java.util.UUID;

public interface OrganizationRepository extends JpaRepository<Organization, UUID> {
    Boolean existsByNameAndManagement(String name, Management management);

    @Query(nativeQuery = true, value = "select * from organization\n" +
            "where management_id=:managementId")
    List<Organization> getAllOrganization(UUID managementId);
}
