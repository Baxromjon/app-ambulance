package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CountryRepository extends JpaRepository<Country, UUID> {
    Boolean existsByName(String name);
}
