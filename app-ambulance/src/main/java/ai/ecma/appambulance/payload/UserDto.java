package ai.ecma.appambulance.payload;

import ai.ecma.appambulance.entity.Organization;
import ai.ecma.appambulance.entity.OrganizationPermission;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private UUID id;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank
    private String phoneNumberUser;
    @NotNull
    private Date birthDate;
    private UUID photo;
    private String passportSerial;
    private String passportSerialNumber;
    private String password;
    @NotNull
    private UUID organizationId;
    @NotNull
    private UUID organizationRoleId;
    private UUID positionId;
    private UUID photoId;
    private List<UUID> rolesId;
    private List<OrganizationPermission> permissionList;
    private List<UUID> permissionsId;


    public UserDto(UUID id, String firstName, String lastName, String phoneNumberUser, String password, UUID organizationId, UUID organizationRoleId, List<UUID> rolesId, List<UUID> permissionsId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumberUser = phoneNumberUser;
        this.password = password;
        this.organizationId = organizationId;
        this.organizationRoleId = organizationRoleId;
        this.rolesId = rolesId;
        this.permissionsId = permissionsId;
    }

    public UserDto(String firstName, String lastName, String phoneNumberUser, String passportSerial, String passportSerialNumber, String password, UUID organizationId, UUID organizationRoleId, UUID positionId, List<OrganizationPermission> permissionList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumberUser = phoneNumberUser;
        this.passportSerial = passportSerial;
        this.passportSerialNumber = passportSerialNumber;
        this.password = password;
        this.organizationId = organizationId;
        this.organizationRoleId = organizationRoleId;
        this.positionId = positionId;
        this.permissionList = permissionList;
    }
}
