package ai.ecma.appambulance.projection;

import ai.ecma.appambulance.entity.Brand;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "brandProjection", types = Brand.class)
public interface BrandProjection {

    UUID getId();

    String getName();

    String getDescription();
}
