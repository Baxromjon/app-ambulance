package ai.ecma.appambulance.entity.enums;

public enum SystemRoleEnum {
    ROLE_SUPER_ADMIN,
    ROLE_ADMIN,
    ROLE_MODERATOR,
    ROLE_USER
}
