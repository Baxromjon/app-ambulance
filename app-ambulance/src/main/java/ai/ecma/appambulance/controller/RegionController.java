package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Region;
import ai.ecma.appambulance.entity.RegionExcelExporter;
import ai.ecma.appambulance.entity.RegionPDFExporter;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.projection.DistrictObject;
import ai.ecma.appambulance.projection.RegionObject;
import ai.ecma.appambulance.projection.ResRegion;
import ai.ecma.appambulance.service.RegionService;
import com.lowagie.text.DocumentException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/region")
public class RegionController {

    @Autowired
    RegionService regionService;

    @GetMapping("getAll")
    public List<Region> getAllRegion() {
        return regionService.getAllRegion();
    }

    @GetMapping("/export/excel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormat.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment: filename=region_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        List<Region> regionList = regionService.listAll();

        RegionExcelExporter excelExporter = new RegionExcelExporter(regionList);
        excelExporter.export(response);
    }

    @GetMapping("/export/pdf")
    public void exportPDF(HttpServletResponse response) throws IOException, DocumentException {
        response.setContentType("application/pdf");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormat.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename+regions_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);

        List<Region> regionList = regionService.listAll();
        RegionPDFExporter exporter = new RegionPDFExporter(regionList);
        exporter.export(response);
    }

    @PostMapping("save")
    public HttpEntity<?> addRegion(@RequestBody ResRegion resRegion) {
        ApiResponse apiResponse = regionService.addRegion(resRegion);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("{id}")
    public Region getRegionById(@PathVariable UUID id) {
        return regionService.getRegionById(id);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editRegion(@PathVariable UUID id, @RequestBody ResRegion resRegion) {
        ApiResponse apiResponse = regionService.editRegion(id, resRegion);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteRegion(@PathVariable UUID id) {
        ApiResponse apiResponse = regionService.deleteRegion(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/getAllRegionOfCountry/{id}")
    public HttpEntity<?> getAllRegionOfCountry(@PathVariable UUID id) {
        List<RegionObject> allDistrictRegion = regionService.getAllRegionOfCountry(id);
        return ResponseEntity.ok(new ApiResponse(true, allDistrictRegion));
    }

    @GetMapping("getAllDistrictOfRegion/{id}")
    public HttpEntity<?> getAllDistrictOfRegion(@PathVariable UUID id) {
        List<DistrictObject> allDistrictRegion = regionService.getAllDistrictRegion(id);
        return ResponseEntity.ok(new ApiResponse(true, allDistrictRegion));
    }
}
