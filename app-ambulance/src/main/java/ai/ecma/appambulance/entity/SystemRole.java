package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.enums.SystemRoleEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SystemRole implements GrantedAuthority {
    @Id
    @Enumerated(EnumType.STRING)
    private SystemRoleEnum systemRoleEnum;

    @Override
    public String getAuthority() {
        return systemRoleEnum.name();
    }
}
