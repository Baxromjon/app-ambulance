package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Organization;
import ai.ecma.appambulance.entity.OrganizationRole;
import ai.ecma.appambulance.entity.enums.RoleName;
import ai.ecma.appambulance.payload.OrganizationRoleProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface OrganizationRoleRepository extends JpaRepository<OrganizationRole, UUID> {
    Boolean existsByName(String name);
    Set<OrganizationRole> findByName(RoleName name);

    @Query(nativeQuery = true,value = "select cast(ol.id as varchar) as value, ol.name as label\n" +
            "from organization_role ol")
    List<OrganizationRoleProjection> getAllOrganizationRole();

    @Query(nativeQuery = true, value = "select *\n" +
            "from organization_role ol\n" +
            "where ol.id in (:rolesId)")
    List<OrganizationRole> getAllById(List<UUID> rolesId);
}
