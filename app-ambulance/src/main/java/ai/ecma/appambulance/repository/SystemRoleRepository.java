package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.SystemRole;
import ai.ecma.appambulance.entity.enums.SystemRoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;


public interface SystemRoleRepository extends JpaRepository<SystemRole, SystemRoleEnum> {
    SystemRole findBySystemRoleEnum(SystemRoleEnum roleUser);
}
