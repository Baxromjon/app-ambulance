package ai.ecma.appambulance.payload;

/**
 * created by Baxromjon
 * 10.05.2021
 **/

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseJwt {

    private String tokenType="Bearer ";
    private String accessToken;

    public ResponseJwt(String token) {
        this.accessToken=token;
    }
}
