package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Assembly;
import ai.ecma.appambulance.entity.District;
import ai.ecma.appambulance.entity.Territory;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResAssembly;
import ai.ecma.appambulance.repository.AssemblyRepository;
import ai.ecma.appambulance.repository.DistrictRepository;
import ai.ecma.appambulance.repository.TerritoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AssemblyService {
    @Autowired
    AssemblyRepository assemblyRepository;
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    TerritoryRepository territoryRepository;

    public List<Assembly> getAllAssembly() {
        return assemblyRepository.findAll();
    }

    public ApiResponse addAssembly(ResAssembly resAssembly) {
        Optional<Territory> territoryOptional = territoryRepository.findById(resAssembly.getTerritoryId());
        Optional<District> districtOptional = districtRepository.findById(resAssembly.getDistrictId());
        Boolean existsAssemblyByStreetAndDistrict = assemblyRepository.existsAssembliesByNameAndTerritory(resAssembly.getName(), territoryOptional.get());
        if (existsAssemblyByStreetAndDistrict){
            return new ApiResponse(false, "this assembly allReady exists");
        }
        Assembly assembly=new Assembly(resAssembly.getName(),resAssembly.getStreet(),territoryOptional.get());
        assemblyRepository.save(assembly);
        return new ApiResponse(true,"Assembly added");
    }

    public ApiResponse deleteAssembly(UUID id) {
        assemblyRepository.deleteById(id);
        return new ApiResponse(true, "Assembly deleted");
    }

    public Assembly getAssemblyById(UUID id) {
        Optional<Assembly> optional = assemblyRepository.findById(id);
        if (!optional.isPresent()){
            return new Assembly();
        }
        return optional.get();
    }

    public ApiResponse editAssembly(UUID id, ResAssembly resAssembly) {
        Optional<Territory> territoryOptional = territoryRepository.findById(resAssembly.getTerritoryId());
        Optional<District> districtOptional = districtRepository.findById(resAssembly.getDistrictId());
        Optional<Assembly> optional = assemblyRepository.findById(id);
        if (!optional.isPresent()){
            return new ApiResponse(false,"Assembly not found");
        }
        Assembly assembly=optional.get();
        assembly.setName(resAssembly.getName());
        assembly.setTerritory(territoryOptional.get());
        assembly.setStreet(resAssembly.getStreet());
        assemblyRepository.save(assembly);
        return new ApiResponse(true, "Assembly edited");
    }
}
