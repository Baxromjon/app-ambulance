package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Country;
import ai.ecma.appambulance.entity.Region;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.projection.DistrictObject;
import ai.ecma.appambulance.projection.RegionObject;
import ai.ecma.appambulance.projection.ResRegion;
import ai.ecma.appambulance.repository.CountryRepository;
import ai.ecma.appambulance.repository.RegionRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RegionService {
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    CountryRepository countryRepository;

    public List<Region> getAllRegion() {
        return regionRepository.findAll();
    }

    public ApiResponse addRegion(ResRegion resRegion) {
        Optional<Country> optional = countryRepository.findById(resRegion.getCountryId());
        Boolean exists = regionRepository.existsByNameAndCountry(resRegion.getName(), optional.get());
        if (exists){
            return new ApiResponse(false,"Allready exists");
        }
        Region region=new Region(
                resRegion.getName(),
                optional.get()
        );
        regionRepository.save(region);
        return new ApiResponse(true,"Region added" );

    }


    public Region getRegionById(UUID id) {
        Optional<Region> optional = regionRepository.findById(id);
        if (!optional.isPresent()){
            return new Region();
        }
        return optional.get();
    }

    public ApiResponse editRegion(UUID id, ResRegion resRegion) {
        Optional<Country> optionalCountry = countryRepository.findById(resRegion.getCountryId());
        Optional<Region> optional = regionRepository.findById(id);
        if (!optional.isPresent()){
            return new ApiResponse(false,"This Region not exists" );
        }
        Region region=optional.get();
        region.setName(resRegion.getName());
//        region.setCountry(optionalCountry.get());
        regionRepository.save(region);
        return new ApiResponse(true,"Region edited");
    }

    public ApiResponse deleteRegion(UUID id) {
        regionRepository.deleteById(id);
        return new ApiResponse(true,"Region deleted");
    }

    public List<DistrictObject> getAllDistrictRegion(UUID id) {
        List<DistrictObject> allDistrictByRegionId = regionRepository.getAllDistrictOfRegion(id);
        return allDistrictByRegionId;
    }
    public List<RegionObject> getAllRegionOfCountry(UUID id) {
        List<RegionObject> allRegionOfCountry = regionRepository.getAllRegionOfCountry(id);
        return allRegionOfCountry;
    }

    public List<Region> listAll() {
        return regionRepository.findAll(Sort.by("name").ascending());
    }
}
