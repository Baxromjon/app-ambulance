import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {ToastContainer} from "react-toastify";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Region from "./region";
import District from "./district";
import Address from "./address";
import Country from "./country";


class AddressApp extends Component {
    render() {
        return (
            <div className="container pt-3">
                <ToastContainer/>
                <Router>
                   <Switch>
                       <Route exact path="/country" component={Country}/>
                       <Route exact path="/region" component={Region}/>
                       <Route exact path="/district" component={District}/>
                       <Route exact path="/address" component={Address}/>
                   </Switch>
                </Router>

            </div>
        );
    }
}

AddressApp.propTypes = {};

export default AddressApp;