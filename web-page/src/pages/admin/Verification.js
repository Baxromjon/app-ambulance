import React, {Component} from 'react';
import request from "../../utils/request";
import api from "../../utils/api";
import {TOKEN} from "../../utils/constant";

class Verification extends Component {
    componentDidMount() {
        let params = this.props.match.params;
        request({
            url: api.verification + '?phoneNumber=' + params.phoneNumber + '&code=' + params.code,
            method: 'GET'
        }).then(res => {
            localStorage.setItem(TOKEN, 'Bearer ' + res.data.object);
        })
    }

    routeToCabinet = () => {
        this.props.history.push('/login')
    }

    render() {
        return (
            <div>
                <h1>verification page</h1>
                <button onClick={this.routeToCabinet}
                className="btn btn-success"> Cabinetga o`tish

                </button>
            </div>
        );
    }


}

export default Verification;