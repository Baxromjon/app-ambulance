package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Assembly;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResAssembly;
import ai.ecma.appambulance.repository.DistrictRepository;
import ai.ecma.appambulance.service.AssemblyService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/assembly")
public class AssemblyController {
    @Autowired
    AssemblyService assemblyService;

    @GetMapping("getAll")
    public List<Assembly> getAllAssembly() {
        return assemblyService.getAllAssembly();
    }

    @PostMapping("save")
    public HttpEntity<?> addAssembly(@RequestBody ResAssembly resAssembly) {
        ApiResponse apiResponse = assemblyService.addAssembly(resAssembly);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteAssembly(@PathVariable UUID id) {
        ApiResponse apiResponse = assemblyService.deleteAssembly(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("{id}")
    public Assembly getAssemblyById(@PathVariable UUID id) {
        return assemblyService.getAssemblyById(id);
    }

    @PutMapping("update/{id}")
    public HttpEntity<?> editAssembly(@PathVariable UUID id, @RequestBody ResAssembly resAssembly) {
        ApiResponse apiResponse = assemblyService.editAssembly(id, resAssembly);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

}
