package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.Brand;
import ai.ecma.appambulance.projection.BrandProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

//@RepositoryRestResource(path = "api/brand", collectionResourceRel = "list", excerptProjection = BrandProjection.class)
public interface BrandRepository extends JpaRepository<Brand, Integer> {
    boolean existsByName(String name);

}
