package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.enums.OrganizationPermissionEnum;
import ai.ecma.appambulance.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class OrganizationPermission extends AbsEntity implements GrantedAuthority {

    @Enumerated(EnumType.STRING)
    private OrganizationPermissionEnum organizationPermissionEnum;

    @Override
    public String getAuthority() {
        return organizationPermissionEnum.name();
    }
}
