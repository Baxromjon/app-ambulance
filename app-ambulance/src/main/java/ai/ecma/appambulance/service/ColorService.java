package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Color;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResColor;
import ai.ecma.appambulance.repository.ColorRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ColorService {
    @Autowired
    ColorRepository colorRepository;

    public List<Color> getAllColor() {
        return colorRepository.findAll();
    }

    public ApiResponse addColor(ResColor resColor) {
        Boolean exists = colorRepository.existsByNameAndCode(resColor.getName(), resColor.getCode());
        if (exists){
            return new ApiResponse(false, "this color allReady exists");
        }
        Color color=new Color(resColor.getName(), resColor.getCode(),resColor.getDescription());
        colorRepository.save(color);
        return new ApiResponse(true,"color added");
    }

    public ApiResponse editColor(Integer id, ResColor resColor) {
        Optional<Color> optional = colorRepository.findById(id);
        if (!optional.isPresent()){
            return new ApiResponse(false, "not found this color");
        }
        Color color=optional.get();
        color.setCode(resColor.getCode());
        color.setName(resColor.getName());
        color.setDescription(resColor.getDescription());
        colorRepository.save(color);
        return new ApiResponse(true,"Color edited");
    }

    public ApiResponse deleteColor(Integer id) {
        colorRepository.deleteById(id);
        return new ApiResponse(true, "color deleted");
    }

    public Color getColorById(Integer id) {
        Optional<Color> optional = colorRepository.findById(id);
        if (!optional.isPresent()){
            return new Color();
        }
        return optional.get();
    }
}
