import React, {Component} from 'react';
import {TOKEN} from "../../utils/constant";
import request from "../../utils/request";
import api from "../../utils/api";
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'

class Medicine extends Component {
    state = {
        medicine: [],
        currentMedicine: '',
        medicineFarm: [],
        currentMedicineFarm: '',
        showModal: false,
        showModalDelete: false
    }

    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getMedicine()
            this.getMedicineFarm()
        }
    }

    getMedicine = () => {
        request({
            url: api.getAllMedicine,
            method: 'GET'
        }).then(res => {
            this.setState({medicine: res.data})
        }).catch(err => {
        })
    }
    saveMedicine = (e, v) => {
        console.log(v)
        let current = this.state.currentMedicine
        request({
            url: !current ? api.addMedicine : (api.editMedicine + '/' + current.id),
            method: !current ? 'POST' : 'PUT',
            data: v
        }).then(res => {
            this.getMedicine()
            this.openModal()
        }).catch(err => {
        })
    }
    openModal = () => {
        this.setState({showModal: !this.state.showModal})
    }
    getMedicineFarm=()=>{
        request({
            url:api.getMedicineFarm,
            method:'GET'
        }).then(res=>{
            this.setState({medicineFarm:res.data})
        }).catch(err=>{})
    }

    render() {
        const {medicine, currentMedicine, medicineFarm, currentMedicineFarm} = this.state
        console.log(medicine)
        console.log(medicineFarm)

        return (
            <div>
                <h1 className="text-center">Medicine page</h1>
                <button className="btn btn-success"
                        onClick={this.openModal}>add medicine
                </button>
                <hr/>
                <br/>
                <table className="table table-bordered text-center">
                    <thead>
                    <th>№</th>
                    <th>Medicine name</th>
                    <th>Accepted at</th>
                    <th>Amount</th>
                    <th>description</th>
                    <th>Expired date</th>
                    <th>Price</th>
                    <th>Medicine Farm name</th>
                    <th>Action</th>
                    </thead>
                </table>

                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>Save Medicine</ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveMedicine}>
                            <AvField
                                defaultValue={currentMedicine.name}
                                placeholder="enter medicine name here"
                                name="name"/>
                            <div className="row m-0">
                                <div className="col-md-6">
                                    <AvField
                                        defaultValue={currentMedicine.acceptedAt}
                                        placeholder="enter medicine accepted date here"
                                        name="acceptedAt"
                                        label="Accepted date"
                                        type="date"/>
                                </div>
                                <div className="col-md-6">
                                    <AvField
                                        defaultValue={currentMedicine.amount}
                                        placeholder="enter medicine amount here"
                                        name="amount"
                                        label="Amount"
                                        type="number"/>
                                </div>
                            </div>
                            <div className="row m-0">
                                <div className="col-md-6">
                                    <AvField
                                        defaultValue={currentMedicine.expiredDate}
                                        placeholder="enter medicine expired date here"
                                        name="expiredDate"
                                        label="Expired date"
                                        type="date"/>
                                </div>
                                <div className="col-md-6">
                                    <AvField
                                        defaultValue={currentMedicine.price}
                                        placeholder="enter medicine price here"
                                        name="price"
                                        label="price"/>
                                </div>
                            </div>
                            <AvField
                                defaultValue={currentMedicine.description}
                                placeholder="enter medicine description here"
                                name="description"/>
                            <AvField
                            name="medicineFarmId"
                            type="select">
                                <option value="">select Medicine Farm</option>
                                {medicineFarm?.map(item=>(
                                    <option value={item.id}>{item.name}</option>
                                ))}
                            </AvField>
                            <button className="btn btn-success">save</button>
                            <button className="btn btn-danger"
                            onClick={this.openModal}>cancel</button>

                        </AvForm>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}

Medicine.propTypes = {};

export default Medicine;