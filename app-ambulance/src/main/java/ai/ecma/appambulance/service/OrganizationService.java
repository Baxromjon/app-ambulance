package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.*;
import ai.ecma.appambulance.entity.enums.SystemRoleEnum;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.OrganizationRoleProjection;
import ai.ecma.appambulance.payload.ResOrganization;
import ai.ecma.appambulance.payload.UserDto;
import ai.ecma.appambulance.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class OrganizationService {
    @Autowired
    OrganizationRepository organizationRepository;
    @Autowired
    ManagementRepository managementRepository;
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    OrganizationRoleRepository organizationRoleRepository;
    @Autowired
    OrganizationPermissionRepository organizationPermissionRepository;
    @Autowired
    PositionRepository positionRepository;
    @Autowired
    SystemRoleRepository systemRoleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    public List<Organization> getAll() {
        return organizationRepository.findAll();
    }

    public Organization getById(UUID id) {
        Optional<Organization> optional = organizationRepository.findById(id);
        if (!optional.isPresent()) {
            return new Organization();
        }
        return optional.get();
    }

    @Transactional
    public ApiResponse addOrganization(ResOrganization resOrganization) {
        Management management = managementRepository.findById(resOrganization.getManagementId()).orElseGet(Management::new);
        Boolean exists = organizationRepository.existsByNameAndManagement(resOrganization.getName(), management);
        List<OrganizationRole> organizationRole = organizationRoleRepository.getAllById(resOrganization.getRolesId());
        List<OrganizationPermission> permissionList = organizationPermissionRepository.getAllById(resOrganization.getPermissionsId());
        if (exists) {
            return new ApiResponse(false, "Organization allReady exists");
        }
        District district = districtRepository.findById(resOrganization.getDistrictId()).orElseGet(District::new);
        Organization organization = new Organization(
                resOrganization.getName(),
                resOrganization.getPhoneNumber(),
                district,
                resOrganization.getStreet(),
                management,
                organizationRole,
                permissionList
        );
        organizationRepository.save(organization);

        Position position=new Position(
                "Bo`lim mudiri",
                "",
                organization
        );
        positionRepository.save(position);
        User rahbar=new User(
                resOrganization.getUserDto().getFirstName(),
                resOrganization.getUserDto().getLastName(),
                resOrganization.getUserDto().getPassportSerial(),
                resOrganization.getUserDto().getPassportSerialNumber(),
                resOrganization.getUserDto().getPhoneNumberUser(),
                passwordEncoder.encode(resOrganization.getUserDto().getPassword()),
                Collections.singleton(systemRoleRepository.findBySystemRoleEnum(SystemRoleEnum.ROLE_USER))
        );
        userRepository.save(rahbar);
        return new ApiResponse(true, "successfully added");
    }

    public ApiResponse editOrganization(UUID id, ResOrganization resOrganization) {
        District district = districtRepository.findById(resOrganization.getDistrictId()).orElseGet(District::new);
        Management management = managementRepository.findById(resOrganization.getManagementId()).orElseGet(Management::new);
        Optional<Organization> optional = organizationRepository.findById(id);
        if (!optional.isPresent()) {
            return new ApiResponse(false, "not found");
        }
        Organization organization = optional.get();
        organization.setName(resOrganization.getName());
        organization.setPhoneNumber(resOrganization.getPhoneNumber());
        organization.setDistrict(district);
        organization.setStreet(resOrganization.getStreet());
        organization.setManagement(management);
        organizationRepository.save(organization);
        return new ApiResponse(true, "successfully edited");
    }

    public ApiResponse delete(UUID id) {
        try {
            organizationRepository.deleteById(id);
            return new ApiResponse(true, "successfully edited");
        } catch (Exception e) {
            return new ApiResponse(false, "error");
        }
    }

    public List<Organization> getAllOrganization(UUID managementId) {
        List<Organization> allOrganization = organizationRepository.getAllOrganization(managementId);
        return allOrganization;
    }
}
