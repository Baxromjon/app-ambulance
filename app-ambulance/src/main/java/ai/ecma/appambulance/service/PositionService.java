package ai.ecma.appambulance.service;

/**
 * created by Baxromjon
 * 10.05.2021
 **/

import ai.ecma.appambulance.entity.Organization;
import ai.ecma.appambulance.entity.Position;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.PositionDTO;
import ai.ecma.appambulance.repository.OrganizationRepository;
import ai.ecma.appambulance.repository.PositionRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PositionService {
    @Autowired
    PositionRepository positionRepository;
    @Autowired
    OrganizationRepository organizationRepository;

    public List<Position> getAllPosition() {
        return positionRepository.findAll();
    }

    public ApiResponse savePosition(PositionDTO positionDTO) {
        Organization organization = organizationRepository.findById(positionDTO.getOrganizationId()).orElseGet(Organization::new);
        Boolean exists = positionRepository.existsByNameAndOrganization(positionDTO.getName(), organization);
        if (exists) {
            return new ApiResponse(false, "allready exists");
        }
        Position position = new Position(
                positionDTO.getName(),
                positionDTO.getDescription(),
                organization
        );
        positionRepository.save(position);
        return new ApiResponse(true, "successfully saved");
    }

    public ApiResponse editPosition(UUID id, PositionDTO positionDTO) {
        Optional<Position> positionOptional = positionRepository.findById(id);
        Optional<Organization> organizationOptional = organizationRepository.findById(positionDTO.getOrganizationId());
        if (!positionOptional.isPresent()){
            return new ApiResponse(false,"not find this position");
        }
        Position position=positionOptional.get();
        position.setName(positionDTO.getName());
        position.setDescription(positionDTO.getDescription());
        position.setOrganization(organizationOptional.get());
        positionRepository.save(position);
        return new ApiResponse(true, "successfully saved");
    }

    public ApiResponse deletePosition(UUID id) {
        positionRepository.deleteById(id);
        return new ApiResponse(true, "successfully deleted");
    }

    public Position getById(UUID id) {
        Optional<Position> optional = positionRepository.findById(id);
        return optional.get();
    }
}
