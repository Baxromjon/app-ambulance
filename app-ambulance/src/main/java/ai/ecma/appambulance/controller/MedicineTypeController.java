package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.MedicineType;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResMedicineType;
import ai.ecma.appambulance.service.MedicineTypeService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/medicineType")
public class MedicineTypeController {
    @Autowired
    MedicineTypeService medicineTypeService;

    @GetMapping("getAllMedicineType")
    public List<MedicineType> getAllType() {
        return medicineTypeService.getAllType();
    }

    @GetMapping("{id}")
    public MedicineType getTypeById(@PathVariable Integer id) {
        return medicineTypeService.getTypeById(id);
    }

    @PostMapping("save")
    public HttpEntity<?> addMedicineType(@Valid @RequestBody ResMedicineType resMedicineType) {
        ApiResponse apiResponse = medicineTypeService.addMedicineType(resMedicineType);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editMedicineType(@Valid @PathVariable Integer id, @RequestBody ResMedicineType resMedicineType) {
        ApiResponse apiResponse = medicineTypeService.editMedicineType(id, resMedicineType);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteMapping(@PathVariable Integer id) {
        ApiResponse delete = medicineTypeService.delete(id);
        return ResponseEntity.status(delete.isSuccess() ? 200 : 409).body(delete);
    }
}
