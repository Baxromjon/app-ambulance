package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.*;
import ai.ecma.appambulance.entity.enums.SystemRoleEnum;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.UserDto;
import ai.ecma.appambulance.projection.DriverProjection;
import ai.ecma.appambulance.repository.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    OrganizationRepository organizationRepository;
    @Autowired
    OrganizationRoleRepository organizationRoleRepository;
    @Autowired
    SystemRoleRepository systemRoleRepository;
    @Autowired
    OrganizationPermissionRepository organizationPermissionRepository;

    public ApiResponse editProfile(UserDto userDto, User user) {
        if (passwordEncoder.matches(userDto.getPassword(), user.getPassword())) {
            user.setFirstName(userDto.getFirstName());
            user.setLastName(userDto.getLastName());
            user.setPassword(userDto.getPassword());
            if (!user.getPhoneNumber().equals(userDto.getPhoneNumberUser())) {
                if (userRepository.existsByPhoneNumber(userDto.getPhoneNumberUser())) {
                    return new ApiResponse("Allready exists", false);
                }
            }
            if (userDto.getPhoto() != null)
                user.setPhoto(attachmentRepository.findById(userDto.getPhoto()).orElseThrow(() -> new ResourceNotFoundException("Rasm topilmadi" + userDto.getPhoto())));
            user.setPhoneNumber(userDto.getPhoneNumberUser());
            userRepository.save(user);
            return new ApiResponse("Tahrirlandi", true);
        } else {
            return new ApiResponse("Parol xato", false);
        }

    }

    public ApiResponse register(UserDto userDto) {
        Boolean exists = userRepository.existsByFirstNameAndLastNameAndPhoneNumber(userDto.getFirstName(), userDto.getLastName(), userDto.getPhoneNumberUser());
        if (exists) {
            return new ApiResponse(false, "allReady exists");
        }
        Organization organization = organizationRepository.findById(userDto.getOrganizationId()).orElseGet(Organization::new);
        OrganizationRole organizationRole = organizationRoleRepository.findById(userDto.getOrganizationRoleId()).orElseGet(OrganizationRole::new);
        List<OrganizationPermission> permissionList = organizationPermissionRepository.getAllById(userDto.getPermissionsId());
        User user = new User(
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getPassportSerial(),
                userDto.getPassportSerialNumber(),
                userDto.getPhoneNumberUser(),
                passwordEncoder.encode("root123"),
                Collections.singletonList(organization),
                organizationRole,
                Collections.singleton(systemRoleRepository.findBySystemRoleEnum(SystemRoleEnum.ROLE_USER)),
                permissionList
        );
        userRepository.save(user);
        return new ApiResponse(true, "successfully saved");
    }

    public ApiResponse editEmployee(UUID id, UserDto userDto) {
        Attachment attachment = attachmentRepository.findById(userDto.getPhotoId()).orElseGet(Attachment::new);
        Optional<User> optional = userRepository.findById(id);
        if (!optional.isPresent()) {
            return new ApiResponse(false, "not found");
        }
        User user = optional.get();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPhoneNumber(userDto.getPhoneNumberUser());
        user.setPhoto(attachment);
        user.setPassword(userDto.getPassword());
        userDto.setOrganizationRoleId(userDto.getOrganizationRoleId());
        userRepository.save(user);
        return new ApiResponse(true, "successfully edited");
    }

    public ApiResponse delete(UUID id) {
        try {
            userRepository.deleteById(id);
            return new ApiResponse(true, "successfully deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "not found");
        }
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public List<DriverProjection> getAllDriver() {
        return userRepository.getAllDriver();
    }

    public List<User> getAllById(UUID organizationId, UUID roleId) {
        return userRepository.getAllUser(organizationId, roleId);
    }

//    public ApiResponse registerPersonnelDepartment(UserDto userDto) {
//        Organization organization = organizationRepository.findById(userDto.getOrganizationId()).orElseGet(Organization::new);
//        OrganizationRole organizationRole = organizationRoleRepository.findById(userDto.getOrganizationRoleId()).orElseGet(OrganizationRole::new);
//        Boolean exists = userRepository.existsByFirstNameAndLastNameAndOrganizationRole(userDto.getFirstName(), userDto.getLastName(), organizationRole);
//        if (exists) {
//            return new ApiResponse(false, "allReady exists");
//        }
//        User user = new User(
//                userDto.getFirstName(),
//                userDto.getLastName(),
//                userDto.getPhoneNumber(),
//                userDto.getPassword(),
//                Collections.singletonList(organization),
//                organizationRole,
//                Collections.singleton(systemRoleRepository.findBySystemRoleEnum(SystemRoleEnum.ROLE_USER))
//        );
//        userRepository.save(user);
//        return new ApiResponse(true, "successfully registered");
//    }

//    public ApiResponse registerLeader(UserDto userDto) {
//        Organization organization = organizationRepository.findById(userDto.getOrganizationId()).orElseGet(Organization::new);
//        OrganizationRole organizationRole = organizationRoleRepository.findById(userDto.getOrganizationRoleId()).orElseGet(OrganizationRole::new);
//        Boolean exists1 = userRepository.existsByOrganizationRoleAndOrganization(organizationRole, Collections.singletonList(organization));
//        if (exists1) {
//            return new ApiResponse(false, "allReady exists");
//        }
//        User leader = new User(
//                userDto.getFirstName(),
//                userDto.getLastName(),
//                userDto.getPhoneNumber(),
//                userDto.getPassword(),
//                Collections.singletonList(organization),
//                organizationRole,
//                Collections.singleton(systemRoleRepository.findBySystemRoleEnum(SystemRoleEnum.ROLE_USER))
//        );
//        userRepository.save(leader);
//        return new ApiResponse(true, "successfully registered");
//    }
}
