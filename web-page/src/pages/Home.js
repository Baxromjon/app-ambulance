import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Home extends Component {
    routeToLogin=()=>{
        this.props.history.push("/login")
    }
    render() {
        return (
            <div>
                <h1>ambulance home page</h1>
                <div>
                    <button className="btn btn-success"
                    onClick={this.routeToLogin}>login</button>
                </div>
            </div>
        );
    }
}

Home.propTypes = {};

export default Home;