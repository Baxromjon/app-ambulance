package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.Exception.ResourceNotFoundException;
import ai.ecma.appambulance.entity.Order;
import ai.ecma.appambulance.entity.Route;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResOrder;
import ai.ecma.appambulance.repository.OrderRepository;
import ai.ecma.appambulance.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/orders")
public class OrderController {
    @Autowired
    OrderService orderService;
    @Autowired
    OrderRepository orderRepository;

    @GetMapping("getAllOrders")
    public List<Order> getAll(){
        return orderService.getAll();
    }

    @GetMapping("{id}")
    public Order getById(@PathVariable UUID id){
        return orderService.getById(id);
    }

    @PostMapping("createOrder")
    public HttpEntity<?> createOrder(@RequestBody List<Route> routes){
        ApiResponse apiResponse = orderService.addOrder(routes);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);
    }


}
