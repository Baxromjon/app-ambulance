package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Medicine;
import ai.ecma.appambulance.entity.MedicineFarm;
import ai.ecma.appambulance.entity.MedicineType;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResMedicine;
import ai.ecma.appambulance.repository.MedicineFarmRepository;
import ai.ecma.appambulance.repository.MedicineRepository;
import ai.ecma.appambulance.repository.MedicineTypeRepository;
import autovalue.shaded.org.objectweb$.asm.$Opcodes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MedicineService {
    @Autowired
    MedicineRepository medicineRepository;
    @Autowired
    MedicineFarmRepository medicineFarmRepository;
    @Autowired
    MedicineTypeRepository medicineTypeRepository;

    public List<Medicine> getAllMedicine() {
        return medicineRepository.findAll();
    }

    public Medicine getMedicineById(UUID id) {
        Optional<Medicine> optional = medicineRepository.findById(id);
        if (!optional.isPresent()){
            return new Medicine();
        }
        return optional.get();
    }

    public ApiResponse addMedicine(ResMedicine resMedicine) {
        MedicineFarm medicineFarm = medicineFarmRepository.findById(resMedicine.getMedicineFarmId()).orElseGet(MedicineFarm::new);
        MedicineType medicineType = medicineTypeRepository.findById(resMedicine.getMedicineTypeId()).orElseGet(MedicineType::new);
        Boolean exists = medicineRepository.existsByName(resMedicine.getName());
        if (exists){
            return new ApiResponse(false, "Medicine allReady exists");
        }
        Medicine medicine=new Medicine();
        medicine.setName(resMedicine.getName());
        medicine.setDescription(resMedicine.getDescription());
        medicine.setAmount(resMedicine.getAmount());
        medicine.setPrice(resMedicine.getPrice());
        medicine.setAcceptedDate(resMedicine.getAcceptedDate());
        medicine.setExpiredDate(resMedicine.getExpiredDate());
        medicine.setMedicineFarm(medicineFarm);
        medicine.setMedicineTypes(Collections.singletonList(medicineType));
        medicineRepository.save(medicine);
        return new ApiResponse(true, "Medicine added");
    }

    public ApiResponse delete(UUID id) {
        try {
            medicineRepository.deleteById(id);
            return new ApiResponse(true, "Medicine deleted");
        }catch (Exception e){
            return new ApiResponse(false,"not found");
        }
    }

    public ApiResponse editMedicine(UUID id, ResMedicine resMedicine){
        MedicineFarm medicineFarm = medicineFarmRepository.findById(resMedicine.getMedicineFarmId()).orElseGet(MedicineFarm::new);
        MedicineType medicineType = medicineTypeRepository.findById(resMedicine.getMedicineTypeId()).orElseGet(MedicineType::new);
        Optional<Medicine> optional = medicineRepository.findById(id);
        if (!optional.isPresent()){
            return new ApiResponse(false, "Medicine not found");
        }
        Medicine medicine=optional.get();
        medicine.setName(resMedicine.getName());
        medicine.setDescription(resMedicine.getDescription());
        medicine.setAmount(resMedicine.getAmount());
        medicine.setPrice(resMedicine.getPrice());
        medicine.setAcceptedDate(resMedicine.getAcceptedDate());
        medicine.setExpiredDate(resMedicine.getExpiredDate());
        medicine.setMedicineFarm(medicineFarm);
        medicine.setMedicineTypes(Collections.singletonList(medicineType));
        medicineRepository.save(medicine);
        return new ApiResponse(true, "Medicine edited");
    }
}
