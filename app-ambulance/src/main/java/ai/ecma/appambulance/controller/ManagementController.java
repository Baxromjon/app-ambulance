package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Management;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.ResManagement;
import ai.ecma.appambulance.service.ManagementService;
import com.google.api.Http;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api/management")
public class ManagementController {
    @Autowired
    ManagementService managementService;

    @GetMapping("getAllManagements")
    public List<Management> getAll() {
        return managementService.getAll();
    }

    @GetMapping("{id}")
    public Management getById(@PathVariable UUID id) {
        return managementService.getById(id);
    }

    @PostMapping("save")
    public HttpEntity<?> addManagement(@Valid @RequestBody ResManagement resManagement) {
        ApiResponse apiResponse = managementService.addManagement(resManagement);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("edit/{id}")
    public HttpEntity<?> editManagement(@Valid @PathVariable UUID id, @RequestBody ResManagement resManagement) {
        ApiResponse edit = managementService.edit(id, resManagement);
        return ResponseEntity.status(edit.isSuccess() ? 200 : 409).body(edit);
    }

    @DeleteMapping("delete/{id}")
    public HttpEntity<?> deleteManagement(@PathVariable UUID id) {
        ApiResponse delete = managementService.delete(id);
        return ResponseEntity.status(delete.isSuccess() ? 200 : 409).body(delete);
    }


}
