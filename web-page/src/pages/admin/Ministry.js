import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import request from "../../utils/request";
import api from "../../utils/api";
import {toast} from "react-toastify";
import {TOKEN} from "../../utils/constant";


class Ministry extends Component {
    state = {
        ministries: [],
        countries: [],
        currentMinistry: '',
        showModal: false,
        showModalDelete: false
    }
    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getMinistries()
            this.getCountries()
        }
    }
    openModal = () => {
        this.setState({showModal: true})
    }
    closeModal = () => {
        this.setState({showModal: false})
    }
    getCountries = () => {
        request({
            url: api.getCountry,
            method: 'GET',
        }).then(res => {
            this.setState({countries: res.data})
            console.log(res.data)
        }).catch(err => {
            alert("xatolik")
        })
    }
    getMinistries = () => {
        request({
            url: api.getMinistry,
            method: 'GET',
        }).then(res => {
            this.setState({ministries: res.data})
        }).catch(err => {
            alert("xato")
        })
    }
    saveMinistry = (e, v) => {
        let current = this.state.currentMinistry
        request({
            url: current ? (api.editMinistry + '/' + current.id) : api.addMinistry,
            method: current ? 'PUT' : 'POST',
            date: v
        }).then(res => {
            this.getMinistries()
            this.closeModal()
        }).catch(err => {
            alert("xatolik")
        })
    }
    editMinistry = (ministry) => {
        this.openModal();
        this.setState({currentMinistry: ministry})
    }
    deleteModal = (ministry) => {
        this.setState({
            currentMinistry: ministry,
            showModalDelete: true
        })
    }
    hideDeleteModal = () => {
        this.setState({
            showModalDelete: false,
            currentMinistry: ''
        })
    }
    deleteMinistry = () => {
        let current = this.state.currentMinistry
        request({
            url: api.deleteMinistry + '/' + current.id,
            method: 'DELETE'
        }).then(ans => {
            this.hideDeleteModal()
            this.getMinistries()
        }).catch(err => {
            alert("xatolik")
        })
    }

    render() {
        return (
            <div>
                <h1 className="text-center">Ministry list</h1>
                <button className="btn btn-primary"
                        onClick={this.openModal}>add ministry
                </button>
                <br/>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Ministry name</th>
                        <th>Country name</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.ministries?.map((ministry, index) =>
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{ministry.name}</td>
                            <td>{ministry.country.name}</td>
                            <td>
                                <button className="btn btn-success"
                                        onClick={() => this.editMinistry(ministry)}>edit
                                </button>
                                <button className="btn btn-danger"
                                        onClick={() => this.deleteModal(ministry)}>delete
                                </button>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </table>
                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>{this.state.currentMinistry ? "Edit Ministry" : "Add Ministry"}</ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveMinistry}>
                            <AvField
                                defaultValue={this.state.currentMinistry.name}
                                name="name" required/>
                            <AvField
                                type="select"
                                name="countryId">
                                <option value="" disabled>select country</option>
                                {this.state.countries?.map((country, index) =>
                                    <option value={country.id}>{country.name}</option>)}
                            </AvField>
                            <button className="btn btn-success mt-3">{this.state.currentMinistry?'Edit':'Save'}</button>
                            <button className="btn btn-danger mt-3"
                                    onClick={this.closeModal}>cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>{"Do you want delete this ministry " + this.state.currentMinistry.name}</ModalHeader>
                    <ModalFooter>
                        <button className="btn btn-danger" onClick={this.deleteMinistry}>delete</button>
                        <button className="btn btn-success"
                                onClick={this.hideDeleteModal}>cancel
                        </button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

Ministry.propTypes = {};

export default Ministry;