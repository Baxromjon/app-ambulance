import React, {Component} from 'react';
import request from "../../utils/request";
import api from "../../utils/api";
import {TOKEN} from "../../utils/constant";
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation'

class Territory extends Component {
    state = {
        territories: [],
        currentTerritory: '',
        districts: [],
        currentDistrict: '',
        showModal: false,
        showModalDelete: false
    }


    componentDidMount() {
        if (!localStorage.getItem(TOKEN)) {
            this.props.history.push('/login')
        } else {
            this.getTerritory()
            this.getDistrict()
        }
    }

    getTerritory = () => {
        request({
            url: api.getAllTerritory,
            method: 'GET'
        }).then(res => {
            // console.log(res.data)
            this.setState({territories: res.data})
        }).catch(err => {
        })
    }
    getDistrict = () => {
        request({
            url: api.getDistrict,
            method: 'GET'
        }).then(res => {
            // console.log(res.data)
            this.setState({districts: res.data})
        }).catch(err => {
        })
    }
    openModal = () => {
        this.setState({
            showModal: !this.state.showModal
        })
    }
    openDeleteModal = () => {
        this.setState({showModalDelete: !this.state.showModalDelete})
    }
    editModal = (territory) => {
        console.log(territory.id)
        this.openModal()
        this.setState({currentTerritory: territory})
    }
    deleteModal = (territory) => {
        this.openDeleteModal()
        this.setState({currentTerritory: territory})
    }
    saveTerritory = (e, v) => {
        let current = this.state.currentTerritory
        request({
            url: !current ? api.saveTerritory : (api.editTerritory + '/' + current.id),
            method: current ? 'PUT' : 'POST',
            data: v
        }).then(res => {
            this.getTerritory()
            this.openModal()
        }).catch(err => {
        })
    }
    deleteTerritory = () => {
        let current = this.state.currentTerritory
        console.log("deletega keldi")
        request({
            url: api.deleteTerritory + '/' + current.id,
            method: 'DELETE'
        }).then(res => {
            this.openDeleteModal()
            this.getTerritory()
        }).catch(err => {
        })
    }

    render() {
        const {territories, districts, currentTerritory} = this.state
        return (
            <div>
                <h1 className="text-center">Territory page</h1>
                <button className="btn btn-success"
                        onClick={this.openModal}>add territory
                </button>
                <hr/>
                <br/>
                <table className="table table-bordered text-center">
                    <thead>
                    <th>№</th>
                    <th>name</th>
                    <th>street</th>
                    <th>district</th>
                    <th>action</th>
                    </thead>
                    <tbody>
                    {territories?.map((item, index) =>
                        (
                            <tr key={index}>
                                <td>{index + 1}</td>

                                <td>{item.name}</td>
                                <td>{item.street}</td>
                                <td>{item.district.name}</td>
                                <td>
                                    <button className="btn btn-success"
                                            onClick={() => this.editModal(item)}>edit
                                    </button>
                                    <button className="btn btn-danger"
                                            onClick={() => this.deleteModal(item)}>delete
                                    </button>
                                </td>
                            </tr>)
                    )}
                    </tbody>

                </table>

                <Modal isOpen={this.state.showModal}>
                    <ModalHeader>{currentTerritory ? "edit territory" : "save territory"}</ModalHeader>
                    <ModalBody>
                        <AvForm onValidSubmit={this.saveTerritory}>
                            <AvField
                                defaultValue={currentTerritory.name}
                                placeholder="territory name here"
                                validate={{required: {value: true, errorMessage: "Please enter name"}}}
                                name="name"/>
                            <AvField
                                defaultValue={currentTerritory.street}
                                placeholder="territory street here"
                                validate={{required: {value: true, errorMessage: "Please enter street"}}}
                                name="street"/>
                            <AvField
                                type="select"
                                name="districtId"
                                validate={{required: {value: true, errorMessage: "Please select district"}}}
                            >
                                <option value="" disabled>select district</option>
                                {districts?.map(item =>
                                    <option value={item.id}>{item.name}</option>)}
                            </AvField>
                            <button className="btn btn-success">save</button>
                            <button className="btn btn-danger"
                                    onClick={this.openModal}>cancel
                            </button>
                        </AvForm>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.showModalDelete}>
                    <ModalHeader>{"Do you want delete this territory: " + currentTerritory.name}</ModalHeader>
                    <ModalFooter>
                        <button className="btn btn-danger"
                                onClick={this.deleteTerritory}>Delete
                        </button>
                        <button className="btn btn-success"
                                onClick={this.openDeleteModal}>Cancel
                        </button>
                    </ModalFooter>
                </Modal>
            </div>

        );
    }
}

Territory.propTypes = {};

export default Territory;