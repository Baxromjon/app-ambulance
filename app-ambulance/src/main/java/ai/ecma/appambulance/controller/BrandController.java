package ai.ecma.appambulance.controller;

import ai.ecma.appambulance.entity.Brand;
import ai.ecma.appambulance.service.BrandService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/brand")
public class BrandController {
    @Autowired
    BrandService brandService;

    @GetMapping("getAllBrand")
    public List<Brand> getAllBrand(){
       return brandService.getAllBrand();
    }

}
