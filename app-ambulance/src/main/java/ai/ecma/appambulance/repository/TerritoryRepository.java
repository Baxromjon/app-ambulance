package ai.ecma.appambulance.repository;

import ai.ecma.appambulance.entity.District;
import ai.ecma.appambulance.entity.Territory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * created by Baxromjon
 * 28.05.2021
 **/

public interface TerritoryRepository extends JpaRepository<Territory, UUID> {
    Boolean existsByDistrictAndStreet(District district, String street);
}
