package ai.ecma.appambulance.service;

import ai.ecma.appambulance.entity.Car;
import ai.ecma.appambulance.entity.TimeTable;
import ai.ecma.appambulance.entity.User;
import ai.ecma.appambulance.payload.ApiResponse;
import ai.ecma.appambulance.payload.TimeTableDTO;
import ai.ecma.appambulance.repository.CarRepository;
import ai.ecma.appambulance.repository.TimeTableRepository;
import ai.ecma.appambulance.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import sun.java2d.pipe.AAShapePipe;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class TimeTableService {
    @Autowired
    TimeTableRepository timeTableRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CarRepository carRepository;

    public List<TimeTable> getAll() {
        return timeTableRepository.findAll();
    }

    public ApiResponse addTimeTable(TimeTableDTO timeTableDTO) throws ParseException {
        User user = userRepository.findById(timeTableDTO.getUserId()).orElseGet(User::new);
        Car car = carRepository.findById(timeTableDTO.getCarId()).orElseGet(Car::new);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date parsedDate = dateFormat.parse(timeTableDTO.getFromDate() + " " + timeTableDTO.getFromTime()+":00.000");
        Timestamp fromTime = new java.sql.Timestamp(parsedDate.getTime());
        Date parsedDateToTime = dateFormat.parse(timeTableDTO.getToDate() + " " + timeTableDTO.getToTime()+":00.000");
        Timestamp toTime = new Timestamp(parsedDateToTime.getTime());
        Boolean exists = timeTableRepository.existsByUserAndCarAndFromTime(user, car, fromTime);
        if (exists) {
            return new ApiResponse(false, "allReady exists");
        }
        TimeTable timeTable = new TimeTable(user, car, fromTime, toTime, timeTableDTO.getRateByHour());
        timeTableRepository.save(timeTable);
        return new ApiResponse(true, "successfully added");

    }
}
