package ai.ecma.appambulance.entity;

import ai.ecma.appambulance.entity.enums.RoleName;
import ai.ecma.appambulance.entity.template.AbsEntity;
import ai.ecma.appambulance.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class OrganizationRole extends AbsEntity implements GrantedAuthority {


    @Enumerated(EnumType.STRING)
    @Column(name = "name")
    private RoleName name;//SHOFYOR, DOCTOR, XAMSHIRA


    @ManyToMany
    @JoinTable(name = "organization_role_organization_permission",
            joinColumns = {@JoinColumn(name = "organization_role_id")},
            inverseJoinColumns = {@JoinColumn(name = "organization_permission_id")})
    private List<OrganizationPermission> organizationPermissions;


    public OrganizationRole(RoleName name) {
        this.name = name;
    }

    public OrganizationRole(List<OrganizationPermission> organizationPermissions) {
        this.organizationPermissions = organizationPermissions;
    }

    @Override
    public String getAuthority() {
        return name.name();
    }
}
