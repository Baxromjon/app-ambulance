package ai.ecma.appambulance.payload;
/**
 * created by Baxromjon
 * 28.05.2021
 **/

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor


public class TerritoryDto {
    private String name;
    private String street;
    private UUID districtId;
}
